<?php
opcache_reset();
/**
 * SqlQueryBuilder Class
 * Generates simple SQL queries dynamically
 * Please report any bug
 * See documentation
 * 
 * @author David Regla Demaree <dreglad@gmail.com>
 */
if ((int)phpversion() < 5) die("Sorry, This class is written for PHP 5, maybe later I'll re-write it for PHP 4");
class SqlQueryBuilder {
	
	const VERSION = "1.3";
	/**
	 * Turns error messages on and off, usefull for debugging
	 *
	 * @var boolean
	 */
	public $showErrors = true;
	public $showMessages = true;
	
	/**
	 * Holds the query type (select, insert, update, delete)
	 * it can take an extra value named "query" for
	 * returning arbitrary SQL Queries.
	 *
	 * @var string
	 */
	public $queryType = "SELECT"; // Default Query Type
	
	/**
	 * These hold the variables for building the query
	 *
	 * @var string
	 */
	protected $query;
	protected $table;
	protected $groupBy;
	protected $having;
	protected $orderBy;
	protected $limit;
	
	/**
	 * Columns and values arrays, dimensions must match
	 *
	 * @var array
	 */
	protected $columnNames = array(); 
	protected $columnValues  = array();


	/**
	 * Constructor - Defines the query type
	 *
	 * @param string $queryType
	 */
	public function __construct($queryType) {
		$this->queryType = strtoupper($queryType);
	}

	public function setTable($tableName) {
		$this->table = $tableName;
	}
 	public function resetAll() {
		$this->table = "";
		$this->query = "";
		$this->resetQueryItems();
	}

	public function resetQueryItems() {
		$this->columnNames  = null;
		$this->columnValues = null;
		$this->groupBy = "";
		$this->having = "";
		$this->orderBy = "";
		$this->limit = "";
	} 
	/**
	 * Adds a (column, value, datatype) to the list
	**/
	/* private function addColumnName($colName) {
		$this->print_message("addColumnName::colName=".$colName);
		$this->columnNames[] = $colName;
	}
	private function addColumnValue($colValue, $colType="text") {
	  $this->print_message("addColumnValue::colValue=".$colValue);
	  $colType=trim($colType);
	  if($colValue <> null and $colValue <> '') {
		  if ($colType == "number") {
			$this->columnValues[] = $colValue;		  
		  } else if ($colType == "date") {
			$this->columnValues[] = "date('".$colValue."')"; //date must be in 'YYYY-MM-DD' format
		  } else { //"text"
			$colValue = (get_magic_quotes_gpc()) ? $colValue : addslashes($colValue);
			$this->columnValues[] = "'".$colValue."'";
		  }
	  }
	} */
	public function addColumnValuePair($colName, $colValue, $colType="text" ) {
		$this->print_message("addColumnValuePair [$colName] [$colValue]");
		$colType=trim($colType);
		if($colName <> null and $colName <> '' and $colValue <> null and $colValue <> '') { 
			if ($colType == "number" and $colValue >= 0) {
				$this->columnNames[] = $colName;
				$this->columnValues[] = $colValue; 
			} else if ($colType == "date") { // AND validate_date($colValue);
				$this->columnNames[] = $colName;
				$this->columnValues[] = "date('".$colValue."')"; //date must be in 'YYYY-MM-DD' format
			} else { //"text"
				$colValue = (get_magic_quotes_gpc()) ? $colValue : addslashes($colValue);
				$this->columnNames[] = $colName;
				$this->columnValues[] = "'".$colValue."'";
			}
		}
	}
	public function addColumnValuePairArray($colNameArray, $colValueArray, $colTypeArray=Array() ) {
		$this->print_message("addColumnValuePairArray");
		for ($arrayIndx=0, $colCount = count($colNameArray); $arrayIndx < $colCount ; $arrayIndx++) {
			$this->print_message("colName::".$colNameArray[$arrayIndx]);
			if($colTypeArray[$arrayIndx] <> null AND $colTypeArray[$arrayIndx] <> "")
				$this->addColumnValuePair($colNameArray[$arrayIndx], $colValueArray[$arrayIndx], $colTypeArray[$arrayIndx]);
			$this->addColumnValuePair($colNameArray[$arrayIndx], $colValueArray[$arrayIndx]);
		}
	}
	public function addColumnValuePairList($colNameList, $colValueList, $colTypeList="" ) { // list must be speperated by $FIELD_SEPERATOR defined in _GLOBAL_CONSTANTS.php
		$this->print_message("addColumnValuePairList");
		$colNameArray = explode("],[", $colNameList);
		$colValueArray = explode("],[", $colValueList);
		$colTypeArray = explode("],[", $colTypeList);
		$this->print_message("count(colNameArray)::".count($colNameArray));
		$this->addColumnValuePairArray($colNameArray, $colValueArray, $colTypeArray );
	}
	public function setWhere($where) {
		$this->where = $where;	
	}
	public function setGroupBy($groupBy) {
		$this->groupBy = $groupBy;
	}
	public function setHaving($having) {
		$this->having = $having;
	}
	public function setOrderBy($orderBy) {
		$this->orderBy = $orderBy;
	}
	public function setQuery($query) {
		$this->query = $query;
	}
	public function setLimit($limit) {
		$this->limit = $limit;
	}
	
	public function showErrors($showErrors) {
		$this->showErrors = ($showErrors)? true:false; 
	}
	private function error($message) {
		if ($this->showErrors) {
/* 			print "<font size='10' face='arial' color='red'>\n";
			print "<p>SQLQueryBuilder v" . self::VERSION . "</p>";
			print "</font>";
			print "<font size='8' face='arial' color='blue>\n";
			print "<p>$message</p>";
			print "</font>"; */
			echo "<font size='10' face='arial' color='red'>\n";
			echo "<p>SQLQueryBuilder v" . self::VERSION . "</p>";
			echo "</font>";
			echo "<font size='8' face='arial' color='blue>\n";
			echo "<p>$message</p>";
			echo "</font>";
		}
		return false;
	}
	public function print_message($message_text) {
		if($this->showMessages)
			echo "<br>.$message_text";
	}
	/**
	 * Generates and returns query as a string
	 * @return string
	**/
	public function buildQuery() {
		//echo "<br>buildQuery::";
		//echo "<br>this->queryType::".$this->queryType."";
		//echo "<br>this->table::".$this->table."";
		//var_dump($this->columnNames);
		//var_dump($this->columnValues);

		if (empty($this->table) and ($this->queryType != "QUERY")) return $this->error("Error - No table selected");

		$sqlString = "";
		switch ($this->queryType) {
			case "SELECT":
				$sqlString.= "SELECT ";
				$sqlString.= implode(", ", $this->columnNames);
				$sqlString.=" FROM {$this->table}";
				
				if ($this->where)   $sqlString.= " WHERE $this->where";
				if ($this->groupBy) $sqlString.= " GROUP BY $this->groupBy";
				if ($this->having)  $sqlString.= " HAVING $this->having";
				if ($this->orderBy) $sqlString.= " ORDER BY $this->orderBy";
				if ($this->limit)   $sqlString.= " LIMIT $this->limit";
				
				break;
			case "INSERT":
				if (count($this->columnNames) != count($this->columnValues)) return $this->error("Error - Column list doesn't match the value list");
				$sqlString.= "INSERT INTO {$this->table} ";
				
				$sqlString.= "(";
				$sqlString.= implode(", ", $this->columnNames);
				$sqlString.= ") ";
				
				$sqlString.= "VALUES";
				
				$sqlString.= " (";
				$sqlString.= implode(", ", $this->columnValues);
				$sqlString.= ")";
				break;
			case "UPDATE":
				if (count($this->columnNames) != count($this->columnValues)) return $this->error("Error - count(ColumnName) doesn't match the count(ColumnName)");
				$sqlString.= "UPDATE {$this->table} SET ";
				
				$noColumns = count($this->columnNames); //count($this->columnNames);
				for ($i=0;$i<$noColumns;$i++) {
					$sqlString.= "{$this->columnNames[$i]} = {$this->columnValues[$i]}";
					if ($i < $noColumns-1) $sqlString.= ", ";
				}
				if ($this->where) $sqlString.= " WHERE $this->where";
				if ($this->limit) $sqlString.= " LIMIT $this->limit";
				
				break;
			case "DELETE":
				$sqlString.= "DELETE FROM {$this->table} ";
				
				if ($this->where) $sqlString.= "WHERE $this->where";
				break;
			case "QUERY":
				if (!$this->query) $this->error("Warning - There's no SQL");
				$sqlString.= $this->query;
		}
		$sqlString.=";";
		return $sqlString;
	}
	public function buildSQLsfromJsonArray($keyIdColumnDetails, $jsonArray, $UItoDBMappingArray, $columnsDataTypeArray ) {
		global $DB_ITEMS_TABLE_NAME;
		$sqlArray = array();
		//echo "<br>buildSQLsfromJsonArray :: this->table::".$this->table."";
		$UIelementNames = array_keys($UItoDBMappingArray);
		for($jsonIndx=0; $jsonIndx < count($jsonArray) ; $jsonIndx++ ) { // loop thru total rows in the JsonObject
			//echo "<br>buildSQLsfromJsonArray::jsonIndx=$jsonIndx";
			$this->resetQueryItems();
			$this->addColumnValuePair($keyIdColumnDetails['columnName'], $keyIdColumnDetails['columnValue'], $keyIdColumnDetails['columnType']);
			for($elementIndx=0; $elementIndx < count($UIelementNames); $elementIndx++ ) { // loop thru total columns in the JsonObject
				$colName=$UItoDBMappingArray[$UIelementNames[$elementIndx]];
				$colValue=$jsonArray[$jsonIndx][$UIelementNames[$elementIndx]];
				$colType=$columnsDataTypeArray[$UIelementNames[$elementIndx]];
				//echo "<br>.....colName=$colName [$colValue] [$colType]";
			if( ($colType == "number" and  $colValue > 0) OR  ($colType == "text" and  $colValue <> "") )
				$this->addColumnValuePair($colName,$colValue,$colType);
			}
			if($this->queryType == "UPDATE") {
				if($keyIdColumnDetails['columnType'] == 'number')
					$this->setWhere($keyIdColumnDetails['columnName']." = ".$keyIdColumnDetails['columnValue']);
				else
					$this->setWhere($keyIdColumnDetails['columnName']." = '".$keyIdColumnDetails['columnValue']."'");
			}
			if($this->table == $DB_ITEMS_TABLE_NAME) {
				$this->addColumnValuePair('last_updated_by', $_SESSION['userId'], "number");
				$this->addColumnValuePair('last_updated_date', 'now()', "number");
				if($this->queryType == "INSERT") {
					$this->addColumnValuePair('entered_by', $_SESSION['userId'], "number");
					$this->addColumnValuePair('entered_date', 'now()', "number");
				}
			}
			$sql = $this->buildQuery();
			// echo "<br>buildSQLsfromJsonArray::sql=$sql";
			$sqlArray[] = $sql;
		}
		return $sqlArray;
	}
}
?>
