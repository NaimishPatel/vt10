<?php 
	opcache_reset();
	// require_once '../../../api/session.php';
	require_once '../../../api/connection.php';
	require_once '../include/PHP4remote.php';
	require_once '../../../api/SqlQueryBuilder.class.php';
	// ini_set("display_errors", "1");
	// error_reporting(E_ERROR | E_PARSE | E_WARNING);
	// error_reporting(E_ALL);
?>
<?php
	//$itemId = $_REQUEST['itemId'];
	//echo "<br>".getItemDetailsJson($itemId);

 // function getItemDetailsJson($parentItemId) {
 		$parentItemId = $_REQUEST['parentItemId'];
		global $itemUItoDBMappingArray;
		global $DB_ITEMS_TABLE_NAME;
		global $itemDateUItoDBMappingArray; 
		global $DB_ITEMDATES_TABLE_NAME;
		global $itemSubjectsUItoDBMappingArray;
		global $DB_ITEMSUBJECTS_TABLE_NAME;
		global $itemKeywordLineUItoDBMappingArray;
		global $DB_ITEMKEYWORDS_TABLE_NAME;
		global $itemEventsUItoDBMappingArray;
		global $DB_ITEMEVENTS_TABLE_NAME;
		global $itemLocationsUItoDBMappingArray;
		global $DB_ITEMLOCATIONS_TABLE_NAME;
		global $con;
		
		//while ($dbRowsItemChildren = mysql_fetch_assoc($dbResultItemChildren));
		// array_pop($dbRowsItemChildren);
		$itemArray = array();
		$itemIndx=0;
		// process parent [ start
			$itemArray[$itemIndx] = fetchTableData($itemUItoDBMappingArray,$DB_ITEMS_TABLE_NAME, $parentItemId, "SingleRecord");
			$itemDescp = fetchTableData($itemTextsUItoDBMappingArray, $DB_ITEMTEXTS_TABLE_NAME, $parentItemId, "SingleRecord");
			$itemArray[$itemIndx]['additionalDetails'] = $itemDescp;
			$itemDatesArray = fetchTableData($itemDateUItoDBMappingArray, $DB_ITEMDATES_TABLE_NAME, $parentItemId, "MultiRecord");
			$itemArray[$itemIndx]['eventDateArray'] = $itemDatesArray;
			$itemSubjectsArray = fetchTableData($itemSubjectsUItoDBMappingArray, $DB_ITEMSUBJECTS_TABLE_NAME, $parentItemId, "MultiRecord");
			$itemArray[$itemIndx]['subjectArray'] = $itemSubjectsArray;
			$itemKeywordlinesArray = fetchTableData($itemNotesUItoDBMappingArray, $DB_ITEM_NOTES_TABLE_NAME, $parentItemId, "MultiRecord");
			$itemArray[$itemIndx]['keywordLineArray'] = $itemKeywordlinesArray;
			$itemEventArray = fetchTableData($itemEventsUItoDBMappingArray, $DB_ITEMEVENTS_TABLE_NAME, $parentItemId, "MultiRecord");
			$itemArray[$itemIndx]['eventArray'] = $itemEventArray;
			$itemLocationlinesArray = fetchTableData($itemLocationsUItoDBMappingArray, $DB_ITEMLOCATIONS_TABLE_NAME, $parentItemId, "MultiRecord");
			$itemArray[$itemIndx]['locationArray'] = $itemLocationlinesArray;
			$itemLangugelinesArray = fetchTableData($itemLanguagesUItoDBMappingArray, $DB_ITEMLANGUAGES_TABLE_NAME, $parentItemId, "MultiRecord");
			$itemArray[$itemIndx]['languageArray'] = $itemLangugelinesArray;
			$itemVaktalinesArray = fetchTableData($itemVaktaUItoDBMappingArray, $DB_ITEM_VAKTA_TABLE_NAME, $parentItemId, "MultiRecord");
			$itemArray[$itemIndx]['vaktaArray'] = $itemVaktalinesArray;
			
			$itemCategorylinesArray = fetchTableData($itemCategoriesUItoDBMappingArray, $DB_ITEM_CATEGORIES_TABLE_NAME, $parentItemId, "MultiRecord");
			$itemArray[$itemIndx]['categotyArray'] = $itemCategorylinesArray;

			//tagsArray
			$itemTagslinesArray = fetchTableData($itemTagsUItoDBMappingArray, $DB_ITEM_TAG_TABLE_NAME, $parentItemId, "MultiRecord");
			$itemArray[$itemIndx]['tagsArray'] = $itemTagslinesArray;
			
			//$JsonString = json_encode ($itemArray);
			//echo "<br>JsonString=$JsonString";
		// process children/clips ] end
		
		// process children/clips [ start
			$vars = array("~~parentItemId~~" => $parentItemId);
			$dbQueryItemChildren = substituteTokensIn($QUERY_GET_ITEM_CHILDREN, $vars);
			//echo "<br>dbQueryItemChildren=$dbQueryItemChildren";
			$dbResultItemChildren = mysql_query($dbQueryItemChildren, $con) ;

			for($itemIndx=1; $dbRowItemChildren = mysql_fetch_array($dbResultItemChildren) ;  $itemIndx++) 
			{
				$itemId = $dbRowItemChildren['item_id'];
				//fetch items_final
				//echo "<br>itemIdArray[itemIndx]::".$itemId;
				$itemArray[$itemIndx] = fetchTableData($itemUItoDBMappingArray,$DB_ITEMS_TABLE_NAME, $itemId, "SingleRecord");
				//$itemArray['itemDetails'] = $itemDetailsArray;
				//$JsonString = json_encode ($itemArray);
				//echo "<br>JsonString=$JsonString";
				//fetch item_dates
				$itemDatesArray = fetchTableData($itemDateUItoDBMappingArray, $DB_ITEMDATES_TABLE_NAME, $itemId, "MultiRecord");
				$itemArray[$itemIndx]['eventDateArray'] = $itemDatesArray;
				
				//additionalDetails
				$itemDescp = fetchTableData($itemTextsUItoDBMappingArray, $DB_ITEMTEXTS_TABLE_NAME, $itemId, "SingleRecord");
				$itemArray[$itemIndx]['additionalDetails'] = $itemDescp;
				//$JsonString = json_encode ($itemArray);
				//echo "<br>JsonString=$JsonString";
				//fetch item_subjects
				$itemSubjectsArray = fetchTableData($itemSubjectsUItoDBMappingArray, $DB_ITEMSUBJECTS_TABLE_NAME, $itemId, "MultiRecord");
				$itemArray[$itemIndx]['subjectArray'] = $itemSubjectsArray;
				//$JsonString = json_encode ($itemArray);
				//echo "<br>JsonString=$JsonString";
				//fetch item_keywordlines
				$itemKeywordlinesArray = fetchTableData($itemNotesUItoDBMappingArray, $DB_ITEM_NOTES_TABLE_NAME, $itemId, "MultiRecord");
				$itemArray[$itemIndx]['keywordLineArray'] = $itemKeywordlinesArray;

				$itemEventArray = fetchTableData($itemEventsUItoDBMappingArray, $DB_ITEMEVENTS_TABLE_NAME, $itemId, "MultiRecord");
				$itemArray[$itemIndx]['eventArray'] = $itemEventArray;
				
				$itemLocationlinesArray = fetchTableData($itemLocationsUItoDBMappingArray, $DB_ITEMLOCATIONS_TABLE_NAME, $itemId, "MultiRecord");
				$itemArray[$itemIndx]['locationArray'] = $itemLocationlinesArray;
				
				$itemLangugelinesArray = fetchTableData($itemLanguagesUItoDBMappingArray, $DB_ITEMLANGUAGES_TABLE_NAME, $itemId, "MultiRecord");
				$itemArray[$itemIndx]['languageArray'] = $itemLangugelinesArray;
				
				$itemVaktalinesArray = fetchTableData($itemVaktaUItoDBMappingArray, $DB_ITEM_VAKTA_TABLE_NAME, $itemId, "MultiRecord");
				$itemArray[$itemIndx]['vaktaArray'] = $itemVaktalinesArray;
				
				$itemCategorylinesArray = fetchTableData($itemCategoriesUItoDBMappingArray, $DB_ITEM_CATEGORIES_TABLE_NAME, $itemId, "MultiRecord");
				$itemArray[$itemIndx]['categotyArray'] = $itemCategorylinesArray;
				
				$itemTagslinesArray = fetchTableData($itemTagsUItoDBMappingArray, $DB_ITEM_TAG_TABLE_NAME, $itemId, "MultiRecord");
				$itemArray[$itemIndx]['tagsArray'] = $itemTagslinesArray;
				//$JsonString = json_encode ($itemArray);
				//echo "<br>JsonString=$JsonString";
			
			}
		// process children/clips ] end
		// $jsonString = json_encode ($itemArray);
		echo json_encode ($itemArray);
		//echo "<br>count(itemArray)::".count($itemArray);
		//echo "jsonString=$jsonString";
		// return $jsonString;
	// }
	function fetchTableData($UItoDBMappingArray, $dbTableName, $itemId, $type){
		global $con;
		global $QUERY_GET_TABLE_DATA_FOR_ITEM;
		$vars = array("~~dbTableName~~" => $dbTableName, "~~itemId~~" => $itemId);
  		$dbQuery = substituteTokensIn($QUERY_GET_TABLE_DATA_FOR_ITEM, $vars);
		//echo "<br>dbQuery=$dbQuery";
		$dbResult = mysql_query($dbQuery, $con) ;
		if(!$dbResult) {
			echo "<br>mysql_error()=".mysql_error();
		}
		
		//echo "<br>var_dump(dbResult)::";  var_dump($dbResult);
 		$dbRowsArray = array();
		while ($dbRowsArray[] = mysql_fetch_assoc($dbResult));
		array_pop($dbRowsArray);
		$UIElementNames = array_keys($UItoDBMappingArray);
		$DBcolumnNames = array_keys(array_flip($UItoDBMappingArray));
		
		
		//echo "<br>var_dump(dbRowsArray)::";  var_dump($dbRowsArray);
		$matchingRows = array();
		$matchingRowsIndx = 0;
		foreach($dbRowsArray as $dbRowArray)
		{
			//echo "<br>var_dump(dbRowArray)::";  var_dump($dbRowArray[0]);
			for($columnIndx=0; $columnIndx < count($UItoDBMappingArray); $columnIndx++) 
			{
				//echo "<br>UIElementNames[columnIndx]=".$UIElementNames[$columnIndx];
				//echo "<br>DBcolumnNames[columnIndx]=".$DBcolumnNames[$columnIndx];
				//echo "<br>dbRowArray[DBcolumnNames[columnIndx]]=".$dbRowArray['item_id'];
				if($dbRowArray[$DBcolumnNames[$columnIndx]] <> null) 
				{
					if($type=="SingleRecord")
						$matchingRows[$UIElementNames[$columnIndx]] = $dbRowArray[$DBcolumnNames[$columnIndx]];
					else
						$matchingRows[$matchingRowsIndx][$UIElementNames[$columnIndx]] = $dbRowArray[$DBcolumnNames[$columnIndx]];
				}
			}
			$matchingRowsIndx++;
		}
		//echo "<br>var_dump(matchingRows)::"; var_dump($matchingRows[0]);
 		//$matchingRows = array();
		return $matchingRows; 
	}
 ?>
<?php
/**
Search Logic
============================================
select * from items where 1=1 
AND title like '%%'
AND category = ''
AND quality = ''
AND file_path like ''
AND confidentiality = ''
AND privileges = ''
AND language = ''
where item_id in
(
	select item_id from item_keywordlines where  (who_id = 42133 and when_id= 5235) 
	UNION
	select item_id from item_keywordlines where  (who_id = 1442 and where_id=2342) 
	UNION
	select item_id from item_subjects where (subject_id = 134)
	UNION
	select item_id from item_dates where start_date_yyyy <= 2015 and start_date_mm <= 05 and start_date_dd <= 7 and end_date_yyyy >= 2015 and end_date_mm >= 05 and end_date_dd >= 7
)

**/
?>