<?php 
	opcache_reset();
	// require_once '../../../api/session.php';
	require_once '../../../api/connection.php';
	require_once '../include/PHP4remote.php';
	require_once '../../../api/SqlQueryBuilder2.class.php';
	ini_set("display_errors", "1");
	error_reporting(E_ERROR | E_PARSE);
	//error_reporting(E_ALL);
?>
<?php
	$itemJsonString = 
	'[ { '
	.'			  "itemId": "" '
	.'			, "parentItemId": "" '
	.'			, "itemCategory": "" '
	.'			, "itemTitle": "Walking" '
	.'			, "technicalQuality": "" '
	.'			, "technicalNotes": "" '
	.'			, "clipStartTime": "" '
	.'			, "clipEndTime": "" '
	.'			, "urlOrFileFolderPath": "" '
	.'			, "fileFolderName": "" '
	.'			, "itemType": "" '
	.'			, "itemUniqueCode": "" '
	.'			, "itemSource": "" '
	.'			, "confidentiality": "" '
	.'			, "accesssibility": "" '
	.'			, "language": "" '
	.'			, "additionalDescp": "" '
	.'} ]';
	$itemJsonString = $_REQUEST['itemJsonString'];
	//echo "<br>itemJsonString=$itemJsonString";
	$itemJsonArray = json_decode($itemJsonString,true);
	//echo "<br>count(itemJsonArray)=".count($itemJsonArray);
	//echo "<br>Title::".$itemJsonArray[0]['itemTitle'];
	$sqlQB = new SqlQueryBuilder2("NULL");
	$sqlQB->queryType = "SELECT";
	$sqlQB->setTable($DB_ITEMS_TABLE_NAME);
	$sqlQB->addColumnValuePair('item_id', '1', 'number' );
	$sqlQB->showMessages = true;
	$sqlQB->showErrors = true;
	global $itemDBtoUIMappingArray;
	//echo "<br>count::".count($itemDBtoUIMappingArray);
	$sqlQB->addColumns($itemUItoDBMappingArray);
	$sqlQB->setWhereClauseColumnsfromJsonArray($itemJsonArray, $itemUItoDBMappingArray, $itemColumnsDataTypeArray, $itemColumnsSearchTypeArray);
	//echo "<br>itemSearchWhereClause::$itemSearchWhereClause";
	$itemSearchSQL = $sqlQB->buildQuery();
	// echo "<br>buildQuery=$itemSearchSQL";
	$itemArray = fetchTableData($itemUItoDBMappingArray,$DB_ITEMS_TABLE_NAME, $itemId, "MultiRecord", $itemSearchSQL);
	echo json_encode ($itemArray);
//-------------------------------------------------------------------------------------------------------------------------------------//
	function fetchTableData($UItoDBMappingArray, $dbTableName, $itemId, $type, $dbQuery) {
		//echo "<br>dbQuery=$dbQuery";
		global $con;
		$dbResult = mysql_query($dbQuery, $con) ;
		if(!$dbResult) {
			echo "<br>mysql_error()=".mysql_error();
		}
		
		//echo "<br>var_dump(dbResult)::";  var_dump($dbResult);
 		$dbRowsArray = array();
		while ($dbRowsArray[] = mysql_fetch_assoc($dbResult));
		array_pop($dbRowsArray);
		$UIElementNames = array_keys($UItoDBMappingArray);
		$DBcolumnNames = array_keys(array_flip($UItoDBMappingArray));
		
		
		//echo "<br>var_dump(dbRowsArray)::";  var_dump($dbRowsArray);
		$matchingRows = array();
		$matchingRowsIndx = 0;
		foreach($dbRowsArray as $dbRowArray)
		{
			//echo "<br>var_dump(dbRowArray)::";  var_dump($dbRowArray[0]);
			for($columnIndx=0; $columnIndx < count($UItoDBMappingArray); $columnIndx++) 
			{
				//echo "<br>UIElementNames[columnIndx]=".$UIElementNames[$columnIndx];
				//echo "<br>DBcolumnNames[columnIndx]=".$DBcolumnNames[$columnIndx];
				//echo "<br>dbRowArray[DBcolumnNames[columnIndx]]=".$dbRowArray[$DBcolumnNames[$columnIndx]];
				if($dbRowArray[$DBcolumnNames[$columnIndx]] <> null) 
				{
					//if($type=="SingleRecord")
					//	$matchingRows[$UIElementNames[$columnIndx]] = $dbRowArray[$DBcolumnNames[$columnIndx]];
					//else
						$matchingRows[$matchingRowsIndx][$UIElementNames[$columnIndx]] = $dbRowArray[$DBcolumnNames[$columnIndx]];
						//echo "<br>dbRowArray[$DBcolumnNames[$columnIndx]] :: ".$dbRowArray[$DBcolumnNames[$columnIndx]];
				}
			}
			$matchingRowsIndx++;
		}
		//echo "<br>var_dump(matchingRows)::"; var_dump($matchingRows[0]);
 		//$matchingRows = array();
		return $matchingRows; 
	}
/*
 	$responseJsonText = '[ { '
							.' "itemId": "123527" '
							.' , "parentItemId": "123" '
							.' , "itemCategory": "1" '
							.' , "itemTitle": "..Satsang Darshan-95th JanmaJayanti" '
							.' , "technicalQuality": "4" '
							.' , "technicalNotes": "voice and audio okay " '
							.' , "clipStartTime": "01:30:15" '
							.' , "clipEndTime": "01:40:45" '
							.' , "urlOrFileFolderPath": "c:/mediaStorage/2015/12/19" '
							.' , "fileFolderName": "AGMZ1467.avi" '
							.' , "itemType": "2" '
							.' , "itemUniqueCode": "bapsSite1Item346" '
							.' , "itemSource": "3" '
							.' , "confidentiality": "4" '
							.' , "accesssibility": "5" '
							.' , "language": "543385" '
							.' , "additionalDescp": "1. blah blah blah \\n 2. blah blah blah \\n 3. blah blah blah..." '
							.'} ]'; 
	echo $responseJsonText;
 */
?>
