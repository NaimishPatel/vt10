<?php
opcache_reset();

require_once '../../api/session.php';
require_once '../../api/connection.php';
require_once '../php/include/PHP.php';

$filePaths = $_REQUEST['filePaths'];



function newTrackHTML ($itemNo,$path){

	
	
	$NEW_AUDIO_TRACK = '';
	$NEW_AUDIO_TRACK .= '<div id="edit_item00_fixedDiv" class="row">';
	$NEW_AUDIO_TRACK .= '        <div class="col-lg-12 col-md-18 col-sm-17 col-xs-24">';
	$NEW_AUDIO_TRACK .= '           <div class="form-group">';
	$NEW_AUDIO_TRACK .= '				<!-- div class="required" -->';
	$NEW_AUDIO_TRACK .= '					<input type="text" class="form-control" name="edit_track_item'.$itemNo.'_itemTitle" id="edit_track_item'.$itemNo.'_itemTitle" placeholder="'.$LABEL_TRACK_TITLE_PLACEHOLDER.'" autofocus>';
	$NEW_AUDIO_TRACK .= '				<!-- /div -->';
	$NEW_AUDIO_TRACK .= '           </div>';
	$NEW_AUDIO_TRACK .= '       </div><!-- .col title div closed -->';
	$NEW_AUDIO_TRACK .= '       <div class="col-lg-3 col-md-6 col-sm-7 col-xs-24">';
	$NEW_AUDIO_TRACK .= '           <div class="form-group">';
	$NEW_AUDIO_TRACK .= '               <select class="form-control itemCategory" name="edit_track_item'.$itemNo.'_itemCategory" id="edit_track_item'.$itemNo.'_itemCategory" placeholder="'.$LABEL_TRACK_CATEGORY_PLACEHOLDER.'">';
	$NEW_AUDIO_TRACK .= '                   <!--option value="">-- Category --</option-->';
	$NEW_AUDIO_TRACK .= '               </select>';
	$NEW_AUDIO_TRACK .= '           </div><!-- .form-group div closed -->';
	$NEW_AUDIO_TRACK .= '       </div><!-- .col category div closed -->';
	$NEW_AUDIO_TRACK .= '       <div class="col-lg-3 col-md-6 col-sm-7 col-xs-24">';
	$NEW_AUDIO_TRACK .= '           <div class="form-group">';
	$NEW_AUDIO_TRACK .= '               <select class="form-control itemCategory2" name="edit_track_item'.$itemNo.'_itemCategory2" id="edit_track_item'.$itemNo.'_itemCategory2" placeholder="'.$LABEL_TRACK_SUB_CATEGORY_PLACEHOLDER.'">';
	$NEW_AUDIO_TRACK .= '                   <!--option value="">-- Category2 --</option-->';
	$NEW_AUDIO_TRACK .= '               </select>';
	$NEW_AUDIO_TRACK .= '           </div><!-- .form-group div closed -->';
	$NEW_AUDIO_TRACK .= '       </div><!-- .col category2 div closed -->';
	$NEW_AUDIO_TRACK .= '       <div class="col-lg-3 col-md-6 col-sm-7 col-xs-24">';
	$NEW_AUDIO_TRACK .= '           <div class="form-group">';
	$NEW_AUDIO_TRACK .= '               <select class="form-control itemTrackNumber" name="edit_track_item'.$itemNo.'_itemTrackNumber" id="edit_track_item'.$itemNo.'_itemTrackNumber" placeholder="'.$LABEL_TRACK_NUMBER_PLACEHOLDER.'">';
	$NEW_AUDIO_TRACK .= '                   <!--option value="">-- tracknumber --</option-->';
	$NEW_AUDIO_TRACK .= '               </select>';
	$NEW_AUDIO_TRACK .= '           </div><!-- .form-group div closed -->';
	$NEW_AUDIO_TRACK .= '       </div><!-- .col track number div closed -->';
	$NEW_AUDIO_TRACK .= '       <div class="col-lg-3 col-md-6 col-sm-7 col-xs-24">';
	$NEW_AUDIO_TRACK .= '           <div class="form-group">';
	$NEW_AUDIO_TRACK .= '               <select class="form-control itemTrackLanguage" name="edit_track_item'.$itemNo.'_itemTrackLanguage" id="edit_track_item'.$itemNo.'_itemTrackLanguage" placeholder="'.$LABEL_TRACK_LANGUAGE_PLACEHOLDER.'">';
	$NEW_AUDIO_TRACK .= '                   <!--option value="">-- tracklanguage --</option-->';
	$NEW_AUDIO_TRACK .= '               </select>';
	$NEW_AUDIO_TRACK .= '           </div><!-- .form-group div closed -->';
	$NEW_AUDIO_TRACK .= '       </div><!-- .col track language div closed -->';
	$NEW_AUDIO_TRACK .= '       <div class="col-lg-12 col-md-18 col-sm-17 col-xs-24">';
	$NEW_AUDIO_TRACK .= '           <div class="form-group">';
	$NEW_AUDIO_TRACK .= '				<div id="edit_track_item'.$itemNo.'_urlOrFileFolderPathDisplayBox" class="urlOrFileFolderPath-display-box" tabindex="0" placeholder="<?php echo $LABEL_ITEM_URL_FILE_FOLDER_PATH_PLACEHOLDER; ?>">';
	$NEW_AUDIO_TRACK .= '                   <!-- Dismissible urlOrFileFolderPaths will be displayed here -->';
	$NEW_AUDIO_TRACK .= '               </div>';
	$NEW_AUDIO_TRACK .= '               <input type="hidden" name="edit_track_item'.$itemNo.'_urlOrFileFolderPath" id="edit_item00_urlOrFileFolderPath" />';
	$NEW_AUDIO_TRACK .= '           </div>';
	$NEW_AUDIO_TRACK .= '       </div><!-- .col path div closed -->';
	$NEW_AUDIO_TRACK .= '   </div>';

	
	return $NEW_AUDIO_TRACK;
}
?>