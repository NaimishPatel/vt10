<?php 
	opcache_reset();
	// require_once '../../../api/session.php';
	require_once '../../../api/connection.php';
	require_once '../include/PHP4remote.php';
	require_once '../../../api/SqlQueryBuilder.class.php';
?>
<?php
/*
{Version}:: v07
{Purpose}:: 
	It returns a list of suggested locations based on search string.
{Description}:: 
	There could be two types of search types. 
	[1]= Based on partial location name. For example : Gujar%. 
		In this type, search text wouldn't any contain '>'.
		For this search type, we'll match search text with ilocations.location_list.location_name_e 
		and ilocations.location_synonames.location_name_e.
		(ilocations.location_synonames.name_tree is for future use)
	[2]= Based on parent name tree and partial location name. For example : Asia > India > Gujar%. For this search type, 
		In this type, search text would contain one or more '>'.
		For this search type, we'll match search text with ilocations.location_list.name_tree and ilocations.location_synonames.name_tree
		(ilocations.location_synonames.name_tree is for future use)
*/
		if($rpParentId == "") //this this page is included in other page, for example : KeywordSuggestions.php
			$rpParentId = $_REQUEST['parentId'];
		if($rpTriggererElementId != "")
			$rpTriggererElementId = $_REQUEST['triggererElementId'];
		$rpSearchText = trim($_REQUEST['searchText']);
		$favourite = $_REQUEST['favourite'];
		if($favourite == "true")
			$favouriteClause = $FAVOURITE_CLAUSE;
		else
			$favouriteClause = "";
		if ($rpSearchText == "" && $rpParentId == "")
			$rpParentId = "0";
		if($rpParentId != "") { //fetch all children
			$vars = array("~~rpSearchText~~" => $rpSearchText, "~~rpParentId~~" => $rpParentId, "~~favouriteClause~~" => $favouriteClause);
			$dbQueryMatchingLocs = substituteTokensIn($QUERY_GET_LOCATION_SUGGESTIONS_WITH_CHILDREN, $vars);
		} else  {
			$rpSearchText = str_replace("*","%",$rpSearchText);
			$parentNameTree = "";
			$rpSearchTextArr = explode(">",$rpSearchText); //
			if(count($rpSearchTextArr) > 1) { // if there is one or more nodes in parent hierarchy i.e. if NOT the root node
				$parentNameTree = trim(substr($rpSearchText, 0, strrpos($rpSearchText, ">")),' ');
			}
			//the count shall be always 1 in inotes_version_08
			if(count($rpSearchTextArr) == 1) { // initial search based on partial location name serach text
				$vars = array("~~rpSearchText~~" => $rpSearchText, "~~favouriteClause~~" => $favouriteClause);
				$dbQueryMatchingLocs = substituteTokensIn($QUERY_GET_LOCATION_SUGGESTIONS, $vars);
			} else {
				$vars = array("~~rpSearchText~~" => $rpSearchText, "~~parentNameTree~~" => $parentNameTree, "~~favouriteClause~~" => $favouriteClause);
				$dbQueryMatchingLocs = substituteTokensIn($QUERY_GET_LOCATION_SUGGESTIONS_MATCHING_NAMETREE, $vars);
			}
		}
		// echo "<br>$dbQueryMatchingLocs";
		$dbResultMatchingLocs = mysql_query($dbQueryMatchingLocs, $con) or mysql_error();
		$dbRowArrayMatchingLocs = array();
		while ($dbRowArrayMatchingLocs[] = mysql_fetch_assoc($dbResultMatchingLocs));
		array_pop($dbRowArrayMatchingLocs);
		// $output = "";
		// if (count($dbRowArrayMatchingLocs)>0) $output = "<strong class='small' style='padding-left: 5px; cursor: default;'>Locations:</strong>";
		// foreach($dbRowArrayMatchingLocs as $dbRowMatchingLocs)
		// {
			// $output .= "<li tabindex='-2' class='suggestionLi' id='" .$rpTriggererElementId."' value='" .$dbRowMatchingLocs['location_id']. "' >" .str_ireplace($rpSearchText,$AUTOSUGGEST_LIST_FORMATTING_TAG_START.$rpSearchText.$AUTOSUGGEST_LIST_FORMATTING_TAG_END, $dbRowMatchingLocs['name_tree']). "</li>";
		// } 
		// echo $output."";
		
		 $ret = array();
		 $ret['results'] = $dbRowArrayMatchingLocs;
		 echo json_encode ($ret);
?>