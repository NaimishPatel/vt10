<?php 
	opcache_reset();
	// require_once '../../../api/session.php';
	require_once '../../../api/connection.php';
	require_once '../include/PHP4remote.php';
	require_once '../../../api/SqlQueryBuilder.class.php';
?>
<?php
/*
{Version}:: v10
{Purpose}:: 
	It returns a list of suggested books based on search string.
{Description}:: 
	It compares searchText with inotes.books.book_title
	
*/
		$rpSearchText = trim($_REQUEST['searchText']);
		if($rpTriggererElementId != "") //this page is included in other page, for example : KeywordSuggestions.php
			$rpTriggererElementId = $_REQUEST['triggererElementId'];		
		$rpSearchText = str_replace("*","%",$rpSearchText);
		$vars = array("~~rpSearchText~~" => $rpSearchText);
		$dbQueryMatchingBooks = substituteTokensIn($QUERY_GET_BOOK_SUGGESTIONS, $vars);
		//echo "$rpSearchText";
		// echo "<br>$dbQueryMatchingBooks";
		$dbResultMatchingBooks = mysql_query($dbQueryMatchingBooks, $con) or mysql_error();
		$dbRowArrayMatchingBooks = array();
		while ($dbRowArrayMatchingBooks[] = mysql_fetch_assoc($dbResultMatchingBooks));
		array_pop($dbRowArrayMatchingBooks);
		// $output = "";
		// if (count($dbRowArrayMatchingBooks)>0) $output = "<strong class='small' style='padding-left: 5px; cursor: default;'>Books:</strong>";
		// foreach($dbRowArrayMatchingBooks as $dbRowMatchingBooks)
		// {
			// $caption = str_replace(lcfirst($rpSearchText),$AUTOSUGGEST_LIST_FORMATTING_TAG_START.lcfirst($rpSearchText).$AUTOSUGGEST_LIST_FORMATTING_TAG_END, $dbRowMatchingBooks['full_name']);
			// $caption = str_replace(ucfirst($rpSearchText),$AUTOSUGGEST_LIST_FORMATTING_TAG_START.ucfirst($rpSearchText).$AUTOSUGGEST_LIST_FORMATTING_TAG_END, $caption);
			// $output .= "<li tabindex='-2' class='suggestionLi' id='" .$rpTriggererElementId."' value='" .$dbRowMatchingBooks['person_id']. "' >" .$caption. "</li>";
		// } 
		// echo $output."";
		$ret = array();
		$ret['results'] = $dbRowArrayMatchingBooks;
		echo json_encode ($ret);
?>