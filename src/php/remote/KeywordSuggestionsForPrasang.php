<?php 
	opcache_reset();
	// require_once '../../../api/session.php';
	require_once '../../../api/connection.php';
	require_once '../include/PHP4remote.php';
	require_once '../../../api/SqlQueryBuilder.class.php';
?>
<?php
/*
{Version}:: v07
{Purpose}:: 
	It returns a list of suggested Keywords based on search string.
{Description}:: 
	There could be two types of search types. 
	[1]= Based on partial keyword name. For example : Gujar%. 
		In this type, search text wouldn't any contain '>'.
		For this search type, we'll match search text with inotes.keywords.title 
		and inotes.keyword_synonames.title.
		(ilocations.location_synonames.title is for future use)
	[2]= Based on parent name tree and partial keyword name. For example : Asia > India > Gujar%. For this search type, 
		In this type, search text would contain one or more '>'.
		For this search type, we'll match search text with inotes.keywords.title and inotes.keyword_synonames.title
		(inotes.keyword_synonames.title is for future use)
*/
		$rpParentId = $_REQUEST['parentId'];
		$rpTriggererElementId = $_REQUEST['triggererElementId'];
		$rpSearchText = trim($_REQUEST['searchText']);
		
		$keywordType = substr($rpTriggererElementId, strrpos($rpTriggererElementId, "_") + 1); //item00_keywordsText_who; //$_REQUEST['keywordType'];
		// echo $keywordType ."<br>";
		if($keywordType == 'who') {
			$rootNodeLeftIndex = $KEYWORD_LEFT_INDEX_WHO;
			$rootNodeRightIndex = $KEYWORD_RIGHT_INDEX_WHO;
			if ($rpSearchText == "") $rpParentId = $KEYWORD_ID_WHO;
		} else if ($keywordType == 'action') {
			$rootNodeLeftIndex = $KEYWORD_LEFT_INDEX_ACTION;
			$rootNodeRightIndex = $KEYWORD_RIGHT_INDEX_ACTION;			
			if ($rpSearchText == "") $rpParentId = $KEYWORD_ID_ACTION;
		} else if ($keywordType == 'what') {
			$rootNodeLeftIndex = $KEYWORD_LEFT_INDEX_WHAT;
			$rootNodeRightIndex = $KEYWORD_RIGHT_INDEX_WHAT;
			if ($rpSearchText == "") $rpParentId = $KEYWORD_ID_WHAT;
		} else if ($keywordType == 'whom') {
			$rootNodeLeftIndex = $KEYWORD_LEFT_INDEX_WHOM;
			$rootNodeRightIndex = $KEYWORD_RIGHT_INDEX_WHOM;
			if ($rpSearchText == "") $rpParentId = $KEYWORD_ID_WHOM;
		} else if ($keywordType == 'when') {
			$rootNodeLeftIndex = $KEYWORD_LEFT_INDEX_WHEN;
			$rootNodeRightIndex = $KEYWORD_RIGHT_INDEX_WHEN;
			if ($rpSearchText == "") $rpParentId = $KEYWORD_ID_WHEN;
		} else if ($keywordType == 'where') {
			$rootNodeLeftIndex = $KEYWORD_LEFT_INDEX_WHERE;
			$rootNodeRightIndex = $KEYWORD_RIGHT_INDEX_WHERE;
			if ($rpSearchText == "") $rpParentId = $KEYWORD_ID_WHERE;
		} else if ($keywordType == 'subject') {
			$rootNodeLeftIndex = $KEYWORD_LEFT_INDEX_SUBJECT;
			$rootNodeRightIndex = $KEYWORD_RIGHT_INDEX_SUBJECT;
			if ($rpSearchText == "") $rpParentId = $KEYWORD_ID_SUBJECT;
		} else if ($keywordType == 'event') {
			$rootNodeLeftIndex = $KEYWORD_LEFT_INDEX_WHEN;
			$rootNodeRightIndex = $KEYWORD_RIGHT_INDEX_WHEN;
			if ($rpSearchText == "") $rpParentId = $KEYWORD_ID_WHEN;
		} else if ($keywordType == 'keyword') {
			$rootNodeLeftIndex = $KEYWORD_LEFT_INDEX_WHO;
			$rootNodeRightIndex = $KEYWORD_RIGHT_INDEX_WHEN;
			// if ($rpSearchText == "") $rpParentId = $KEYWORD_ID_SUBJECT;
		}
		
		/* we dont use this in v8 as of now
		if($rpParentId != "") { //fetch only immediate children
			$dbQueryMatchingKeywords = "SELECT  keyword_id AS id,
												name_tree AS text
										FROM $DB_KEYWORDS_TABLE_NAME
										WHERE parent_keyword_id = $rpParentId
												AND left_index  >= $rootNodeLeftIndex
												AND right_index <= $rootNodeRightIndex
										ORDER BY name_tree, title
										LIMIT $KEYWORD_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT";
		} else  */
		{
			$rpSearchText = trim(str_replace("*","%",$rpSearchText),' ');
			$parentNameTree = "";
			$rpSearchTextArr = explode(">",$rpSearchText);
			if(count($rpSearchTextArr) > 1) { // if there is one or more nodes in parent hierarchy i.e. if NOT the root node
				$parentNameTree = trim(substr($rpSearchText, 0, strrpos($rpSearchText, ">")),' ');
			}
			if(count($rpSearchTextArr) == 1) { // initial search based on partial keyword text
				if ($keywordType == 'subject') {
					//concat('[$rpSearchText]-',title) AS text
					$dbQueryMatchingKeywords = "SELECT  subject_id AS id,
														title AS text
											FROM subjects
											WHERE identical_words LIKE '%$rpSearchText%' OR title LIKE '%$rpSearchText%' 
											ORDER BY  title";
				} else {
					$dbQueryMatchingKeywords = "SELECT  keyword_id AS id,
														name_tree AS text
												FROM $DB_KEYWORDS_TABLE_NAME
												WHERE name_tree LIKE '%$rpSearchText%'
														AND left_index  >= $rootNodeLeftIndex
														AND right_index <= $rootNodeRightIndex
												ORDER BY  name_tree
												LIMIT $KEYWORD_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT";
					/*
					$dbQueryMatchingKeywords = "SELECT  keyword_id AS id,
														concat(title, name_tree_display) AS text
												FROM $DB_KEYWORDS_TABLE_NAME
												WHERE name_tree LIKE '%$rpSearchText%'
														AND left_index  >= $rootNodeLeftIndex
														AND right_index <= $rootNodeRightIndex
												ORDER BY  title, name_tree
												LIMIT $KEYWORD_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT";
					*/
					/*
					$dbQueryMatchingKeywords  = "SELECT id, kwtext AS text 
												FROM
												(
													SELECT  keyword_id AS id,
															title AS kwtext
													FROM $DB_KEYWORDS_TABLE_NAME
													WHERE name_tree LIKE '%$rpSearchText%'
															AND left_index  >= $rootNodeLeftIndex
															AND right_index <= $rootNodeRightIndex
													AND title NOT IN (SELECT title FROM keywords_temp GROUP BY title having count(*) >= 2 )
												UNION ALL
													SELECT  keyword_id AS id,
															concat(title,' (', name_tree,')') AS kwtext
													FROM $DB_KEYWORDS_TABLE_NAME
													WHERE name_tree LIKE '%$rpSearchText%'
															AND left_index  >= $rootNodeLeftIndex
															AND right_index <= $rootNodeRightIndex
													AND title IN (SELECT title FROM keywords_temp GROUP BY title having count(*) >= 2 )
												) kws
												ORDER BY text
												LIMIT $KEYWORD_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT";
					*/
				}
			} else {
				$dbQueryMatchingKeywords = "SELECT  keyword_id AS id,
													name_tree AS text
											FROM $DB_KEYWORDS_TABLE_NAME
											WHERE title LIKE '%$rpSearchText%'
													AND left_index  >= $rootNodeLeftIndex
													AND right_index <= $rootNodeRightIndex
													AND parent_keyword_id IN
														(
														SELECT keyword_id AS parent_keyword_id
														FROM $DB_KEYWORDS_TABLE_NAME
														WHERE title = '$parentNameTree'
														)
											ORDER BY  name_tree, title
											LIMIT $KEYWORD_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT";
			}
			
		}
 		//echo $dbQueryMatchingKeywords;
		$dbResultMatchingKeywords = mysql_query($dbQueryMatchingKeywords, $con) or mysql_error();
		$dbRowArrayMatchingKeywords = array();
		//echo $dbQueryMatchingKeywords;
		
		while ($dbRowArrayMatchingKeywords[] = mysql_fetch_assoc($dbResultMatchingKeywords));
		array_pop($dbRowArrayMatchingKeywords);
		// array_push($dbRowArrayMatchingKeywords, array("keyword_id" => "1", "title" => "મહિમા"));
		// array_push($dbRowArrayMatchingKeywords, array("keyword_id" => "2", "title" => "મહિમા"));
		
		// $output = "";
		// if (count($dbRowArrayMatchingKeywords)>0) { 
			// if ($keywordType == 'subject') $output = "";
			// else $output = "<strong class='small' style='padding-left: 5px; cursor: default;'>Keywords:</strong>";
		// }
		// foreach($dbRowArrayMatchingKeywords as $dbRowMatchingKeywords)
		// {
			// $caption = str_replace(lcfirst($rpSearchText),$AUTOSUGGEST_LIST_FORMATTING_TAG_START.lcfirst($rpSearchText).$AUTOSUGGEST_LIST_FORMATTING_TAG_END, $dbRowMatchingKeywords['title']);
			// $caption = str_replace(ucfirst($rpSearchText),$AUTOSUGGEST_LIST_FORMATTING_TAG_START.ucfirst($rpSearchText).$AUTOSUGGEST_LIST_FORMATTING_TAG_END, $caption);
			// $output .= "<li tabindex='-2' class='suggestionLi' id='" .$rpTriggererElementId. "' value='" .$dbRowMatchingKeywords['keyword_id']. "' >" .$caption. "</li>";
		// }
		// echo $output."";
		$ret = array();
		//$ret['results'] = $dbRowArrayMatchingKeywords;
		
		// echo $dbQueryMatchingKeywords;
		//if ($keywordType == 'where') { 
		
		//[[[[[[[[[[[[[
		//include 'LocationSuggestions.php'; //append Location Search Results
		if ($keywordType == 'keyword') {
		
			if($rpParentId == "") //this this page is included in other page, for example : KeywordSuggestions.php
				$rpParentId = $_REQUEST['parentId'];
			//if($rpTriggererElementId != "")
			//	$rpTriggererElementId = $_REQUEST['triggererElementId'];
			$rpSearchText = trim($_REQUEST['searchText']);
			if ($rpSearchText == "" && $rpParentId == "")
				$rpParentId = "0";
			// $rpParentId = "480380,482382"; //for testing - WORKING
			// { start: new logic for locations in keywordlines
				$lft_rgt_clause = "";
				$rpParentIdArray = explode(",",$rpParentId);
				for($indx=0; $indx<count($rpParentIdArray); $indx++) {
					if($lft_rgt_clause == "") // first time
						$lft_rgt_clause .= " ( lft > $DB_INOTES_SCHEMA_NAME.getLeftRightindex($rpParentIdArray[$indx],'left') AND rgt < $DB_INOTES_SCHEMA_NAME.getLeftRightindex($rpParentIdArray[$indx],'right') )";
					else
						$lft_rgt_clause .= " OR ( lft > $DB_INOTES_SCHEMA_NAME.getLeftRightindex($rpParentIdArray[$indx],'left') AND rgt < $DB_INOTES_SCHEMA_NAME.getLeftRightindex($rpParentIdArray[$indx],'right') )";
				}
			if($rpParentId != "") { //fetch only all children - NOT WORKING
				$dbQueryMatchingLocs = "SELECT  location_id AS id,
												name_tree AS text
										FROM $DB_LOCATIONS_TABLE_NAME 
										WHERE location_name_e LIKE '$rpSearchText%' 
										  AND ($lft_rgt_clause)
										ORDER BY name_tree 
										LIMIT $LOCATION_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT";
			}
			// } end: new logic for locations in keywordlines
			/*if($rpParentId != "") { //fetch only immediate children - WOKRING
				$dbQueryMatchingLocs = "SELECT  location_id AS id,
												name_tree AS text
										FROM $DB_LOCATIONS_TABLE_NAME 
										WHERE location_name_e LIKE '$rpSearchText%' 
										  AND parent_id in ($rpParentId)
										ORDER BY name_tree 
										LIMIT $LOCATION_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT";
			}*/ else  {
				$rpSearchText = str_replace("*","%",$rpSearchText);
				$parentNameTree = "";
				$rpSearchTextArr = explode(">",$rpSearchText); //
				if(count($rpSearchTextArr) > 1) { // if there is one or more nodes in parent hierarchy i.e. if NOT the root node
					$parentNameTree = trim(substr($rpSearchText, 0, strrpos($rpSearchText, ">")),' ');
				}
				//the count shall be always 1 in inotes_version_08
				if(count($rpSearchTextArr) <= 1) { // initial search based on partial location name serach text
					$dbQueryMatchingLocs = "SELECT  location_id AS id,
													name_tree AS text
												FROM $DB_LOCATIONS_TABLE_NAME
												WHERE location_name_e LIKE '$rpSearchText%'
												ORDER BY name_tree
												LIMIT $LOCATION_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT";
				} else {
					$dbQueryMatchingLocs = "SELECT  location_id AS id,
													name_tree AS text
												FROM $DB_LOCATIONS_TABLE_NAME
												WHERE name_tree LIKE '$rpSearchText%' 
												  AND parent_id IN (
																		SELECT location_id AS parent_id 
																		FROM $DB_LOCATIONS_TABLE_NAME
																		WHERE name_tree = '$parentNameTree'
																	)
												ORDER BY name_tree
												LIMIT $LOCATION_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT";
				}
			}
			// echo "$dbQueryMatchingLocs";
			
			$dbResultMatchingLocs = mysql_query($dbQueryMatchingLocs, $con) or die('error: ' .mysql_error() );
			$dbRowArrayMatchingLocs = array();
			while ($dbRowArrayMatchingLocs[] = mysql_fetch_assoc($dbResultMatchingLocs)); //{
			array_pop($dbRowArrayMatchingLocs);
					
		}
					// $dbQueryTmp = "SELECT 1 AS id,'".$rpParentId."' as text";
					// $dbResultTmp = mysql_query($dbQueryTmp, $con) or mysql_error();
					// $dbRowTmp = array();
					// while ($dbRowTmp[] = mysql_fetch_assoc($dbResultTmp));
					// array_pop($dbRowTmp);
					//itemId = 123648
		//]]]]]]]]]]]]]
		
			//global $dbResultMatchingLocs;
		if($keywordType == 'location')
			$ret['results'] = $dbRowArrayMatchingLocs;
		elseif($keywordType == 'keyword') {
			if(count($dbRowArrayMatchingKeywords) > 0 && $dbRowArrayMatchingKeywords[0] != null)
				$ret['results'] = array_merge($dbRowArrayMatchingKeywords,$dbRowArrayMatchingLocs);
			else
				$ret['results'] = $dbRowArrayMatchingLocs;
		} elseif($keywordType == 'subject' || $keywordType == 'event') {
			$ret['results'] = $dbRowArrayMatchingKeywords;
		}
		
		//$ret['results'] = $dbRowTmp;
		//}
		//if ($keywordType == 'who' OR $keywordType == 'whom') {
		//	include 'SaintSuggestions.php';//append Saints Search Results
		//	include 'PeopleSuggestions.php';//append People Search Results
		//}
		echo json_encode ($ret);
?>