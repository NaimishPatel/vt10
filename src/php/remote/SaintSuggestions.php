<?php 
	opcache_reset();
	// require_once '../../../api/session.php';
	require_once '../../../api/connection.php';
	require_once '../include/PHP4remote.php';
	require_once '../../../api/SqlQueryBuilder.class.php';
?>
<?php
/*
{Version}:: v07
{Purpose}:: 
	It returns a list of suggested people based on search string.
{Description}:: 
	It compares searchText with ipeople.master_info.first_name and ipeople.master_info.last_name
	
*/
		$rpSearchText = $_REQUEST['searchText'];
		if($rpTriggererElementId != "") //this this page is included in other page, for example : KeywordSuggestions.php
			$rpTriggererElementId = $_REQUEST['triggererElementId'];		
		//echo "$rpSearchText";
		$rpSearchText = str_replace("*","%",$rpSearchText);
		$vars = array("~~rpSearchText~~" => $rpSearchText);
		$dbQueryMatchingPeople = substituteTokensIn($QUERY_GET_SAINT_SUGGESTIONS, $vars);
		//echo "$rpSearchText";
		//echo "<br>$dbQueryMatchingPeople";
		$dbResultMatchingPeople = mysql_query($dbQueryMatchingPeople, $con) or mysql_error();
		$dbRowArrayMatchingPeople = array();
		while ($dbRowArrayMatchingPeople[] = mysql_fetch_assoc($dbResultMatchingPeople));
		array_pop($dbRowArrayMatchingPeople);
		// $output = "";
		// if (count($dbRowArrayMatchingPeople)>0) $output = "<strong class='small' style='padding-left: 5px; cursor: default;'>Saints:</strong>";
		// foreach($dbRowArrayMatchingPeople as $dbRowMatchingPeople)
		// {
			// $output .= "<li tabindex='-2' class='suggestionLi' id='" .$rpTriggererElementId."' value='" .$dbRowMatchingPeople['saint_auto_id']. "' >" .str_ireplace($rpSearchText,$AUTOSUGGEST_LIST_FORMATTING_TAG_START.$rpSearchText.$AUTOSUGGEST_LIST_FORMATTING_TAG_END, $dbRowMatchingPeople['saint_name_e']). "</li>";
		// } 
		// echo $output."";
		$ret = array();
		$ret['results'] = $dbRowArrayMatchingPeople;
		echo json_encode ($ret);
?>