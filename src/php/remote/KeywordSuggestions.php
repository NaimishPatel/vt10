<?php 
	/*opcache_reset();*/
	// require_once '../../../api/session.php';
	require_once '../../../api/connection.php';
	require_once '../include/PHP4remote.php';
	require_once '../../../api/SqlQueryBuilder.class.php';
?>
<?php
/*
{Version}:: v07
{Purpose}:: 
	It returns a list of suggested Keywords based on search string.
{Description}:: 
	There could be two types of search types. 
	[1]= Based on partial keyword name. For example : Gujar%. 
		In this type, search text wouldn't any contain '>'.
		For this search type, we'll match search text with inotes.keywords.title 
		and inotes.keyword_synonames.title.
		(ilocations.location_synonames.title is for future use)
	[2]= Based on parent name tree and partial keyword name. For example : Asia > India > Gujar%. For this search type, 
		In this type, search text would contain one or more '>'.
		For this search type, we'll match search text with inotes.keywords.title and inotes.keyword_synonames.title
		(inotes.keyword_synonames.title is for future use)
*/
		//$rpParentId = $_REQUEST['parentId'];
		$rpTriggererElementId = $_REQUEST['triggererElementId'];
		$rpSearchText = trim($_REQUEST['searchText']);
		
		$keywordType = substr($rpTriggererElementId, strrpos($rpTriggererElementId, "_") + 1); //item00_keywordsText_who; //$_REQUEST['keywordType'];
		if ($keywordType == 'subject') {
		$dbQueryMatchingKeywords = "SELECT  subject_id AS id,
											title AS text
								FROM subjects
								WHERE title LIKE '%$rpSearchText%' or identical_words like '%$rpSearchText%' 
								ORDER BY  title limit 20";
		} elseif ($keywordType == 'event') {
		$dbQueryMatchingKeywords = "SELECT  event_id AS id,
											title AS text
								FROM events
								WHERE title LIKE '%$rpSearchText%' 
								ORDER BY  title limit 20";
		} elseif ($keywordType == 'vakta') {
		$dbQueryMatchingKeywords = "SELECT  person_id as id,
											concat(first_name, ' ', last_name, IF(other_names is null or other_names = '','',concat('(', other_names, ')')))   as text
								FROM ipeople.person_master
								WHERE first_name LIKE '$rpSearchText%' or  other_names  LIKE '$rpSearchText%'
								ORDER BY  first_name limit 20";
		}  else if ($keywordType == 'itemCategory') {
		$dbQueryMatchingKeywords = "SELECT  category_id as id,
											name_g  as text
								FROM categories
								WHERE name_t LIKE '%$rpSearchText%' or  name_e  LIKE '%$rpSearchText%'
								ORDER BY  name_e limit 20";
		}
		else { 
		$dbQueryMatchingKeywords = "SELECT  keyword_id AS id,
											title AS text
								FROM keywords
								WHERE title LIKE '%$rpSearchText%' 
								ORDER BY  title limit 100";
		}
 		//echo $dbQueryMatchingKeywords;
		$dbResultMatchingKeywords = mysql_query($dbQueryMatchingKeywords, $con) or mysql_error();
		//echo $dbQueryMatchingKeywords;
		$dbRowArrayMatchingKeywords = array();
		//echo $dbQueryMatchingKeywords;
		
		while ($dbRowArrayMatchingKeywords[] = mysql_fetch_assoc($dbResultMatchingKeywords));
		array_pop($dbRowArrayMatchingKeywords);
		//array_push($dbRowArrayMatchingKeywords, array("keyword_id" => "1", "title" => "મહિમા૧"));
		//array_push($dbRowArrayMatchingKeywords, array("keyword_id" => "2", "title" => "મહિમા૨"));
		
		$errorLines = array();
		$errorLineIndx=0;
		$errorLines[$errorLineIndx]['code'] = '1001';
		$errorLines[$errorLineIndx++]['text'] = 'keywordType='.$keywordType;
		$errorLines[$errorLineIndx]['code'] = '1002';
		$errorLines[$errorLineIndx++]['text'] = 'rpSearchText='.$rpSearchText;	
		$errorLines[$errorLineIndx]['code'] = '1003';
		$errorLines[$errorLineIndx++]['text'] = 'dbQueryMatchingKeywords='.$dbQueryMatchingKeywords;	
			
		$ret = array();
		$ret['results'] =  $dbRowArrayMatchingKeywords;
		$ret['errors'] = $errorLines;
		
		
		//echo '<br>'.$dbQueryMatchingKeywords;
		
		//echo '<br>'.$ret['results'];
		
		//echo '<br>'.json_encode ($ret);
		
		echo json_encode ($ret);
?>