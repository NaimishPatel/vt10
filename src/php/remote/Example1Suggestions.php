<?php 
	opcache_reset();
	// require_once '../../../api/session.php';
	require_once '../../../api/connection.php';
	require_once '../include/PHP4remote.php';
	require_once '../../../api/SqlQueryBuilder.class.php';
?>
<?php
/*
{Version}:: v10
{Purpose}:: 
	It returns a list of suggested records based on search string, table, idColumn and textColumn.
{Description}:: 
	It compares searchText with data in the textColumn
	
*/
		$rpIdColumnName = trim($_REQUEST['idColumnName']);
		$rpTextColumnName = trim($_REQUEST['textColumnName']);
		$rpTableName = trim($_REQUEST['tableName']);
		$rpSearchText = trim($_REQUEST['searchText']);
		
		$rpSearchText = str_replace("*","%",$rpSearchText);
		
		$vars = array("~~idColumnName~~" => $rpIdColumnName, 
					"~~textColumnName~~" => $rpTextColumnName, 
					"~~tableName~~" => $rpTableName, 
					"~~rpSearchText~~" => $rpSearchText);
		$dbQueryMatchingRecords = substituteTokensIn($QUERY_GET_GENERIC_SUGGESTIONS, $vars);
		//echo "$rpSearchText";
		// echo "<br>$dbQueryMatchingRecords";
		$dbResultMatchingRecords = mysql_query($dbQueryMatchingRecords, $con) or mysql_error();
		$dbRowArrayMatchingRecords = array();
		while ($dbRowArrayMatchingRecords[] = mysql_fetch_assoc($dbResultMatchingRecords));
		array_pop($dbRowArrayMatchingRecords);
		$ret = array();
		$ret['results'] = $dbRowArrayMatchingRecords;
		echo json_encode ($ret);
?>