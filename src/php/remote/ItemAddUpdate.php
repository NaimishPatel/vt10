<?php 
	opcache_reset();
	require_once '../../../api/session.php';
	require_once '../../../api/connection.php';
	require_once '../include/PHP4remote.php';
	require_once '../../../api/SqlQueryBuilder.class.php';
?>
<?php


		$itemJsonStringFromRequestDummy = '[{"itemId":"","parentItemId":"","itemCategory1":"6.C","itemCategory2":"6","itemTitle":"test descp 1","technicalQuality":"3","technicalNotes":"bad","urlOrFileFolderPath":"VideoLibrary/To Sort/JAY JAY AKSHARPURUSHOTTAM NII to show.MP4","itemType1":"3","itemType2":"09","itemSource":"test descp 1 src","confidentiality":"5","textArray":[{"additionalDescp":"test descp 1 descrrrrrrrrrrrrrrrrrrrrrrrrrrrrr"}],"eventDateArray":[{"eventStartDateYYYY":"1975","eventStartDateMM":"00","eventStartDateDD":"00","eventEndDateYYYY":"1970","eventEndDateMM":"00","eventEndDateDD":"00"}],"keywordLineArray":[{"keywordLine":"1","keywordIndex":"1","keywordId":"test descp 1 kwl","keywordText":"test descp 1 kwl"}],"subjectArray":[{"subjectLine":"1","subjectIndex":"1","subjectId":"3507","subjectText":"A : "}],"eventArray":[{"eventId":"1904","eventText":"1985 - Gunatitanand 200"}],"languageArray":[{"languageId":"3","languageText":"English"}],"locationArray":[{"locationId":"null","locationText":"banglo","locationTree":"banglo"}]}]';
		/*
	'[ { '
	.'			  "itemId": "" '
	.'			, "parentItemId": "123" '
	.'			, "itemCategory": "1" '
	.'			, "itemTitle": "..Satsang Darshan-95th JanmaJayanti" '
	.'			, "technicalQuality": "4" '
	.'			, "technicalNotes": "voice and audio okay " '
	.'			, "eventDateArray": '
	.'			[ '
	.'				{	  "eventStartDateYYYY":"2016", "eventStartDateMM":"12", "eventStartDateDD":"19" '
	.'					, "eventEndDateYYYY":"2016", "eventEndDateMM":"12", "eventEndDateDD":"19" '
	.'				}, '
	.'				{	  "eventStartDateYYYY":"2017", "eventStartDateMM":"12", "eventStartDateDD":"19" '
	.'					, "eventEndDateYYYY":"2017", "eventEndDateMM":"12", "eventEndDateDD":"19" '
	.'				} '
	.'			] '
	.'			, "clipStartTime": "01:30:15" '
	.'			, "clipEndTime": "01:40:45" '
	.'			, "urlOrFileFolderPath": "c:/mediaStorage/2015/12/19" '
	.'			, "fileFolderName": "AGMZ1467.avi" '
	.'			, "itemType": "2" '
	.'			, "itemUniqueCode": "bapsSite1Item346" '
	.'			, "itemSource": "3" '
	.'			, "confidentiality": "4" '
	.'			, "accesssibility": "5" '
	.'			, "language": "543385" '
	.'			, "additionalDescp": "1. blah blah blah \\n 2. blah blah blah \\n 3. blah blah blah..." '
	.'			, "subjectArray": '
	.'			[ '
	.'				 { "subjectId": "4001", "subjectText": "Nishkam Vartman1", "subjectRank": "4" }, '
	.'				 { "subjectId": "4002", "subjectText": "Nirlobh Vartman2", "subjectRank": "4" }, '
	.'				 { "subjectId": "4003", "subjectText": "Nirman Vartman3", "subjectRank": "4" }, '
	.'				 { "subjectId": "4004", "subjectText": "Nihsheh Vartman4", "subjectRank": "4" }, '
	.'				 { "subjectId": "4005", "subjectText": "Niswad Vartman5", "subjectRank": "4" } '
	.'			] ' 
 	.'			, "keywordLineArray": '
	.'			[    {"whoId":"10","actionId":"1711","whatId":"","whomId":"","whereId":"10105275","whenId":"1898","whoText":"Pramukh Swami Maharaj","actionText":"smiling","whatText":"","whomText":"Subhasbhai","whereText":"salangpur, BAPS Mandir","whenText":"Shangar"}'  
	.'				,{ '
	.'					  "whoId": "1009", "actionId": "1008", "whatId": "1007", "whomId": "1006","whereId": "1005", "whenId": "0" '
	.'					, "whoText": "Obama", "actionText": "doing", "whatText": "dandvat", "whomText": "PramukhSwamiMaharaj", "whereText": "Sarangpur YagnapurushVadi", "whenText": "PSM 95th Janmajayanti" '
	.'					, "keywordLineRank": "5" '
	.'				} '
	.'			] '
	.'} ]'; */
	//$itemJsonStringFromRequestDummy = '[{"itemId":"123652","parentItemId":"","itemCategory":"011","itemTitle":"satya test3","technicalQuality":"3","technicalNotes":"","urlOrFileFolderPath":"satya test3.dat","itemType":"1","itemSource":"","confidentiality":"2","accessibility":"08","language":"001","additionalDescp":"satya test3 - additional details","eventDateArray":[{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"05","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"05"}],"keywordLineArray":[{"keywordLine":"1","keywordIndex":"1","keywordId":"10","keywordText":"Pramukh Swami Maharaj"},{"keywordLine":"1","keywordIndex":"2","keywordId":"1840","keywordText":"aarti"},{"keywordLine":"1","keywordIndex":"3","keywordId":"10105275","keywordText":"Salangpur, BAPS Mandir"}],"subjectArray":[{"subjectLine":"1","subjectIndex":"1","subjectId":"3514","subjectText":"D-4 : "}],"eventArray":[{"eventLine":"1","eventIndex":"1","eventId":"1900","eventText":"Sandhya"}],"locationArray":[{"locationId":"518278","locationText":"Salangpur","locationTree":"Salangpur"}]}]';
	//for responseJson
	$errorLines = array();
	$errorLineIndx=0;
	$inotesMessages = array();
	$inotesMessagesIndx=0;
	//for responseJson
	
	//$itemJsonStringFromRequestDummy = '[{"itemId":"","parentItemId":"","itemCategory":"","itemTitle":"..Satsang Darshan-95th JanmaJayanti","technicalQuality":"","technicalNotes":"voice and audio okay ","urlOrFileFolderPath":"c:|mediaStorage|2015|12|19","itemType":"2","itemSource":"3","confidentiality":"4","accesssibility":"","language":"","additionalDescp":"1. blah blah blah 2. blah blah blah 3. blah blah blah...","eventDateArray":[{"eventStartDateYYYY":"2016","eventStartDateMM":"12","eventStartDateDD":"19","eventEndDateYYYY":"2016","eventEndDateMM":"12","eventEndDateDD":"19"},{"eventStartDateYYYY":"2017","eventStartDateMM":"12","eventStartDateDD":"19","eventEndDateYYYY":"2017","eventEndDateMM":"12","eventEndDateDD":"19"}],"keywordLineArray":[{"whoId":"10","actionId":"1711","whatId":"1007","whomId":"5235235","whereId":"10105275","whenId":"1898","whoText":"Pramukh Swami Maharaj","actionText":"smiling","whatText":"temp-test","whomText":"Subhashbhai","whereText":"Salangpur, BAPS Mandir","whenText":"Shangar"},{"whoId":"1009","actionId":"1008","whatId":"1007","whomId":"1006","whereId":"1005","whenId":"5353","whoText":"Obama","actionText":"doing","whatText":"dandvat","whomText":"PramukhSwamiMaharaj","whereText":"Sarangpur YagnapurushVadi","whenText":"PSM 95th Janmajayanti"}],"subjectArray":[{"subjectId":"4001","subjectText":"Nishkam Vartman1"},{"subjectId":"4002","subjectText":"Nirlobh Vartman2"},{"subjectId":"4003","subjectText":"Nirman Vartman3"},{"subjectId":"4004","subjectText":"Nihsheh Vartman4"},{"subjectId":"4005","subjectText":"Niswad Vartman5"}]}]';
	//$itemJsonStringFromRequest = '[{"itemId":"","parentItemId":"","itemCategory":"1","itemTitle":"title123","technicalQuality":"1","technicalNotes":"notes123","urlOrFileFolderPath":"c:\\\\man","itemType":"1","itemSource":"Mumbai","confidentiality":"1","accesssibility":"1","language":"1","additionalDescp":"descp123","eventDateArray":[{"eventStartDateYYYY":"2015","eventStartDateMM":"01","eventStartDateDD":"01","eventEndDateYYYY":"2015","eventEndDateMM":"01","eventEndDateDD":"02"},{"eventStartDateYYYY":"2015","eventStartDateMM":"01","eventStartDateDD":"03","eventEndDateYYYY":"2015","eventEndDateMM":"01","eventEndDateDD":"05"}],"keywordLineArray":[{"whoId":"","actionId":"","whatId":"","whomId":"","whereId":"","whenId":"","whoText":"psm","actionText":"doing","whatText":"puja","whomText":"","whereText":"","whenText":""},{"whoId":"91","actionId":"1745","whatId":"996","whomId":"","whereId":"","whenId":"","whoText":"BAPS Swaminarayan","actionText":"waving","whatText":"flag","whomText":"","whereText":"","whenText":""}],"subjectArray":[{"subjectId":"2224","subjectText":""},{"subjectId":"2226","subjectText":""}]}]';
	$itemJsonStringFromRequest = $_REQUEST['itemJsonString'];
	//echo $itemJsonStringFromRequest;
	 if($itemJsonStringFromRequest == null)
		$itemJsonStringFromRequest = $itemJsonStringFromRequestDummy;
	$itemJsonArray = json_decode($itemJsonStringFromRequest,true);
	//echo "<br>item Count = ".count($itemJsonArray);
	$itemIdUnderProcess = $itemJsonArray[0]['itemId'];
	//echo "<br>item[0]['itemTitle'] = ".$itemJsonArray[0]['itemTitle'];
	if($itemIdUnderProcess == null OR $itemIdUnderProcess == "" OR $itemIdUnderProcess == "0")
		$actionType = "INSERT";
	else
		$actionType = "UPDATE";
	//build INSERT SQL for inotes.items
	$sqlQB = new SqlQueryBuilder();
	$sqlQB->showMessages = false;
	$sqlQB->showErrors = true;
	
	//build INSERT SQL for inotes.items
 	$sqlQB->resetAll();
	$sqlQB->queryType = $actionType;
	$sqlQB->setTable($DB_ITEMS_TABLE_NAME);
	$jsonArray = $itemJsonArray;

	$PROCESS_OTHER_ITEM_TABLES = true;
	if($actionType == "UPDATE") {
		$itemIdColumnDetails = array();
		$itemIdColumnDetails['columnName'] = 'item_id';
		$itemIdColumnDetails['columnValue'] = $itemIdUnderProcess;
		$itemIdColumnDetails['columnType'] = 'number';
	}
	$itemInsertUpdateSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemUItoDBMappingArray, $itemColumnsDataTypeArray ); 
	// there will be always one record of an item
	// echo "$itemInsertUpdateSQLsArray[0]";
	mysql_query($itemInsertUpdateSQLsArray[0], $con);
	$lastInsertId = mysql_insert_id($con);
	//echo "<br>lastInsertId::".$lastInsertId."<br>";
	//$inotesMessages[$inotesMessagesIndx]['code']  = "2001";
	//$inotesMessages[$inotesMessagesIndx++]['text']  = "SQL=$itemInsertUpdateSQLsArray[0]";
	if($actionType == "INSERT") {
		if ($lastInsertId > 0) {
			$itemIdUnderProcess = $lastInsertId;
		} else {
			$errorLines[$errorLineIndx]['code'] = '1001';
			$errorLines[$errorLineIndx++]['text'] = $ERR_DB_INSERT_UPDATE.mysql_error();
		}

	}
	if($itemIdUnderProcess != null AND $itemIdUnderProcess != "" AND $itemIdUnderProcess != "0") { // there was some error in INSERT or UPDATE, so no meaning to process other tables
		
		$itemIdColumnDetails = array();
		$itemIdColumnDetails['columnName'] = 'item_id';
		$itemIdColumnDetails['columnValue'] = $itemIdUnderProcess;
		$itemIdColumnDetails['columnType'] = 'number';
		
		//echo "<br>-----------------------------------------------------------------processing other tables-----------------------------------------------------------------<br>";
		
		//********************************************************************************************************************************************************************
		//delete old data from item_locations table
		$vars = array("~~table~~" => $DB_ITEMLOCATIONS_TABLE_NAME, "~~itemId~~" => $itemIdUnderProcess);
		$itemTablesDeleteSQLsArray[] = substituteTokensIn($QUERY_DELETE_ITEM_FROM_TABLE, $vars);
		
		//build INSERT SQL with new data for inotes.item_dates
		$sqlQB->resetAll();
		$sqlQB->queryType = "INSERT";
		$sqlQB->setTable($DB_ITEMLOCATIONS_TABLE_NAME);
		//$sqlQB->addColumnValuePair('item_id',$itemIdUnderProcess,"number");
		$jsonArray = $itemJsonArray[0]['locationArray'];
		$itemLocationInsertSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemLocationsUItoDBMappingArray, $itemLocationsColumnsDataTypeArray );
		//echo "<br>itemLocationInsertSQLs=$itemLocationInsertSQL";
		
		//********************************************************************************************************************************************************************
		//delete old data from item_events table
		$vars = array("~~table~~" => $DB_ITEMEVENTS_TABLE_NAME, "~~itemId~~" => $itemIdUnderProcess);
		$itemTablesDeleteSQLsArray[] = substituteTokensIn($QUERY_DELETE_ITEM_FROM_TABLE, $vars);
		
		//build INSERT SQL with new data for inotes.item_dates
		$sqlQB->resetAll();
		$sqlQB->queryType = "INSERT";
		$sqlQB->setTable($DB_ITEMEVENTS_TABLE_NAME);
		//$sqlQB->addColumnValuePair('item_id',$itemIdUnderProcess,"number");
		$jsonArray = $itemJsonArray[0]['eventArray'];
		$itemEventInsertSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemEventsUItoDBMappingArray, $itemEventsColumnsDataTypeArray );
		//echo "<br>itemLocationInsertSQLs=$itemLocationInsertSQL";
		
		//********************************************************************************************************************************************************************
		//delete old data from inotes.item_dates
		$vars = array("~~table~~" => $DB_ITEMDATES_TABLE_NAME, "~~itemId~~" => $itemIdUnderProcess);
		$itemTablesDeleteSQLsArray[] = substituteTokensIn($QUERY_DELETE_ITEM_FROM_TABLE, $vars);
		
		//build INSERT SQL with new data for inotes.item_dates
		$sqlQB->resetAll();
		$sqlQB->queryType = "INSERT";
		$sqlQB->setTable($DB_ITEMDATES_TABLE_NAME);
		//$sqlQB->addColumnValuePair('item_id',$itemIdUnderProcess,"number");
		$jsonArray = $itemJsonArray[0]['eventDateArray'];
		$itemDateInsertSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemDateUItoDBMappingArray, $itemDateColumnsDataTypeArray );
		//echo "<br>itemDateInsertSQLs=$itemDateInsertSQL";
		
		//********************************************************************************************************************************************************************
		//delete old data from inotes.item_keywordlines
		$vars = array("~~table~~" => $DB_ITEMLOCATIONS_TABLE_NAME, "~~itemId~~" => $itemIdUnderProcess);
		$itemTablesDeleteSQLsArray[] = substituteTokensIn($QUERY_DELETE_ITEM_FROM_TABLE, $vars);
		$itemTablesDeleteSQLsArray[] = "DELETE FROM $DB_ITEMKEYWORDS_TABLE_NAME WHERE item_id=$itemIdUnderProcess";
		
		//build INSERT SQL with new data for inotes.item_keywordlines
		$sqlQB->resetAll();
		$sqlQB->queryType = "INSERT";
		//echo "<br>$DB_ITEMKEYWORDS_TABLE_NAME=$DB_ITEMKEYWORDS_TABLE_NAME";
		$sqlQB->setTable($DB_ITEMKEYWORDS_TABLE_NAME);
		//$sqlQB->addColumnValuePair('item_id',$itemIdUnderProcess,"number");
		$jsonArray = $itemJsonArray[0]['keywordLineArray'];
		//echo "<br>var_dump(jsonArray)::<br>";
		//var_dump($jsonArray);
		//echo "<br>var_dump(itemKeywordLineUItoDBMappingArray)::<br>";
		//var_dump($itemKeywordLineUItoDBMappingArray);
		//echo "<br>var_dump(itemKeywordLineColumnsDataTypeArray)::<br>";
		//var_dump($itemKeywordLineColumnsDataTypeArray);
		$itemKeywordLineInsertSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemKeywordLineUItoDBMappingArray, $itemKeywordLineColumnsDataTypeArray ); 
		//echo "<br>itemKeywordLineInsertSQLs=$itemKeywordLineInsertSQLArray[0]";	
		//echo "<br>itemKeywordLineInsertSQLs=$itemKeywordLineInsertSQLArray[1]";	

		//********************************************************************************************************************************************************************
		//delete old data from inotes.item_subjects
		$vars = array("~~table~~" => $DB_ITEMSUBJECTS_TABLE_NAME, "~~itemId~~" => $itemIdUnderProcess);
		$itemTablesDeleteSQLsArray[] = substituteTokensIn($QUERY_DELETE_ITEM_FROM_TABLE, $vars);
		
		//build INSERT SQL with new data for inotes.item_subjects
		$sqlQB->resetAll();
		$sqlQB->queryType = "INSERT";
		$sqlQB->setTable($DB_ITEMSUBJECTS_TABLE_NAME);
		//$sqlQB->addColumnValuePair('item_id',$itemIdUnderProcess,"number");
		$jsonArray = $itemJsonArray[0]['subjectArray'];
		$itemSubjectInsertSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemSubjectsUItoDBMappingArray, $itemSubjectsColumnsDataTypeArray ); 
		//echo "<br>itemSubjectInsertSQLs=$itemSubjectInsertSQL"; 

		//********************************************************************************************************************************************************************
		//delete old data from inotes.item_languages
		$vars = array("~~table~~" => $DB_ITEMLANGUAGES_TABLE_NAME, "~~itemId~~" => $itemIdUnderProcess);
		$itemTablesDeleteSQLsArray[] = substituteTokensIn($QUERY_DELETE_ITEM_FROM_TABLE, $vars);
		
		//build INSERT SQL with new data for inotes.item_languages
		$sqlQB->resetAll();
		$sqlQB->queryType = "INSERT";
		$sqlQB->setTable($DB_ITEMLANGUAGES_TABLE_NAME);
		//$sqlQB->addColumnValuePair('item_id',$itemIdUnderProcess,"number");
		$jsonArray = $itemJsonArray[0]['languageArray'];
		$itemLanguageInsertSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemLanguagesUItoDBMappingArray, $itemLanguagesColumnsDataTypeArray ); 
		 
		//********************************************************************************************************************************************************************
		//delete old data from inotes.item_texts
		$vars = array("~~table~~" => $DB_ITEMTEXTS_TABLE_NAME, "~~itemId~~" => $itemIdUnderProcess);
		$itemTablesDeleteSQLsArray[] = substituteTokensIn($QUERY_DELETE_ITEM_FROM_TABLE, $vars);
		
		//build INSERT SQL with new data for inotes.item_texts
		$sqlQB->resetAll();
		$sqlQB->queryType = "INSERT";
		$sqlQB->setTable($DB_ITEMTEXTS_TABLE_NAME);
		//$sqlQB->addColumnValuePair('item_id',$itemIdUnderProcess,"number");
		$jsonArray = $itemJsonArray[0]['textArray'];
		$itemTextInsertSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemTextsUItoDBMappingArray, $itemTextsColumnsDataTypeArray ); 
		
		//********************************************************************************************************************************************************************
		//delete old data from inotes.item_notes
		$vars = array("~~table~~" => $DB_ITEM_NOTES_TABLE_NAME, "~~itemId~~" => $itemIdUnderProcess);
		$itemTablesDeleteSQLsArray[] = substituteTokensIn($QUERY_DELETE_ITEM_FROM_TABLE, $vars);
		
		//build INSERT SQL with new data for inotes.item_notes
		$sqlQB->resetAll();
		$sqlQB->queryType = "INSERT";
		$sqlQB->setTable($DB_ITEM_NOTES_TABLE_NAME);
		$jsonArray = $itemJsonArray[0]['itemNotes'];
		$itemNotesInsertSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemNotesUItoDBMappingArray, $itemNotesColumnsDataTypeArray );
		
		//********************************************************************************************************************************************************************		
		//delete old data from inotes.item_vakta
		$vars = array("~~table~~" => $DB_ITEM_VAKTA_TABLE_NAME, "~~itemId~~" => $itemIdUnderProcess);
		$itemTablesDeleteSQLsArray[] = substituteTokensIn($QUERY_DELETE_ITEM_FROM_TABLE, $vars);
		
		//build INSERT SQL with new data for inotes.item_notes
		$sqlQB->resetAll();
		$sqlQB->queryType = "INSERT";
		$sqlQB->setTable($DB_ITEM_VAKTA_TABLE_NAME);
		$jsonArray = $itemJsonArray[0]['itemVakta'];
		$itemVaktaInsertSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemVaktaUItoDBMappingArray, $itemVaktaColumnsDataTypeArray );
		
		//********************************************************************************************************************************************************************
		//itemTags
		$vars = array("~~table~~" => $DB_ITEM_TAG_TABLE_NAME, "~~itemId~~" => $itemIdUnderProcess);
		$itemTablesDeleteSQLsArray[] = substituteTokensIn($QUERY_DELETE_ITEM_FROM_TABLE, $vars);
		
		//build INSERT SQL with new data for inotes.item_notes
		$sqlQB->resetAll();
		$sqlQB->queryType = "INSERT";
		$sqlQB->setTable($DB_ITEM_TAG_TABLE_NAME);
		$jsonArray = $itemJsonArray[0]['itemTags'];
		$itemTagsInsertSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemTagsUItoDBMappingArray, $itemTagsColumnsDataTypeArray );
		
		//********************************************************************************************************************************************************************
		//delete old data from inotes.categories
		$vars = array("~~table~~" => $DB_ITEM_CATEGORIES_TABLE_NAME, "~~itemId~~" => $itemIdUnderProcess);
		$itemTablesDeleteSQLsArray[] = substituteTokensIn($QUERY_DELETE_ITEM_FROM_TABLE, $vars);
		
		//build INSERT SQL with new data for inotes.item_notes
		$sqlQB->resetAll();
		$sqlQB->queryType = "INSERT";
		$sqlQB->setTable($DB_ITEM_CATEGORIES_TABLE_NAME);
		$jsonArray = $itemJsonArray[0]['itemCategories'];
		$itemCategoriesInsertSQLsArray = $sqlQB->buildSQLsfromJsonArray($itemIdColumnDetails, $jsonArray, $itemCategoriesUItoDBMappingArray,$itemCategoriesColumnsDataTypeArray);
		
		//********************************************************************************************************************************************************************
		
		
	}
	
	if ($actionType == "INSERT")
		$allSQLs = array_merge($itemLocationInsertSQLsArray, $itemEventInsertSQLsArray, $itemDateInsertSQLsArray,$itemSubjectInsertSQLsArray,$itemKeywordLineInsertSQLsArray, $itemLanguageInsertSQLsArray, $itemTextInsertSQLsArray,$itemNotesInsertSQLsArray,$itemVaktaInsertSQLsArray,$itemCategoriesInsertSQLsArray,$itemTagsInsertSQLsArray);
	else 
		$allSQLs = array_merge($itemTablesDeleteSQLsArray, $itemLocationInsertSQLsArray, $itemEventInsertSQLsArray, $itemDateInsertSQLsArray, $itemKeywordLineInsertSQLsArray, $itemSubjectInsertSQLsArray, $itemLanguageInsertSQLsArray, $itemTextInsertSQLsArray,$itemNotesInsertSQLsArray,$itemVaktaInsertSQLsArray,$itemCategoriesInsertSQLsArray,$itemTagsInsertSQLsArray);
	
	for($indx=0; $indx < count($allSQLs) ; $indx++) {
		//echo "<br>allSQLs[indx]=".$allSQLs[$indx];
		mysql_query($allSQLs[$indx], $con);
		$inotesMessages[$inotesMessagesIndx]['code']  = "2001";
		$inotesMessages[$inotesMessagesIndx++]['text']  = "SQL=$allSQLs[$indx]";
		if(mysql_error()) 
		{
			$errorLines[$errorLineIndx]['code'] = '1001';
			$errorLines[$errorLineIndx++]['text'] = $ERR_DB_DELETE.mysql_error();
			//$errorLines[$errorLineIndx]['code'] = '1002';
			//$errorLines[$errorLineIndx++]['text'] = "SQL=$allSQLs[$indx]";
		}
	}
	if(count($errorLines) <= 0) {
		$vars = array("~~actionType~~" => $actionType, "~~itemId~~" => $itemIdUnderProcess);
		$inotesMessages[$inotesMessagesIndx]['code']  = "2001";
		$inotesMessages[$inotesMessagesIndx++]['text']  = substituteTokensIn($ITEM_ACTION_SUCCESS_HTML_TOKEN, $vars);
	}

	$responseJson = array();
	$responseJson['itemId'] = $itemIdUnderProcess;
	$responseJson['errors'] = $errorLines;
	$responseJson['messages'] = $inotesMessages;
	//return json_encode ($responseJson);
	echo json_encode ($responseJson);
	
?>