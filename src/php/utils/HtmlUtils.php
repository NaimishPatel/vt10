<?php
opcache_reset();

function getOptionsHtml($elementName) {
	global $ITEM_CATEGORY;
	global $ITEM_QUALITY;
	global $ITEM_CONFIDENTIALITY;
	global $ITEM_ACCESSIBILITY;
	global $ITEM_LANGUAGE;
	
	switch($elementName) {
		case 'itemCategory':
			return formOptionsHtml($ITEM_CATEGORY);
		case 'technicalQuality':
			return formOptionsHtml($ITEM_QUALITY);
		case 'confidentiality':
			return formOptionsHtml($ITEM_CONFIDENTIALITY);
		case 'accessibility':
			return formOptionsHtml($ITEM_ACCESSIBILITY);
		case 'language':
			return formOptionsHtml($ITEM_LANGUAGE);
	}
}

function formOptionsHtml($keyValueArray) {
	global $OPTIONS_TOKEN_HTML;

	$htmlStr = "";
	foreach ($keyValueArray as $key => $value) { 
		$htmlStrTemp = str_replace("~~key~~", $key, $OPTIONS_TOKEN_HTML);
		$htmlStrTemp = str_replace("~~value~~", $value, $htmlStrTemp);
		$htmlStrTemp = str_replace("~~selected~~", "", $htmlStrTemp);
		$htmlStr .= $htmlStrTemp;
	}
	return $htmlStr;
}

?>