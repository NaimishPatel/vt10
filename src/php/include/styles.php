<?php
opcache_reset();
?>

<!-- jQuery UI Core CSS -->
<link href="<?php echo $FILE_JQUERY_UI_CSS; ?>" rel="stylesheet">

<!-- Bootstrap Core CSS -->
<link href="<?php echo $FILE_BOOTSTRAP_CSS; ?>" rel="stylesheet">

<!-- Bootstrap Min CSS -->
<link href="<?php echo $FILE_BOOTSTRAP_MIN_CSS; ?>" rel="stylesheet">

<!-- Responsive Bootstrap CSS (X)-->
<link href="<?php echo $FILE_RESPONSIVE_BOOTSTRAP_MIN_CSS; ?>" rel="stylesheet">

<!-- font-awesome (Icon Pack) CSS -->
<link href="<?php echo $FILE_FONT_AWESOME_CSS; ?>" rel="stylesheet" type="text/css">

<!-- font-awesome (Icon Pack) Min CSS -->
<link href="<?php echo $FILE_FONT_AWESOME_MIN_CSS; ?>" rel="stylesheet" type="text/css">

<!-- Open Iconic Bootstrap CSS (Icon Pack) -->
<link href="<?php echo $FILE_OPEN_ICONIC_BOOTSTRAP_MIN_CSS; ?>" rel="stylesheet" type="text/css">

<!-- DataTables Bootstrap CSS -->
<link href="<?php echo $FILE_DATATABLES_BOOTSTRAP_MIN_CSS; ?>" rel="stylesheet">

<!-- jQuery DataTables CSS -->
<link href="<?php echo $FILE_JQUERY_DATATABLES_MIN_CSS; ?>" rel="stylesheet">

<!-- FixedColumns DataTables CSS (X)-->
<link href="<?php echo $FILE_FIXED_COLUMNS_DATATABLES_MIN_CSS; ?>" rel="stylesheet">

<!-- FixedHeader DataTables CSS -->
<link href="<?php echo $FILE_FIXED_HEADER_DATATABLES_MIN_CSS; ?>" rel="stylesheet">

<!-- Responsive DataTables CSS -->
<link href="<?php echo $FILE_RESPONSIVE_DATATABLES_MIN_CSS; ?>" rel="stylesheet">

<!-- Scroller DataTables CSS -->
<link href="<?php echo $FILE_SCROLLER_DATATABLES_MIN_CSS; ?>" rel="stylesheet">

<!-- Animate CSS -->
<link href="<?php echo $FILE_ANIMATE_CSS; ?>" rel="stylesheet">

<!-- Select2 CSS -->
<link href="<?php echo $FILE_SELECT2_CSS; ?>" rel="stylesheet">

<!-- Select2 Bootstrap CSS -->
<link href="<?php echo $FILE_SELECT2_BOOTSTRAP_CSS; ?>" rel="stylesheet">

<!-- Bootstrap Datetimepicker CSS -->
<link href="<?php echo $FILE_BOOTSTRAP_DATE_TIME_PICKER_CSS; ?>" rel="stylesheet">

<!-- elfinder CSS -->
<link href="<?php echo $FILE_ELFINDER_CSS; ?>" rel="stylesheet">

<!-- elfinder theme CSS -->
<link href="<?php echo $FILE_ELFINDER_THEME_CSS; ?>" rel="stylesheet">