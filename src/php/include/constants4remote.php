<?php
opcache_reset();

require_once '../constants/_APP_CONSTANTS.php';
require_once '../constants/_VERSION_CONSTANTS.php';
require_once '../constants/_DB_CONSTANTS.php';
require_once '../constants/_FILENAME_CONSTANTS.php';
require_once '../constants/_GLOBAL_CONSTANTS.php';
require_once '../constants/_MESSAGE_CONSTANTS.php';
require_once '../constants/_QUERY_CONSTANTS.php';
require_once '../constants/_HTML_TOKEN_CONSTANTS.php';

?>