<?php
opcache_reset();
?>	

<!-- jQuery Core -->
<script src="<?php echo $FILE_JQUERY_JS; ?>"></script>

<!-- jQuery UI Core (dont know which version)-->
<script src="<?php echo $FILE_JQUERY_UI_JS; ?>"></script>

<!-- Bootstrap Core -->
<script src="<?php echo $FILE_BOOTSTRAP_JS; ?>"></script>

<!-- jQuery DataTables Min -->
<script src="<?php echo $FILE_JQUERY_DATATABLES_MIN_JS; ?>"></script>

<!-- DataTables Bootstrap -->
<script src="<?php echo $FILE_DATATABLES_BOOTSTRAP_MIN_JS; ?>"></script>

<!-- DataTables FixedHeader -->
<script src="<?php echo $FILE_DATATABLES_FIXED_HEADER_MIN_JS; ?>"></script>

<!-- DataTables Responsive -->
<script src="<?php echo $FILE_DATATABLES_RESPONSIVE_MIN_JS; ?>"></script>

<!-- DataTables Scroller -->
<script src="<?php echo $FILE_DATATABLES_SCROLLER_MIN_JS; ?>"></script>

<!-- Select2 -->
<script src="<?php echo $FILE_SELECT2_FULL_JS; ?>"></script>

<!-- Datetimepicker -->
<script src="<?php echo $FILE_BOOTSTRAP_DATE_TIME_PICKER_JS; ?>"></script>

<!-- elfinder -->
<script src="<?php echo $FILE_ELFINDER_FULL_JS; ?>"></script>

<!-- JQUERY.TEXTCOMPLETE -->
<script src="<?php echo $FILE_JQUERY_TEXTCOMPLETE_JS; ?>"></script>