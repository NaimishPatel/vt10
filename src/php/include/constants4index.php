<?php
opcache_reset();

require_once 'src/php/constants/_APP_CONSTANTS.php';
require_once 'src/php/constants/_VERSION_CONSTANTS.php';
require_once 'src/php/constants/_DB_CONSTANTS.php';
require_once 'src/php/constants/_FILENAME_CONSTANTS.php';
require_once 'src/php/constants/_GLOBAL_CONSTANTS.php';
require_once 'src/php/constants/_MESSAGE_CONSTANTS.php';
require_once 'src/php/constants/_QUERY_CONSTANTS.php';
require_once 'src/php/constants/_HTML_TOKEN_CONSTANTS.php';

?>