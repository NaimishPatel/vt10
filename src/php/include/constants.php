<?php
opcache_reset();

require_once '../php/constants/_APP_CONSTANTS.php';
require_once '../php/constants/_VERSION_CONSTANTS.php';
require_once '../php/constants/_DB_CONSTANTS.php';
require_once '../php/constants/_FILENAME_CONSTANTS.php';
require_once '../php/constants/_GLOBAL_CONSTANTS.php';
require_once '../php/constants/_MESSAGE_CONSTANTS.php';
require_once '../php/constants/_QUERY_CONSTANTS.php';
require_once '../php/constants/_HTML_TOKEN_CONSTANTS.php';

?>