<?php
opcache_reset();

$FILE_INDEX_PHP = "../../index.php";
$FILE_FAVICON_PNG = "../../favicon.png";
$FILE_FAVICON_PNG_4INDEX = "favicon.png";

/* *** API *** */
$FILE_CONNECTION_PHP = "../../api/connection.php";
$FILE_SESSION_PHP = "../../api/session.php";
$FILE_SQL_QUERY_BUILDER_PHP = "../../api/SqlQueryBuilder.class.php";
$FILE_SQL_QUERY_BUILDER_1_PHP = "../../api/SqlQueryBuilder1.class.php";
$FILE_SQL_QUERY_BUILDER_2_PHP = "../../api/SqlQueryBuilder2.class.php";
$FILE_ELFINDER_CONNECTOR_PHP = "../../api/elFinderConnector.class.php";
$FILE_ELFINDER_VOLUME_DRIVER_PHP = "../../api/elFinderVolumeDriver.class.php";
$FILE_ELFINDER_VOLUME_FTP_PHP = "../../api/elFinderVolumeFTP.class.php";
$FILE_ELFINDER_VOLUME_FTP_IIS_PHP = "../../api/elFinderVolumeFTPIIS.class.php";
$FILE_ELFINDER_VOLUME_LOCAL_FILESYSTEM_PHP = "../../api/elFinderVolumeLocalFileSystem.class.php";
$FILE_ELFINDER_VOLUME_MYSQL_PHP = "../../api/elFinderVolumeMySQL.class.php";
/* *** API Ends *** */
/* *** ************************************************************************************************** *** */

/* *** LIB *** */
/* CSS */
$FILE_ANIMATE_CSS = "../../lib/css/animate.css";
$FILE_JQUERY_UI_CSS = "../../lib/css/jquery-ui.css";
$FILE_BOOTSTRAP_CSS = "../../lib/css/bootstrap.css";
$FILE_BOOTSTRAP_MIN_CSS = "../../lib/css/bootstrap.min.css";
$FILE_BOOTSTRAP_MIN_CSS_4INDEX = "lib/css/bootstrap.min.css";
$FILE_BOOTSTRAP_DATE_TIME_PICKER_CSS = "../../lib/css/bootstrap-datetimepicker.css";
$FILE_DATATABLES_BOOTSTRAP_MIN_CSS = "../../lib/css/dataTables.bootstrap.min.css";
$FILE_FIXED_COLUMNS_DATATABLES_MIN_CSS = "../../lib/css/fixedColumns.dataTables.min.css";
$FILE_FIXED_HEADER_DATATABLES_MIN_CSS = "../../lib/css/fixedHeader.dataTables.min.css";
$FILE_FONT_AWESOME_CSS = "../../lib/css/font-awesome.css";
$FILE_FONT_AWESOME_MIN_CSS = "../../lib/css/font-awesome.min.css";
$FILE_FONT_AWESOME_MIN_CSS_4INDEX = "lib/css/font-awesome.min.css";
$FILE_JQUERY_DATATABLES_MIN_CSS = "../../lib/css/jquery.dataTables.min.css";
$FILE_OPEN_ICONIC_BOOTSTRAP_MIN_CSS = "../../lib/css/open-iconic-bootstrap.min.css";
$FILE_RESPONSIVE_BOOTSTRAP_MIN_CSS = "../../lib/css/responsive.bootstrap.min.css";
$FILE_RESPONSIVE_DATATABLES_MIN_CSS = "../../lib/css/responsive.dataTables.min.css";
$FILE_SCROLLER_DATATABLES_MIN_CSS = "../../lib/css/scroller.dataTables.min.css";
$FILE_SELECT2_CSS = "../../lib/css/select2.css";
$FILE_SELECT2_BOOTSTRAP_CSS = "../../lib/css/select2-bootstrap.css";
$FILE_ELFINDER_CSS = "../../lib/css/elfinder.full.css";
$FILE_ELFINDER_THEME_CSS = "../../lib/css/elfinder-theme.css";
/* CSS Ends */
/* ---------------------------------------------------------------------------------------------------------- */

/* Fonts */
$FILE_FONT_AWESOME_OTF = "../../lib/fonts/FontAwesome.otf";
$FILE_FONT_AWESOME_WEBFONT_EOT = "../../lib/fonts/fontawesome-webfont.eot";
$FILE_FONT_AWESOME_WEBFONT_SVG = "../../lib/fonts/fontawesome-webfont.svg";
$FILE_FONT_AWESOME_WEBFONT_TTF = "../../lib/fonts/fontawesome-webfont.ttf";
$FILE_FONT_AWESOME_WEBFONT_WOFF = "../../lib/fonts/fontawesome-webfont.woff";
$FILE_FONT_AWESOME_WEBFONT_WOFF2 = "../../lib/fonts/fontawesome-webfont.woff2";
$FILE_GLYPHICONS_HALFLINGS_REGULAR_EOT = "../../lib/fonts/glyphicons-halflings-regular.eot";
$FILE_GLYPHICONS_HALFLINGS_REGULAR_SVG = "../../lib/fonts/glyphicons-halflings-regular.svg";
$FILE_GLYPHICONS_HALFLINGS_REGULAR_TTF = "../../lib/fonts/glyphicons-halflings-regular.ttf";
$FILE_GLYPHICONS_HALFLINGS_REGULAR_WOFF = "../../lib/fonts/glyphicons-halflings-regular.woff";
$FILE_GLYPHICONS_HALFLINGS_REGULAR_WOFF2 = "../../lib/fonts/glyphicons-halflings-regular.woff2";
$FILE_OPEN_ICONIC_EOT = "../../lib/fonts/open-iconic.eot";
$FILE_OPEN_ICONIC_OTF = "../../lib/fonts/open-iconic.otf";
$FILE_OPEN_ICONIC_SVG = "../../lib/fonts/open-iconic.svg";
$FILE_OPEN_ICONIC_TTF = "../../lib/fonts/open-iconic.ttf";
$FILE_OPEN_ICONIC_WOFF = "../../lib/fonts/open-iconic.woff";
/* Fonts Ends */
/* ---------------------------------------------------------------------------------------------------------- */

/* Images */
$FILE_BAPS_LOGO_BG_PNG = "../../lib/images/BAPSLogoBG.png";
$FILE_BAPS_LOGO_BG_PNG_4INDEX = "lib/images/BAPSLogoBG.png";
$FILE_BAPS_LOGO_HEADER_PNG = "../../lib/images/BAPSLogoHeader.png";
$FILE_BAPS_LOGO_HEADER_HI_RES_PNG = "../../lib/images/BAPSLogoHeaderHiRes.png";
$FILE_CLEAR_PNG = "../../lib/images/clear.png";
$FILE_LOADING_PNG = "../../lib/images/loading.gif";
/* Images Ends */
/* ---------------------------------------------------------------------------------------------------------- */

/* JS */
$FILE_BOOTSTRAP_JS = "../../lib/js/bootstrap.js";
$FILE_BOOTSTRAP_JS_4INDEX = "lib/js/bootstrap.js";
$FILE_BOOTSTRAP_DATE_TIME_PICKER_JS = "../../lib/js/bootstrap-datetimepicker.js";
$FILE_DATATABLES_BOOTSTRAP_MIN_JS = "../../lib/js/dataTables.bootstrap.min.js";
$FILE_DATATABLES_FIXED_HEADER_MIN_JS = "../../lib/js/dataTables.fixedHeader.min.js";
$FILE_DATATABLES_RESPONSIVE_MIN_JS = "../../lib/js/dataTables.responsive.min.js";
$FILE_DATATABLES_SCROLLER_MIN_JS = "../../lib/js/dataTables.scroller.min.js";
$FILE_JQUERY_DATATABLES_MIN_JS = "../../lib/js/jquery.dataTables.min.js";
$FILE_JQUERY_JS = "../../lib/js/jquery.js";
$FILE_JQUERY_JS_4INDEX = "lib/js/jquery.js";
$FILE_JQUERY_UI_JS = "../../lib/js/jquery-ui.js";
$FILE_SELECT2_FULL_JS = "../../lib/js/select2.full.js";
$FILE_ELFINDER_FULL_JS = "../../lib/js/elfinder.full.js";
$FILE_JQUERY_TEXTCOMPLETE_JS = "../../lib/js/jquery.textcomplete.js";
/* JS Ends */
/* *** LIB Ends *** */
/* *** ************************************************************************************************** *** */

/* *** SRC *** */
/* PHP (Logic) */
$FILE_DROP_DOWN_OPTIONS_PHP = "../php/DropDownOptions.php";
$FILE_FUNCTIONS_PHP = "../php/functions.php";

//Remote
$FILE_ITEM_ADD_UPDATE_PHP = "../php/remote/ItemAddUpdate.php";
$FILE_ITEM_DELETE_PHP = "../php/remote/ItemDelete.php";
$FILE_ITEM_SEARCH_PHP = "../php/remote/ItemSearch.php";
$FILE_ITEM_VIEW_PHP = "../php/remote/ItemView.php";
$FILE_KEYWORD_SUGGESTIONS_PHP = "../php/remote/KeywordSuggestions.php";
$FILE_KEYWORD_SUGGESTIONS_FOR_PRASANG_PHP = "../php/remote/KeywordSuggestionsForPrasang.php";
$FILE_BOOK_SUGGESTIONS_PHP = "../php/remote/BookSuggestions.php";
$FILE_LOCATION_SUGGESTIONS_PHP = "../php/remote/LocationSuggestions.php";
$FILE_PEOPLE_SUGGESTIONS_PHP = "../php/remote/PeopleSuggestions.php";
$FILE_SAINT_SUGGESTIONS_PHP = "../php/remote/SaintSuggestions.php";
$FILE_AUDIO_CONNECTOR_4ELFINDER_PHP = "../php/remote/connector.audio.php";
$FILE_MINIMAL_CONNECTOR_4ELFINDER_PHP = "../php/remote/connector.minimal.php";
$FILE_IMAGE_CONNECTOR_4ELFINDER_PHP = "../php/remote/connector.image.php";
$FILE_CONNECTOR_4ELFINDER_PHP = "../php/remote/connector.php";
$FILE_VIDEO_CONNECTOR_4ELFINDER_PHP = "../php/remote/connector.video.php";
$FILE_SAVE_ACC_SETTINGS_PHP = "../php/remote/SaveAccSettings.php";
$FILE_AUDIO_FILE_INFO_RETRIEVE_PHP = "../php/remote/AudioFileInfo.php";

//Constants
$FILE_APP_CONSTANTS_PHP = "../php/constants/_APP_CONSTANTS.php";
$FILE_DB_CONSTANTS_PHP = "../php/constants/_DB_CONSTANTS.php";
$FILE_FILENAME_CONSTANTS_PHP = "../php/constants/_FILENAME_CONSTANTS.php";
$FILE_GLOBAL_CONSTANTS_PHP = "../php/constants/_GLOBAL_CONSTANTS.php";
$FILE_HTML_TOKEN_CONSTANTS_PHP = "../php/constants/_HTML_TOKEN_CONSTANTS.php";
$FILE_MESSAGE_CONSTANTS_PHP = "../php/constants/_MESSAGE_CONSTANTS.php";
$FILE_VERSION_TOKEN_CONSTANTS_PHP = "../php/constants/_VERSION_CONSTANTS.php";
$FILE_QUERY_CONSTANTS_PHP = "../php/constants/_QUERY_CONSTANTS.php";

//Include
$FILE_CONSTANTS_PHP = "../php/include/constants.php";
$FILE_CONSTANTS_4INDEX_PHP = "../php/include/constants4index.php";
$FILE_CONSTANTS_4REMOTE_PHP = "../php/include/constants4remote.php";
$FILE_PHP_PHP = "../php/include/PHP.php";
$FILE_PHP_4REMOTE_PHP = "../php/include/PHP4remote.php";
$FILE_SCRIPTS_PHP = "../php/include/scripts.php";
$FILE_STYLES_PHP = "../php/include/styles.php";

//Utils
$FILE_HTML_UTILS_PHP = "../php/utils/HtmlUtils.php";

/* PHP (Logic) Ends */
/* ---------------------------------------------------------------------------------------------------------- */

/* Pages (UI) */
$FILE_HEADER_PHP = "header.php";
$FILE_VIDEOINFO_PHP = "videoinfo.php";
$FILE_VIDEOINFO_PHP_4INDEX = "src/pages/videoinfo.php";
$FILE_VIDEOSEARCH_PHP = "videosearch.php";
$FILE_DASHBOARD_PHP = "dashboard.php";
$FILE_DASHBOARD_PHP_4INDEX = "src/pages/dashboard.php";
$FILE_PRASANG_PHP = "prasang.php";
$FILE_IMAGE_PHP = "image.php";

//html
// $FILE_X_HTML = "html/x.html";

/* Pages (UI) Ends */
/* ---------------------------------------------------------------------------------------------------------- */

/* JS */
$FILE_COMMON_JS_PHP = "../js/common.js.php";
$FILE_VIDEOINFO_JS_PHP = "../js/videoinfo.js.php";
$FILE_VIDEOSEARCH_JS_PHP = "../js/videosearch.js.php";
$FILE_DASHBOARD_JS_PHP = "../js/dashboard.js.php";
$FILE_PRASANG_JS_PHP = "../js/prasang.js.php";
$FILE_IMAGE_JS_PHP = "../js/image.js.php";
$FILE_HEADER_JS_PHP = "../js/header.js.php";
/* JS Ends */
/* ---------------------------------------------------------------------------------------------------------- */

/* CSS */
$FILE_VIDEOINFO_CSS = "../css/videoinfo.css";
$FILE_VIDEOSEARCH_CSS = "../css/videosearch.css";
$FILE_COMMON_CSS = "../css/common.css";
$FILE_HEADER_CSS = "../css/header.css";
$FILE_DASHBOARD_CSS = "../css/dashboard.css";
$FILE_PRASANG_CSS = "../css/prasang.css";
$FILE_IMAGE_CSS = "../css/image.css";
/* CSS Ends */

/* *** SRC Ends *** */
/* *** ************************************************************************************************** *** */

?>