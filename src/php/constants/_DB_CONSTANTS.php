<?php
opcache_reset();

$LOCATION_AUTO_SUGGESTIONS_MIN_SEARCH_TEXT_LENGTH = 1;
$LOCATION_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT = 200;   //
$KEYWORD_AUTO_SUGGESTIONS_MIN_SEARCH_TEXT_LENGTH = 1;
$KEYWORD_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT = 200;
$SUBJECT_AUTO_SUGGESTIONS_MIN_SEARCH_TEXT_LENGTH = 1;
$SUBJECT_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT = 200;
$PEOPLE_AUTO_SUGGESTIONS_MIN_SEARCH_TEXT_LENGTH = 1;
$PEOPLE_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT = 200;
$BOOKS_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT = 200;
$GENERIC_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT = 200;
$KEYWORD_ID_WHO = 1;
$KEYWORD_LEFT_INDEX_WHO = -1;
$KEYWORD_RIGHT_INDEX_WHO = 18208;
$KEYWORD_ID_ACTION = 1593;
$KEYWORD_LEFT_INDEX_ACTION = 513;
$KEYWORD_RIGHT_INDEX_ACTION = 1044;
$KEYWORD_ID_WHAT = 3528;
$KEYWORD_LEFT_INDEX_WHAT = 1045;
$KEYWORD_RIGHT_INDEX_WHAT = 3712;
$KEYWORD_ID_WHOM = 1999;
$KEYWORD_LEFT_INDEX_WHOM = 3713;
$KEYWORD_RIGHT_INDEX_WHOM = 4020;
$KEYWORD_ID_WHERE = 1993;
$KEYWORD_LEFT_INDEX_WHERE = 4021;
$KEYWORD_RIGHT_INDEX_WHERE = 4032;
$KEYWORD_ID_WHEN = 1859;
$KEYWORD_LEFT_INDEX_WHEN = 4033;
$KEYWORD_RIGHT_INDEX_WHEN = 4300;
$KEYWORD_ID_SUBJECT = 3506;
$KEYWORD_LEFT_INDEX_SUBJECT = 4357;
$KEYWORD_RIGHT_INDEX_SUBJECT = 7176;
$KEYWORD_ID_EVENT = 3506;
$KEYWORD_LEFT_INDEX_EVENT = 4357;
$KEYWORD_RIGHT_INDEX_EVENT = 7176;

// database tables
$DB_INOTES_SCHEMA_NAME			= "inotes$VER_V_NUMBER";
$DB_LOCATION_SCHEMA_NAME		= 'ilocations';
$DB_PEOPLE_SCHEMA_NAME			= 'ipeople';
$DB_SAINTS_SCHEMA_NAME			= 'saintdb1';
$DB_LOCATIONS_TABLE_NAME		= $DB_LOCATION_SCHEMA_NAME .'.locations_final';
$DB_KEYWORDS_TABLE_NAME  		= $DB_INOTES_SCHEMA_NAME . '.keywords';
$DB_ITEMS_TABLE_NAME    		= $DB_INOTES_SCHEMA_NAME . '.items';
$DB_ITEMTEXTS_TABLE_NAME    	= $DB_INOTES_SCHEMA_NAME . '.item_texts';
$DB_ITEMKEYWORDS_TABLE_NAME		= $DB_INOTES_SCHEMA_NAME . '.item_kwlines';
$DB_ITEMSUBJECTS_TABLE_NAME		= $DB_INOTES_SCHEMA_NAME . '.item_subjects';
$DB_ITEMDATES_TABLE_NAME		= $DB_INOTES_SCHEMA_NAME . '.item_dates';
$DB_PEOPLE_MASTER_TABLE_NAME	= $DB_PEOPLE_SCHEMA_NAME . '.master_info';
$DB_SAINTS_MASTER_TABLE_NAME	= $DB_SAINTS_SCHEMA_NAME . '.master_info';
$DB_ITEMLOCATIONS_TABLE_NAME	= $DB_INOTES_SCHEMA_NAME . '.item_locations';
$DB_ITEMEVENTS_TABLE_NAME		= $DB_INOTES_SCHEMA_NAME . '.item_events';
$DB_ITEMLANGUAGES_TABLE_NAME	= $DB_INOTES_SCHEMA_NAME . '.item_languages';
$DB_VALUELOOKUPS_TABLE_NAME		= $DB_INOTES_SCHEMA_NAME . '.value_lookups';
$DB_USERS_TABLE_NAME			= $DB_INOTES_SCHEMA_NAME . '.users';
$DB_BOOKS_TABLE_NAME			= $DB_INOTES_SCHEMA_NAME . '.books';
$DB_ITEM_NOTES_TABLE_NAME       = $DB_INOTES_SCHEMA_NAME . '.item_notes';
$DB_VAKTA_MASTER_TABLE_NAME		= $DB_PEOPLE_SCHEMA_NAME . '.person_master';
$DB_ITEM_VAKTA_TABLE_NAME		= $DB_INOTES_SCHEMA_NAME . '.item_vakta';
$DB_ITEM_CATEGORIES_TABLE_NAME	= $DB_INOTES_SCHEMA_NAME . '.item_categories';
$DB_ITEM_TAG_TABLE_NAME		    = $DB_INOTES_SCHEMA_NAME . '.item_tags';

// 	UI to DB : elementId:columnName mapping
$itemUItoDBMappingArray 			= array();
$itemTextsUItoDBMappingArray 		= array();
$itemLocationsUItoDBMappingArray	= array();
$itemEventsUItoDBMappingArray 		= array();
$itemLanguagesUItoDBMappingArray 	= array();
$itemDateUItoDBMappingArray 		= array();
$itemKeywordLineUItoDBMappingArray	= array();
$itemSubjectsUItoDBMappingArray 	= array();
$itemReferencesUItoDBMappingArray 	= array();
$itemNotesUItoDBMappingArray 		= array();
$itemNotesColumnsDataTypeArray      = array();
$itemVaktaUItoDBMappingArray 		= array();
$itemVaktaColumnsDataTypeArray      = array();
$itemCategoriesUItoDBMappingArray 	= array();
$itemCategoriesColumnsDataTypeArray = array();
$itemTagsUItoDBMappingArray         = array();
$itemTagsColumnsDataTypeArray       = array();

$itemUItoDBMappingArray['itemId'] 				= 'item_id';
$itemUItoDBMappingArray['parentItemId'] 	 	= 'parent_item_id';
$itemUItoDBMappingArray['itemCategory1'] 		= 'category1';
$itemUItoDBMappingArray['itemCategory2'] 		= 'category2';
$itemUItoDBMappingArray['itemTitle']  			= 'title';
$itemUItoDBMappingArray['technicalQuality']		= 'technical_quality';
$itemUItoDBMappingArray['audioQuality']		    = 'audio_quality';
$itemUItoDBMappingArray['technicalNotes']  		= 'technical_notes';
$itemUItoDBMappingArray['clipStartTime']   		= 'clip_start_time';
$itemUItoDBMappingArray['clipEndTime']     		= 'clip_end_time';
$itemUItoDBMappingArray['urlOrFileFolderPath']	= 'fpath_url';
$itemUItoDBMappingArray['fileFolderName']		= 'fname';
$itemUItoDBMappingArray['itemType1'] 			= 'type1';
$itemUItoDBMappingArray['itemType2'] 			= 'type2';
$itemUItoDBMappingArray['itemUniqueCode'] 		= 'unique_code';
$itemUItoDBMappingArray['itemSource']         	= 'original_source';
$itemUItoDBMappingArray['confidentiality']		= 'target_peopletype';
$itemUItoDBMappingArray['accessibility'] 		= 'target_sitelevel';
$itemUItoDBMappingArray['language']       		= 'language';
$itemUItoDBMappingArray['artist']       		= 'artist';

$itemColumnsDataTypeArray['itemId'] 			= 'number';        	
$itemColumnsDataTypeArray['parentItemId'] 	 	= 'number'; 	
$itemColumnsDataTypeArray['itemCategory1'] 		= 'text';
$itemColumnsDataTypeArray['itemCategory2'] 		= 'text';
$itemColumnsDataTypeArray['itemTitle']  		= 'text';
$itemColumnsDataTypeArray['technicalQuality']	= 'text';
$itemColumnsDataTypeArray['audioQuality']		= 'text';
$itemColumnsDataTypeArray['technicalNotes']  	= 'text';
$itemColumnsDataTypeArray['clipStartTime']   	= 'text';
$itemColumnsDataTypeArray['clipEndTime']     	= 'text';
$itemColumnsDataTypeArray['urlOrFileFolderPath']= 'text';
$itemColumnsDataTypeArray['fileFolderName']		= 'text';
$itemColumnsDataTypeArray['itemType1'] 			= 'text';
$itemColumnsDataTypeArray['itemType2'] 			= 'text';
$itemColumnsDataTypeArray['itemUniqueCode'] 	= 'text';
$itemColumnsDataTypeArray['itemSource']         = 'text';
$itemColumnsDataTypeArray['confidentiality']	= 'text';
$itemColumnsDataTypeArray['accessibility'] 		= 'text';
$itemColumnsDataTypeArray['language']       	= 'text';
$itemColumnsDataTypeArray['artist']       		= 'number';

$itemColumnsSearchTypeArray['itemId'] 				= 'EQUAL_NUM';
$itemColumnsSearchTypeArray['parentItemId'] 		= 'EQUAL'; 	
$itemColumnsSearchTypeArray['itemCategory1'] 		= 'EQUAL';
$itemColumnsSearchTypeArray['itemCategory2'] 		= 'EQUAL';
$itemColumnsSearchTypeArray['itemTitle']  			= 'LIKE_B';
$itemColumnsSearchTypeArray['technicalQuality']		= 'EQUAL';
$itemColumnsSearchTypeArray['technicalNotes']  		= 'LIKE_B';
$itemColumnsSearchTypeArray['clipStartTime']   		= 'EQUAL'; // some smart search option for clip start time shall be required
$itemColumnsSearchTypeArray['clipEndTime']     		= 'EQUAL'; // --do--
$itemColumnsSearchTypeArray['urlOrFileFolderPath']	= 'LIKE_B';
$itemColumnsSearchTypeArray['fileFolderName']		= 'LIKE_B';
$itemColumnsSearchTypeArray['itemType1'] 			= 'EQUAL';
$itemColumnsSearchTypeArray['itemType2'] 			= 'EQUAL';
$itemColumnsSearchTypeArray['itemUniqueCode'] 		= 'EQUAL';
$itemColumnsSearchTypeArray['itemSource']         	= 'LIKE_B';
$itemColumnsSearchTypeArray['confidentiality']		= 'EQUAL';
$itemColumnsSearchTypeArray['accessibility'] 		= 'EQUAL';
$itemColumnsSearchTypeArray['language']       		= 'EQUAL';
$itemColumnsSearchTypeArray['artist']				= 'EQUAL';

$itemTextsUItoDBMappingArray['additionalDescp'] 	= 'description';
$itemTextsUItoDBMappingArray['stext1']				= 'stext1';
$itemTextsUItoDBMappingArray['stext2']				= 'stext2';
$itemTextsUItoDBMappingArray['ltext1']				= 'ltext1';
$itemTextsUItoDBMappingArray['ltext2']				= 'ltext2';

$itemTextsColumnsDataTypeArray['additionalDescp']	= 'text';
$itemTextColumnsDataTypeArray['stext1']				= 'text';
$itemTextColumnsDataTypeArray['stext2'] 			= 'text';
$itemTextColumnsDataTypeArray['ltext1'] 			= 'text';
$itemTextColumnsDataTypeArray['ltext2'] 			= 'text';

$itemDateUItoDBMappingArray['eventStartDateYYYY']	= 'start_date_yyyy';
$itemDateUItoDBMappingArray['eventStartDateMM'] 	= 'start_date_mm';
$itemDateUItoDBMappingArray['eventStartDateDD']		= 'start_date_dd';
$itemDateUItoDBMappingArray['eventEndDateYYYY'] 	= 'end_date_yyyy';
$itemDateUItoDBMappingArray['eventEndDateMM']		= 'end_date_mm';
$itemDateUItoDBMappingArray['eventEndDateDD']		= 'end_date_dd';

$itemDateColumnsDataTypeArray['eventStartDateYYYY']	= 'number';
$itemDateColumnsDataTypeArray['eventStartDateMM'] 	= 'number';
$itemDateColumnsDataTypeArray['eventStartDateDD']	= 'number';
$itemDateColumnsDataTypeArray['eventEndDateYYYY'] 	= 'number';
$itemDateColumnsDataTypeArray['eventEndDateMM']		= 'number';
$itemDateColumnsDataTypeArray['eventEndDateDD']		= 'number';

$itemKeywordLineUItoDBMappingArray['keywordLine'] 	= 'line_no';
$itemKeywordLineUItoDBMappingArray['keywordIndex'] 	= 'word_no'; 
$itemKeywordLineUItoDBMappingArray['keywordId'] 	= 'keyword_id';
$itemKeywordLineUItoDBMappingArray['keywordText']	= 'keyword_title';
//$itemKeywordLineUItoDBMappingArray['keywordRank']	= 'keyword_rank';
 	
$itemKeywordLineColumnsDataTypeArray['keywordLine']	= 'number';
$itemKeywordLineColumnsDataTypeArray['keywordIndex']= 'number';
$itemKeywordLineColumnsDataTypeArray['keywordId'] 	= 'number';
$itemKeywordLineColumnsDataTypeArray['keywordText']	= 'text';
$itemKeywordLineColumnsDataTypeArray['keywordRank']	= 'number';

$itemSubjectsUItoDBMappingArray['subjectLine'] 		= 'line_no';
$itemSubjectsUItoDBMappingArray['subjectIndex'] 	= 'word_no'; 
$itemSubjectsUItoDBMappingArray['subjectId'] 		= 'subject_id';
$itemSubjectsUItoDBMappingArray['subjectText']		= 'subject_title';
//$itemSubjectsUItoDBMappingArray['subjectRank']	= 'subject_rank';
 	
$itemSubjectsColumnsDataTypeArray['subjectLine']	= 'number';
$itemSubjectsColumnsDataTypeArray['subjectIndex']	= 'number';
$itemSubjectsColumnsDataTypeArray['subjectId'] 		= 'number';
$itemSubjectsColumnsDataTypeArray['subjectText']	= 'text';
//$itemSubjectsColumnsDataTypeArray['subjectRank']	= 'number';

$itemReferencesUItoDBMappingArray['bookId'] 		= 'book_id';
$itemReferencesUItoDBMappingArray['bookText']		= 'ref_address';
//$itemReferencesUItoDBMappingArray['additionalInfo']	= 'additional_info';

$itemReferencesColumnsDataTypeArray['bookId'] 	= 'number';
$itemReferencesColumnsDataTypeArray['bookText']	= 'text';
//$itemReferencesColumnsDataTypeArray['additionalInfo']	= 'text';

//$itemEventsUItoDBMappingArray['eventLine'] 		= 'line_no';
//$itemEventsUItoDBMappingArray['eventIndex'] 		= 'word_no'; 
$itemEventsUItoDBMappingArray['eventId'] 			= 'event_id';
$itemEventsUItoDBMappingArray['eventText']			= 'event_title';

 	
//$itemEventsColumnsDataTypeArray['eventLine']		= 'number';
//$itemEventsColumnsDataTypeArray['eventIndex']		= 'number';
$itemEventsColumnsDataTypeArray['eventId'] 			= 'number';
$itemEventsColumnsDataTypeArray['eventText']		= 'text';

//$itemLanguagesUItoDBMappingArray['languageLine'] 		= 'line_no';
//$itemLanguagesUItoDBMappingArray['languageIndex'] 	= 'word_no'; 
$itemLanguagesUItoDBMappingArray['languageId'] 			= 'language_id';
$itemLanguagesUItoDBMappingArray['languageText']		= 'language_name';

 	
//$itemLanguagesColumnsDataTypeArray['languageLine']		= 'number';
//$itemLanguagesColumnsDataTypeArray['languageIndex']		= 'number';
$itemLanguagesColumnsDataTypeArray['languageId'] 		= 'number';
$itemLanguagesColumnsDataTypeArray['languageText']		= 'text';


$itemLocationsUItoDBMappingArray['locationId'] 		= 'location_id';
$itemLocationsUItoDBMappingArray['locationText']	= 'location_name';
$itemLocationsUItoDBMappingArray['locationTree']	= 'location_name_tree';
 	
$itemLocationsColumnsDataTypeArray['locationId'] 	= 'number';
$itemLocationsColumnsDataTypeArray['locationText']	= 'text';
$itemLocationsColumnsDataTypeArray['locationTree']	= 'text';

//$itemNotesUItoDBMappingArray['noteId'] = 'note_id';
$itemNotesUItoDBMappingArray['noteText'] = 'note_text';

//$itemNotesColumnsDataTypeArray['noteId'] = 'number';
$itemNotesColumnsDataTypeArray['noteText'] = 'text';

$itemVaktaUItoDBMappingArray['vaktaId'] = 'vakta_id';
$itemVaktaUItoDBMappingArray['vaktaName'] = 'vakta_name';

$itemVaktaColumnsDataTypeArray['vaktaId'] = 'number';
$itemVaktaColumnsDataTypeArray['vaktaName'] = 'text';

$itemCategoriesUItoDBMappingArray['categoryId'] = 'category_id';
$itemCategoriesUItoDBMappingArray['categoryTitle'] = 'category_title';

$itemCategoriesColumnsDataTypeArray['categoryId'] = 'number';
$itemCategoriesColumnsDataTypeArray['categoryTitle'] = 'text';

$itemTagsUItoDBMappingArray['tagLine'] 		= 'line_no';
$itemTagsUItoDBMappingArray['tagIndex'] 	= 'word_no';
$itemTagsUItoDBMappingArray['tagId'] 		= 'tag_id';
$itemTagsUItoDBMappingArray['tagText']		= 'tag_title';

$itemTagsColumnsDataTypeArray['tagLine']	= 'number';
$itemTagsColumnsDataTypeArray['tagIndex']	= 'number';
$itemTagsColumnsDataTypeArray['tagId'] 		= 'number';
$itemTagsColumnsDataTypeArray['tagText']	= 'text';

$itemDBtoUIMappingArray 			= array_flip($itemUItoDBMappingArray);
$itemTextsDBtoUIMappingArray 		= array_flip($itemTextsUItoDBMappingArray);
$itemDateDBtoUIMappingArray 		= array_flip($itemDateUItoDBMappingArray);
$itemKeywordLineDBtoUIMappingArray 	= array_flip($itemKeywordLineUItoDBMappingArray);
$itemSubjectsDBtoUIMappingArray 	= array_flip($itemSubjectsUItoDBMappingArray);
$itemLocationsDBtoUIMappingArray 	= array_flip($itemLocationsUItoDBMappingArray);
$itemReferencesDBtoUIMappingArray 	= array_flip($itemReferencesUItoDBMappingArray);

//Lookup Constants //Not used as of now. Remove when version is stable.
$LOOKUP_TYPE_ITEM_CATEGORY = "ITEM_CATEGORY";
$LOOKUP_TYPE_ITEM_QUALITY = "ITEM_QUALITY";
$LOOKUP_TYPE_ITEM_CONFIDENTIALITY = "ITEM_CONFIDENTIALITY";
$LOOKUP_TYPE_ITEM_ACCESSIBILITY = "ITEM_ACCESSIBILITY";
$LOOKUP_TYPE_ITEM_LANGUAGE = "ITEM_LANGUAGE";
$LOOKUP_TYPE_PRAS_CATEGORY = "PRAS_CATEGORY";
$LOOKUP_TYPE_ITEM_VAKTA = "ITEM_VAKTA";

//Item types
$ITEMTYPE_SWAMISHREE 			= 1;
$ITEMTYPE_IMAGE 				= 2;
$ITEMTYPE_VIDEO 				= 3;
$ITEMTYPE_AUDIO 				= 4;
$ITEMTYPE_PRESENTATION_SLIDES 	= 5;
$ITEMTYPE_DOCUMENT 				= 6;
$ITEMTYPE_PRASANG 				= 7;
$ITEMTYPE_VIDEO_INTERVIEW 		= 8;
$ITEMTYPE_AUDIO_INTERVIEW 		= 9;
$ITEMTYPE_PRESENTATION_VIDEO 	= 10;

//Item type lookups
$LOOKUP_ITEMTYPE = 	array(
							$ITEMTYPE_SWAMISHREE."" => array("type2" => "", "category1" => "", "category2" => ""), //Swamishree
							$ITEMTYPE_IMAGE."" => array("type2" => "IMAGE_TYPE_2", "category1" => "IMAGE_CATEGORY_1", "category2" => "IMAGE_CATEGORY_2"), //Image
							$ITEMTYPE_VIDEO."" => array("type2" => "VIDEO_TYPE_2", "category1" => "VIDEO_CATEGORY_1", "category2" => "VIDEO_CATEGORY_2"), //Video
							$ITEMTYPE_AUDIO."" => array("type2" => "AUDIO_TYPE_2", "category1" => "AUDIO_CATEGORY_1", "category2" => "AUDIO_CATEGORY_2"), //Audio
							$ITEMTYPE_PRESENTATION_SLIDES."" => array("type2" => "", "category1" => "", "category2" => ""), //Presentation-Slides
							$ITEMTYPE_DOCUMENT."" => array("type2" => "", "category1" => "", "category2" => ""), //Document
							$ITEMTYPE_PRASANG."" => array("type2" => "PRASANG_TYPE_2", "category1" => "PRASANG_CATEGORY_1", "category2" => "PRASANG_CATEGORY_2"), //Prasang
							$ITEMTYPE_VIDEO_INTERVIEW."" => array("type2" => "", "category1" => "", "category2" => ""), //Video-Interview
							$ITEMTYPE_AUDIO_INTERVIEW."" => array("type2" => "", "category1" => "", "category2" => ""), //Audio-Interview
							$ITEMTYPE_PRESENTATION_VIDEO."" => array("type2" => "", "category1" => "", "category2" => ""), //Presentation-Video
						);

?>