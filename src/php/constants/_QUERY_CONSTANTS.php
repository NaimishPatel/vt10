 <?php
opcache_reset();

$QUERY_AUTHENTICATE_USER = 
"SELECT 
	user_id, 
	parent_user_id, 
	user_name, 
	user_email, 
	user_mobile, 
	master_admin 
FROM 
	$DB_USERS_TABLE_NAME 
WHERE 
	user_name = '~~username~~' AND 
	password = '~~password~~' AND 
	active = 1;";

$QUERY_GET_LOOKUPS = 
"SELECT
	lookup_code AS id,
	lookup_value AS text
FROM $DB_VALUELOOKUPS_TABLE_NAME
WHERE
	lookup_type = '~~lookupType~~'
ORDER BY 
	lookup_code;";

$QUERY_GET_LOOKUPS_FROM = 
"SELECT 
	~~idColName~~ AS id,
	~~textColName~~ AS text
FROM ~~tableName~~;";

$QUERY_DELETE_ITEM_FROM_TABLE = 
"DELETE 
FROM ~~table~~ 
WHERE 
	item_id = ~~itemId~~;";

$QUERY_GET_ITEM_CHILDREN = 
"SELECT 
	item_id 
FROM $DB_ITEMS_TABLE_NAME 
WHERE 
	parent_item_id = ~~parentItemId~~;";

$QUERY_GET_TABLE_DATA_FOR_ITEM = 
"SELECT 
	* 
FROM ~~dbTableName~~ 
WHERE 
	item_id = ~~itemId~~;";

$QUERY_GET_SAINT_SUGGESTIONS = 
"SELECT 
	saint_auto_id AS id,
	saint_name_e AS text
FROM $DB_SAINTS_MASTER_TABLE_NAME
WHERE 
	saint_name_e LIKE '%~~rpSearchText~~%'
ORDER BY 
	saint_name_e
LIMIT $PEOPLE_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT;";

$QUERY_GET_PEOPLE_SUGGESTIONS = 
"SELECT 
	person_id AS id,
	concat(first_name, ' ', last_name) AS text
FROM $DB_PEOPLE_MASTER_TABLE_NAME
WHERE 
	first_name LIKE '~~rpSearchText~~%' OR
	last_name LIKE '~~rpSearchText~~%' 
ORDER BY 
	text
LIMIT $PEOPLE_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT;";

$QUERY_GET_BOOK_SUGGESTIONS = 
"SELECT 
	book_id AS id,
	book_title AS text
FROM $DB_BOOKS_TABLE_NAME
WHERE 
	book_title LIKE '~~rpSearchText~~%'
ORDER BY 
	text
LIMIT $GENERIC_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT;";

$QUERY_GET_GENERIC_SUGGESTIONS = 
"SELECT 
	~~idColumnName~~ AS id,
	~~textColumnName~~ AS text
FROM ~~tableName~~
WHERE 
	~~textColumnName~~ LIKE '~~rpSearchText~~%'
ORDER BY 
	text
LIMIT 200;";

$QUERY_GET_LOCATION_SUGGESTIONS_WITH_CHILDREN = 
"SELECT 
	location_id AS id,
	name_tree AS text
FROM $DB_LOCATIONS_TABLE_NAME 
WHERE 
	location_name_e LIKE '~~rpSearchText~~%' 
	AND lft > $DB_INOTES_SCHEMA_NAME.getLeftRightindex(~~rpParentId~~,'left')
	AND rgt < $DB_INOTES_SCHEMA_NAME.getLeftRightindex(~~rpParentId~~,'right')
	~~favouriteClause~~
ORDER BY 
	name_tree 
LIMIT $LOCATION_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT;";

$QUERY_GET_LOCATION_SUGGESTIONS = 
"SELECT 
	location_id AS id,
	name_tree AS text
FROM $DB_LOCATIONS_TABLE_NAME
WHERE 
	location_name_e LIKE '~~rpSearchText~~%'
	~~favouriteClause~~
ORDER BY 
	name_tree
LIMIT $LOCATION_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT;";

$QUERY_GET_LOCATION_SUGGESTIONS_MATCHING_NAMETREE = 
"SELECT 
	location_id AS id,
	name_tree AS text
FROM $DB_LOCATIONS_TABLE_NAME
WHERE 
	name_tree LIKE '~~rpSearchText~~%' 
	AND parent_id IN (
		SELECT 
			location_id AS parent_id 
		FROM $DB_LOCATIONS_TABLE_NAME
		WHERE 
			name_tree = '~~parentNameTree~~'
	)
	~~favouriteClause~~
ORDER BY 
	name_tree
LIMIT $LOCATION_AUTO_SUGGESTIONS_RESULT_LIMIT_COUNT;";

$FAVOURITE_CLAUSE = 
"AND favourite = 1";
?>