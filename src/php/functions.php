<?php
opcache_reset();

function getFormValue($param_name) {
	$param_value = trim($_REQUEST[$param_name]);
	//$param_value = stripslashes($param_value);
	$param_value = htmlspecialchars($param_value);
	// echo "<br>getFormValue::$param_name::value=".$param_value."<br>";
	return $param_value;
}

/*
*	$vars: Associative array
*/
function substituteTokensIn($queryConstant, $vars) {
	$query = $queryConstant;
	if($queryConstant == $QUERY_GET_TABLE_DATA_FOR_ITEM) echo $query;
	foreach($vars as $name => $value) {
		$query = str_replace($name, $value, $query);
	}
	if($queryConstant == $QUERY_GET_TABLE_DATA_FOR_ITEM) echo $query;
	return $query;
}

function getLookUps($lookupType) {
	global $QUERY_GET_LOOKUPS;
	global $con;
	$lookupResult = array();
	$vars = array("~~lookupType~~" => $lookupType);
	$qryGetLookups = substituteTokensIn($QUERY_GET_LOOKUPS, $vars);
	// echo $qryGetLookups;
	$lookupResults = mysql_query($qryGetLookups, $con) or mysql_error();
	while ($lookupResult[] = mysql_fetch_assoc($lookupResults));
	array_pop($lookupResult);
	return json_encode($lookupResult);
}

function getLookUpsFrom($tableName, $idColName, $textColName) {
	global $QUERY_GET_LOOKUPS_FROM;
	global $con;
	$lookupResult = array();
	$vars = array("~~tableName~~" => $tableName, "~~idColName~~" => $idColName, "~~textColName~~" => $textColName);
	$qryGetLookups = substituteTokensIn($QUERY_GET_LOOKUPS_FROM, $vars);
	// echo $qryGetLookups;
	$lookupResults = mysql_query($qryGetLookups, $con) or mysql_error();
	while ($lookupResult[] = mysql_fetch_assoc($lookupResults));
	array_pop($lookupResult);
	return json_encode($lookupResult);
}

?>