<?php
opcache_reset();

require_once '../../api/session.php';
require_once '../../api/connection.php';
require_once '../php/include/PHP.php';
$itemId = $_REQUEST['itemId'];
$_SESSION['itemType'] = $ITEMTYPE_IMAGE;
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="<?php echo $LABEL_IMAGE_PAGE_META_DESCRIPTION; ?>" />
<link href="<?php echo $FILE_FAVICON_PNG; ?>" rel="shortcut icon" />
<title><?php echo $LABEL_IMAGE_PAGE_TITLE; ?></title>

<!-- Include styles -->
<?php require_once '../php/include/styles.php';?>
<!-- Page specific styles -->
<link href="<?php echo $FILE_COMMON_CSS; ?>" rel="stylesheet">
<link href="<?php echo $FILE_HEADER_CSS; ?>" rel="stylesheet">
<link href="<?php echo $FILE_IMAGE_CSS; ?>" rel="stylesheet">
</head>

<body>
	<!-- Show Header -->
	<?php require_once 'header.php';?>
	<!-- Begin Image page content -->
	<div id="top" class="container-fluid">
		<div id="elfinder-content" class="row">
			<div class="main col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<!-- Begin elfinder content -->
				<div id="elfinder">
					<!-- elfinder will be displayed here -->
				</div>
			</div>
		</div>
		<div id="image-content" class="row" hidden>
			<div class="main col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h1 class="page-header"><?php echo $LABEL_IMAGE_INFORMATION; ?></h1><!-- Title -->
                   	</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						<a onClick="addAnotherImage();" class="btn btn-default btn-outline float-right page-header" data-toggle="offcanvas" ><?php echo $LABEL_ADD_ANOTHER_IMAGE; ?></a>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h3><?php echo $LABEL_BASIC_IMAGE_DETAILS; ?></h3>
                   	</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						<a id="edit_item00_switchParent1" class="btn btn-sm btn-default btn-outline float-right btn-collapse collapse" onClick="switchParent(this);"><?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?></a>
						<a id="edit_item00_switchParent2" class="btn btn-sm btn-default btn-outline float-right btn-toggle collapsed" data-toggle="tooltip" title="<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>" data-placement="top" onClick="switchParent(this);">
							<span class="sr-only"><?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?></span>
							<span class="fa fa-list-alt"></span>
						</a>
					</div>
					<!-- Basic image details starts -->
                    <form id="edit_item00_form" role="form">
                        <div id="edit_item00_fixedDiv" class="row">
                            <div class="col-lg-6 col-md-18 col-sm-17 col-xs-24">
                                <div class="form-group">
									<!-- div class="required" -->
										<input type="text" class="form-control" name="edit_item00_itemTitle" id="edit_item00_itemTitle" placeholder="<?php echo $LABEL_IMAGE_TITLE_PLACEHOLDER; ?>" autofocus>
									<!-- /div -->
                                </div>
                            </div><!-- .col title div closed -->
                            <div class="col-lg-3 col-md-8 col-sm-8 col-xs-24">
                                <div class="form-group">
                                    <select class="form-control itemType2" name="edit_item00_itemType2" id="edit_item00_itemType2">
                                        <!--option value="">-- itemType2 --</option-->
                                    </select>
                                </div>
                            </div><!-- .col itemType2 div closed -->
                            <div class="col-lg-3 col-md-6 col-sm-7 col-xs-24">
                                <div class="form-group">
                                    <select class="form-control itemCategory1" name="edit_item00_itemCategory1" id="edit_item00_itemCategory1">
                                        <!--option value="">-- Category1 --</option-->
                                    </select>
                                </div><!-- .form-group div closed -->
                            </div><!-- .col category1 div closed -->
                            <div class="col-lg-3 col-md-6 col-sm-7 col-xs-24">
                                <div class="form-group">
                                    <select class="form-control itemCategory2" name="edit_item00_itemCategory2" id="edit_item00_itemCategory2">
                                        <!--option value="">-- Category2 --</option-->
                                    </select>
                                </div><!-- .form-group div closed -->
                            </div><!-- .col category2 div closed -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-24">
                                <div class="form-group">
                                    <select class="form-control technicalQuality" name="edit_item00_technicalQuality" id="edit_item00_technicalQuality">
                                        <!--option value="">-- Quality --</option-->
                                    </select>
                                </div>
                            </div><!-- .col quality div closed -->
                            <div class="col-lg-6 col-md-18 col-sm-18 col-xs-24">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="edit_item00_technicalNotes" id="edit_item00_technicalNotes" placeholder="<?php echo $LABEL_ITEM_TECHNICAL_NOTES_PLACEHOLDER; ?>">
                                </div>
                            </div><!-- .col notes div closed -->
							<div class="col-lg-6 col-md-9 col-sm-10 col-xs-24">
                                <div class="form-group">
									<div class="input-group">
										<input type="text" class="form-control" name="edit_item00_urlOrFileFolderPathText" id="edit_item00_urlOrFileFolderPathText" placeholder="<?php echo $LABEL_ITEM_URL_FILE_FOLDER_PATH_PLACEHOLDER; ?>">
										<a tabindex="0" class="input-group-addon" id="edit_item00_addUrlOrFileFolderPath" onclick="addUrlOrFileFolderPath($('#edit_item00_urlOrFileFolderPathText').val());" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_ITEM_URL_FILE_FOLDER_PATH_PLACEHOLDER; ?>">
											<span class="glyphicon glyphicon-plus"></span>
										</a>
									</div>
									<!--input type="hidden" name="edit_item00_urlOrFileFolderPath" id="edit_item00_urlOrFileFolderPath" /-->
                                </div>
                            </div><!-- .col urlOrFileFolderPaths div closed -->
                            <div class="col-lg-6 col-md-18 col-sm-17 col-xs-24">
                                <div class="form-group">
									<div id="edit_item00_urlOrFileFolderPathDisplayBox" class="urlOrFileFolderPath-display-box display-box">
                                        <!-- Dismissible urlOrFileFolderPaths will be displayed here -->
                                    </div>
                                </div>
                            </div><!-- .col path div closed -->
                            <div class="col-lg-3 col-md-6 col-sm-7 col-xs-24">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="edit_item00_itemSource" id="edit_item00_itemSource" placeholder="<?php echo $LABEL_ITEM_SOURCE_PLACEHOLDER; ?>">
                                </div>
                            </div><!-- .col source div closed -->
                            <div class="col-lg-3 col-md-8 col-sm-8 col-xs-24">
                                <div class="form-group">
                                    <select class="form-control confidentiality" name="edit_item00_confidentiality" id="edit_item00_confidentiality">
                                        <!--option value="">-- Confidentiality --</option-->
                                    </select>
                                </div>
                            </div><!-- .col confidentiality div closed -->
                            <!--div class="col-lg-3 col-md-8 col-sm-8 col-xs-24">
                                <div class="form-group">
                                    <select class="form-control accessibility" name="edit_item00_accessibility" id="edit_item00_accessibility">
                                        <!--option value="">-- Accessibility --</option>
                                    </select>
                                </div>
                            </div><!-- .col accessibility div closed -->
                        </div>
                        <div id="edit_item00_collapsableDiv" class="row collapse in">
							<!--div class="col-lg-6 col-md-8 col-sm-8 col-xs-24">
                                <div class="form-group">
									<div class="input-group">
										<select class="form-control language" name="edit_item00_langId_language" id="edit_item00_langId_language">
											<!--option value="">-- Language --</option>
										</select>
										<input type="hidden" name="edit_item00_langText_language" id="edit_item00_langText_language" />
										<a tabindex="0" class="input-group-addon" id="edit_item00_addLanguageLine" onclick="addLanguageLine(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_LANGUAGE; ?>">
											<span class="glyphicon glyphicon-plus"></span>
										</a>
									</div>
                                </div>
                            </div><!-- .col language div closed >
							<div class="col-lg-6 col-md-15 col-sm-14 col-xs-24">
                                <div class="form-group">
                                    <div id="edit_item00_languageDisplayBox" class="language-display-box display-box">
                                        <!-- Dismissible languages will be displayed here >
                                    </div>
                                </div>
                            </div--><!-- display language .col div closed -->
							<div class="col-lg-6 col-md-9 col-sm-10 col-xs-24">
                                <div class="form-group">
									<div class="input-group">
										<select class="form-control location" name="edit_item00_locId_location" id="edit_item00_locId_location">
											<!--option value="">-- Location --</option-->
										</select>
										<input type="hidden" name="edit_item00_locText_location" id="edit_item00_locText_location" />
										<a tabindex="0" class="input-group-addon" id="edit_item00_addLocationLine" onclick="addLocationLine(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_LOCATION; ?>">
											<span class="glyphicon glyphicon-plus"></span>
										</a>
									</div>
                                </div>
                            </div><!-- .col location div closed -->
							<div class="col-lg-18 col-md-15 col-sm-14 col-xs-24">
                                <table width="100%">
									<tr>
										<td width="1%">
											<div class="form-group">
												<input type="checkbox" name="edit_search_favourite" id="edit_search_favourite" placeholder="Search from Favourites" checked />
											</div>
										</td>
										<td width="99%">
											<div class="form-group">
												<div id="edit_item00_locationDisplayBox" class="location-display-box display-box">
													<!-- Dismissible locations will be displayed here -->
												</div>
											</div>
										</td>
									</tr>
								</table>
                            </div><!-- display location .col div closed -->
							<div class="col-lg-6 col-md-9 col-sm-10 col-xs-24">
                                <div class="form-group">
									<div class="input-group">
										<select class="form-control event" name="edit_item00_evtId_event" id="edit_item00_evtId_event">
											<!--option value="">-- Event --</option-->
										</select>
										<input type="hidden" name="edit_item00_evtText_event" id="edit_item00_evtText_event" />
										<a tabindex="0" class="input-group-addon" id="edit_item00_addEventLine" onclick="addEventLine(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_EVENTS; ?>">
											<span class="glyphicon glyphicon-plus"></span>
										</a>
									</div>
                                </div>
                            </div><!-- .col event div closed -->
							<div class="col-lg-6 col-md-15 col-sm-14 col-xs-24">
                                <div class="form-group">
                                    <div id="edit_item00_eventDisplayBox" class="event-display-box display-box">
                                        <!-- Dismissible events will be displayed here -->
                                    </div>
                                </div>
                            </div><!-- display event .col div closed -->
							<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
								<input class="form-control date form_datetime" id="edit_item00_eventStartDateTime" placeholder="<?php echo $LABEL_ITEM_EVENT_START_DATE_PLACEHOLDER; ?>" data-date="1970-01-16T00:00:00Z" data-date-format="dd M yyyy" data-link-field="dtp_input1" type="text" value="" readonly onblur="populateEndDate(this);" />
								<input type="hidden" id="dtp_input1" value="" />
                            </div>
							<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<div class="input-group">
										<input class="form-control date form_datetime" id="edit_item00_eventEndDateTime" placeholder="<?php echo $LABEL_ITEM_EVENT_END_DATE_PLACEHOLDER; ?>" data-date="1970-01-16T00:00:00Z" data-date-format="dd M yyyy" data-link-field="dtp_input2" type="text" value="" readonly />
										<a tabindex="0" class="input-group-addon" id="edit_item00_addDates" onclick="addDates(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_DATES; ?>">
                                        	<span class="glyphicon glyphicon-plus"></span>
                                        </a>
									</div>
									<input type="hidden" id="dtp_input2" value="" />
								</div>
                            </div>
							<div class="col-lg-6 col-md-12 col-sm-24 col-xs-24">
                                <div class="form-group">
                                    <div id="edit_item00_dateDisplayBox" class="date-display-box display-box">
                                        <!-- Dismissible dates will be displayed here -->
                                    </div>
                                </div>
                            </div><!-- .col date display div closed -->
							<!-- Keywords starts -->
                            <div id="main-keywords-panel" class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
                                <div class="well well-sm well-primary">
                                    <div class="row">
                                        <div class="col-lg-1 col-md-24 col-sm-24 col-xs-24 keyword-label" style="margin-top:6px;">
                                            <strong><?php echo $LABEL_KEYWORDS; ?></strong>
                                        </div>
                                        <div class="col-lg-23 col-md-23 col-sm-23 col-xs-24">
                                            <div class="form-group">
												<div class="input-group">
													<select class="form-control keyword" name="edit_item00_keywordId_keyword" id="edit_item00_keywordId_keyword">
														<!--option value="">-- Keyword --</option-->
													</select>
													<input type="hidden" name="edit_item00_keywordText_keyword" id="edit_item00_keywordText_keyword" />
													<a tabindex="0" class="input-group-addon" id="edit_item00_addKeywordLine" onclick="addKeywordLine(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_KEYWORDS; ?>">
														<span class="glyphicon glyphicon-plus"></span>
													</a>
												</div>
                                            </div><!-- .form-group div closed -->
                                        </div><!-- .col keyword-input-box div closed -->
                                        <div class="col-lg-22 col-lg-offset-1 col-md-23 col-sm-23 col-xs-24">
                                            <div class="form-group">
                                                <div id="edit_item00_keywordDisplayBox" class="keyword-display-box">
                                                    <!-- Dismissible Keyword Lines will be displayed here -->
                                                </div><!-- .keywords-display-box display-box div closed -->
                                            </div><!-- .form-group div closed -->
                                        </div><!-- .col-md-24 div closed -->
                                    </div><!-- .row (nested) div closed -->
                                </div><!-- .well div closed -->
                            </div><!-- main-keywords-panel div closed -->
                            <!-- Keywords ends -->
							<div class="col-lg-6 col-md-9 col-sm-10 col-xs-24">
                                <div class="form-group">
									<div class="input-group">
										<select class="form-control subject" name="edit_item00_subId_subject" id="edit_item00_subId_subject">
											<!--option value="">-- Subject --</option-->
										</select>
										<input type="hidden" name="edit_item00_subText_subject" id="edit_item00_subText_subject" />
										<a tabindex="0" class="input-group-addon" id="edit_item00_addSubjectLine" onclick="addSubjectLine(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_SUBJECTS; ?>">
											<span class="glyphicon glyphicon-plus"></span>
										</a>
									</div>
                                </div>
                            </div><!-- .col subject div closed -->
							<div class="col-lg-18 col-md-15 col-sm-14 col-xs-24">
                                <div class="form-group">
                                    <div id="edit_item00_subjectDisplayBox" class="subject-display-box display-box">
                                        <!-- Dismissible subjects will be displayed here -->
                                    </div>
                                </div>
                            </div><!-- display subject .col div closed -->
                            <div class="col-lg-23 col-md-23 col-sm-23 col-xs-24">
                                <div class="form-group">
                                    <textarea class="form-control" name="edit_item00_additionalDescp" id="edit_item00_additionalDescp" placeholder="<?php echo $LABEL_ITEM_ADDITIONAL_DESCP_PLACEHOLDER; ?>"></textarea>
                                </div>
                            </div><!-- description .col div closed -->
                        </div>
						<div class="row">
							<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
								<div class="alert alert-sm alert-danger" id="edit_item00_errors_div" hidden>
									<!-- Show Warning/Error Messages here -->
								</div>
								<div class="alert alert-sm alert-success" id="edit_item00_messages_div" hidden>
									<!-- Show Success Messages here -->
								</div>
							</div>
						</div>
						<!-- Hidden inputs -->
						<input type="hidden" name="edit_item00_parentItemId" id="edit_item00_parentItemId" />
						<input type="hidden" name="edit_item00_itemId" id="edit_item00_itemId" />
						<input type="hidden" name="edit_item00_itemType1" id="edit_item00_itemType1" value="<?php echo $_SESSION['itemType']; ?>" />
					</form><!-- Basic image details ends -->
                </div>
                <div class="row"><!-- Save button starts -->
                    <div class="col-lg-12 col-lg-offset-6 col-md-16 col-md-offset-4 col-sm-24 col-xs-24">
                        <button id="edit_item00_save" class="btn btn-lg btn-block btn-info" type="button" onClick="sendToServer(this.id, '<?php echo $_SESSION['itemType']; ?>');" data-toggle="tooltip" title="<?php echo $LABEL_SAVE; ?>">
                            	<span class="fa fa-save"></span> <?php echo $LABEL_SAVE; ?> <span class="fa fa-save"></span>
                        </button>
                    </div>
                </div><!-- Save button ends -->
				
			</div><!-- .main div closed -->
		</div><!-- .row div closed -->
	</div><!-- .container-fluid #top div closed -->
	<a href="#top">
		<div class="scroll-to-top" data-toggle="tooltip" title="<?php echo $LABEL_SCROLL_TO_TOP; ?>">
			<span class="fa fa-arrow-up"></span>
		</div>
	</a>
	<!-- End image info page content -->
	
	<!-- Delete Confirmation Modal >
	<div id="confirmDeleteModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<input type="hidden" name="del_item_number" id="del_item_number" />
			<!-- Modal content>
			<div class="modal-content">
				<div class="modal-header">
					<h4><?php echo $LABEL_CONFIRM_DELETE_CLIP; ?></h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal" onClick="deleteClip($('#del_item_number').val());"><?php echo $LABEL_YES; ?></button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $LABEL_NO; ?></button>
				</div>
			</div>
		</div>
	</div>
	<!-- Delete Confirmation Modal ends -->
	
	<!-- Image Info Not Saved Modal -->
	<div id="imageInfoNotSavedModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4><?php echo $LABEL_IMAGE_INFO_NOT_SAVED; ?></h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal" onClick="sendToServer('edit_item00_save');"><?php echo $LABEL_SAVE_AND_PROCEED; ?></button>
					<button type="button" class="btn btn-default" data-dismiss="modal" onClick="document.write('<script> parent.location.href = \'<?php echo $FILE_IMAGE_PHP; ?>\';</script>');"><?php echo $LABEL_PROCEED_ANYWAY; ?></button>
				</div>
			</div>
		</div>
	</div>
	<!-- Image Info Not Saved Modal ends -->
	
	<!-- Include scripts -->
	<?php
		require_once '../php/include/scripts.php';
		// 	<!-- Page specific JS -->
		require_once '../js/image.js.php';
		// 	<!-- Common JS -->
		require_once '../js/common.js.php';
		// 	<!-- Header JS -->
		require_once '../js/header.js.php';
	?>
</body>
</html>