<?php
opcache_reset();

?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />

<!-- elFinder CSS (REQUIRED) -->
<link rel="stylesheet" type="text/css" href="../../../elfinder/css/elfinder.full.css">
<link rel="stylesheet" type="text/css" href="../../../elfinder/css/theme.css">
<link rel="stylesheet" type="text/css" href="../../lib/css/jquery-ui.css">
</head>

<body>
	<div id="elfinder">
		<!-- elfinder will be displayed here -->
	</div>
	
	<!-- Include scripts -->
	<!-- jQuery Core -->
<script src="../../lib/js/jquery.js"></script>

<!-- jQuery UI Core (dont know which version)-->
<script src="../../lib/js/jquery-ui.js"></script>
	<!-- elfinder JS -->
	<script src="../../../elfinder/js/elfinder.full.js"></script>
	<script>

$(document).ready(function(){
	
	$('#elfinder').elfinder({
		url : '../../../elfinder/php/connector.video.php'  // connector URL (REQUIRED)
	});

});

</script>
</body>
</html>