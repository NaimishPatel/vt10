<?php
opcache_reset();

require_once '../../api/session.php';
require_once '../../api/connection.php';
require_once '../php/include/PHP.php';
$itemId = $_REQUEST['itemId'];
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="<?php echo $LABEL_AUDIO_PAGE_META_DESCRIPTION; ?>" />
<link href="<?php echo $FILE_FAVICON_PNG; ?>" rel="shortcut icon" />
<title><?php echo $LABEL_AUDIO_PAGE_TITLE; ?></title>

<!-- Include styles -->
<?php require_once '../php/include/styles.php';?>
<!-- Page specific styles -->
<link href="<?php echo $FILE_COMMON_CSS; ?>" rel="stylesheet">
<link href="<?php echo $FILE_HEADER_CSS; ?>" rel="stylesheet">
<link href="<?php echo $FILE_VIDEOINFO_CSS; ?>" rel="stylesheet">
</head>

<body>
	<!-- Show Header -->
	<?php require_once 'header.php';?>
	<!-- Begin Video Info page content -->
	<div id="top" class="container-fluid">
		<div id="elfinder-content" class="row">
			<div class="main col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<!-- Begin elfinder content -->
				<div id="elfinder">
					<!-- elfinder will be displayed here -->
				</div>
			</div>
		</div>
		<div id="audio-content" class="row" hidden>
			<div class="main col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h1 class="page-header"><?php echo $LABEL_AUDIO_INFORMATION; ?></h1><!-- Title -->
                   	</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						<a onClick="addAnotherVideo();" class="btn btn-default btn-outline float-right" data-toggle="offcanvas" ><?php echo $LABEL_ADD_ANOTHER_VIDEO; ?></a>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h3><?php echo $LABEL_AUDIO_ALBUM_DETAILS; ?></h3>
                   	</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
						<a id="edit_item00_switchParent1" class="btn btn-sm btn-default btn-outline float-right btn-collapse collapse" onClick="switchParent(this);"><?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?></a>
						<a id="edit_item00_switchParent2" class="btn btn-sm btn-default btn-outline float-right btn-toggle collapsed" data-toggle="tooltip" title="<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>" data-placement="top" onClick="switchParent(this);">
							<span class="sr-only"><?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?></span>
							<span class="fa fa-list-alt"></span>
						</a>
					</div>
					<!-- Basic video details starts -->
                    <form id="edit_item00_form" role="form">
                        <div id="edit_item00_fixedDiv" class="row">
							<!-- Album Title -->
                            <div class="col-lg-12 col-md-18 col-sm-17 col-xs-24">
                                <div class="form-group">
									<!-- div class="required" -->
										<input type="text" class="form-control" name="edit_item00_itemTitle" id="edit_item00_itemTitle" placeholder="<?php echo $LABEL_ALBUM_TITLE_PLACEHOLDER; ?>" autofocus>
									<!-- /div -->
                                </div>
                            </div><!-- .col title div closed -->
                            <!-- Category -->
							<div class="col-lg-3 col-md-6 col-sm-7 col-xs-24">
                                <div class="form-group">
                                    <select class="form-control itemCategory" name="edit_item00_itemCategory" id="edit_item00_itemCategory">
                                        <!--option value="">-- Category --</option-->
                                    </select>
                                </div><!-- .form-group div closed -->
                            </div><!-- .col category div closed -->
                            <!-- URL from EL Finder -->
							<div class="col-lg-12 col-md-18 col-sm-17 col-xs-24">
                                <div class="form-group">
									<div id="edit_item00_urlOrFileFolderPathDisplayBox" class="urlOrFileFolderPath-display-box display-box" tabindex="0" placeholder="<?php echo $LABEL_ITEM_URL_FILE_FOLDER_PATH_PLACEHOLDER; ?>">
                                        <!-- Dismissible urlOrFileFolderPaths will be displayed here -->
                                    </div>
                                    <input type="hidden" name="edit_item00_urlOrFileFolderPath" id="edit_item00_urlOrFileFolderPath" />
                                </div>
                            </div><!-- .col path div closed -->
                        </div>
                        <div id="edit_item00_collapsableDiv" class="row collapse in">
							<div class="col-lg-6 col-md-9 col-sm-10 col-xs-24">
                                <div class="form-group">
									<div class="input-group">
										<select class="form-control album_artist" name="edit_item00_artId_album_artist" id="edit_item00_artId_album_artist" placeholder = "<?php echo $LABEL_ALBUM_ARTIST_PLACEHOLDER; ?>">
											<!--option value="">-- Album Artist --</option-->
										</select>
										<input type="hidden" name="edit_item00_artText_album_artist" id="edit_item00_artText_album_artist" />
										<a tabindex="0" class="input-group-addon" id="edit_item00_addAlbumArtistLine" onclick="addAlbumArtistLine(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_ALBUM_ARTIST; ?>">
											<span class="glyphicon glyphicon-plus"></span>
										</a>
									</div>
                                </div>
                            </div><!-- .col album artist div closed -->
							<div class="col-lg-18 col-md-15 col-sm-14 col-xs-24">
                                <table width="100%">
									<tr>
										<td>
											<div class="form-group">
												<div id="edit_item00_albumArtistDisplayBox" class="album-artist-display-box display-box">
													<!-- Dismissible album artists will be displayed here -->
												</div>
											</div>
										</td>
									</tr>
								</table>
                            </div><!-- display album artist .col div closed -->
							<div class="col-lg-6 col-md-9 col-sm-10 col-xs-24">
                                <div class="form-group">
									<div class="input-group">
										<select class="form-control location" name="edit_item00_locId_location" id="edit_item00_locId_location">
											<!--option value="">-- Location --</option-->
										</select>
										<input type="hidden" name="edit_item00_locText_location" id="edit_item00_locText_location" />
										<a tabindex="0" class="input-group-addon" id="edit_item00_addLocationLine" onclick="addLocationLine(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_LOCATIONS; ?>">
											<span class="glyphicon glyphicon-plus"></span>
										</a>
									</div>
                                </div>
                            </div><!-- .col location div closed -->
							<div class="col-lg-18 col-md-15 col-sm-14 col-xs-24">
                                <table width="100%">
									<tr>
										<td>
											<div class="form-group">
												<div id="edit_item00_locationDisplayBox" class="location-display-box display-box">
													<!-- Dismissible locations will be displayed here -->
												</div>
											</div>
										</td>
									</tr>
								</table>
                            </div><!-- display location .col div closed -->
							<div class="col-lg-6 col-md-9 col-sm-10 col-xs-24">
                                <div class="form-group">
									<div class="input-group">
										<select class="form-control event" name="edit_item00_evtId_event" id="edit_item00_evtId_event">
											<!--option value="">-- Event --</option-->
										</select>
										<input type="hidden" name="edit_item00_evtText_event" id="edit_item00_evtText_event" />
										<a tabindex="0" class="input-group-addon" id="edit_item00_addEventLine" onclick="addEventLine(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_EVENTS; ?>">
											<span class="glyphicon glyphicon-plus"></span>
										</a>
									</div>
                                </div>
                            </div><!-- .col event div closed -->
							<div class="col-lg-18 col-md-15 col-sm-14 col-xs-24">
                                <div class="form-group">
                                    <div id="edit_item00_eventDisplayBox" class="event-display-box display-box">
                                        <!-- Dismissible events will be displayed here -->
                                    </div>
                                </div>
                            </div><!-- display event .col div closed -->
							<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
								<input class="form-control date form_datetime" id="edit_item00_eventStartDateTime" placeholder="<?php echo $LABEL_ITEM_EVENT_START_DATE_PLACEHOLDER; ?>" data-date="1970-01-16T00:00:00Z" data-date-format="dd M yyyy" data-link-field="dtp_input1" type="text" value="" readonly onblur="populateEndDate(this);" />
								<input type="hidden" id="dtp_input1" value="" />
                            </div>
							<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<div class="input-group">
										<input class="form-control date form_datetime" id="edit_item00_eventEndDateTime" placeholder="<?php echo $LABEL_ITEM_EVENT_END_DATE_PLACEHOLDER; ?>" data-date="1970-01-16T00:00:00Z" data-date-format="dd M yyyy" data-link-field="dtp_input2" type="text" value="" readonly />
										<a tabindex="0" class="input-group-addon" id="edit_item00_addDates" onclick="addDates(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_DATES; ?>">
                                        	<span class="glyphicon glyphicon-plus"></span>
                                        </a>
									</div>
									<input type="hidden" id="dtp_input2" value="" />
								</div>
                            </div>
							<div class="col-lg-18 col-md-12 col-sm-24 col-xs-24">
                                <div class="form-group">
                                    <div id="edit_item00_dateDisplayBox" class="date-display-box display-box">
                                        <!-- Dismissible dates will be displayed here -->
                                    </div>
                                </div>
                            </div><!-- .col date display div closed -->
							
							
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-24">
                                <div class="form-group">
                                    <a tabindex="0" id="edit_item00_save" class="btn btn-primary" onClick="sendToServer(this.id)" data-toggle="tooltip" data-original-title="<?php echo $LABEL_SAVE; ?>">
                                    	<span class="fa fa-save"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
						<div class="row">
							<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
								<div class="alert alert-sm alert-danger" id="edit_item00_errors_div" hidden>
									<!-- Show Warning/Error Messages here -->
								</div>
								<div class="alert alert-sm alert-success" id="edit_item00_messages_div" hidden>
									<!-- Show Success Messages here -->
								</div>
							</div>
						</div>
						<!-- Selected Audio Tracks will be added here -->
						<div class="row" id="audioTracks">

						</div>
						<!-- Hidden inputs -->
						<input type="hidden" name="edit_item00_parentItemId" id="edit_item00_parentItemId" />
						<input type="hidden" name="edit_item00_itemId" id="edit_item00_itemId" />
						<input type="hidden" name="edit_item00_itemType" id="edit_item00_itemType" value="3" />
					</form><!-- Audio form ends -->
                </div>
                
				
			</div><!-- .main div closed -->
		</div><!-- .row div closed -->
	</div><!-- .container-fluid #top div closed -->
	<a href="#top">
		<div class="scroll-to-top" data-toggle="tooltip" title="<?php echo $LABEL_SCROLL_TO_TOP; ?>">
			<span class="fa fa-arrow-up"></span>
		</div>
	</a>
	<!-- End video info page content -->
	
	<!-- Delete Confirmation Modal -->
	<div id="confirmDeleteModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<input type="hidden" name="del_item_number" id="del_item_number" />
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4><?php echo $LABEL_CONFIRM_DELETE_CLIP; ?></h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal" onClick="deleteClip($('#del_item_number').val());"><?php echo $LABEL_YES; ?></button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $LABEL_NO; ?></button>
				</div>
			</div>
		</div>
	</div>
	<!-- Delete Confirmation Modal ends -->
	
	<!-- Video Info Not Saved Modal -->
	<div id="videoInfoNotSavedModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4><?php echo $LABEL_VIDEO_INFO_NOT_SAVED; ?></h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal" onClick="sendToServer('edit_item00_save');"><?php echo $LABEL_SAVE_AND_PROCEED; ?></button>
					<button type="button" class="btn btn-default" data-dismiss="modal" onClick="document.write('<script> parent.location.href = \'<?php echo $FILE_VIDEOINFO_PHP; ?>\';</script>');"><?php echo $LABEL_PROCEED_ANYWAY; ?></button>
				</div>
			</div>
		</div>
	</div>
	<!-- Video Info Not Saved Modal ends -->
	
	<!-- Include scripts -->
	<?php
		require_once '../php/include/scripts.php';
		// 	<!-- Page specific JS -->
		require_once '../js/audioinfo.js.php';
		// 	<!-- Common JS -->
		require_once '../js/common.js.php';
		// 	<!-- Header JS -->
		require_once '../js/header.js.php';
	?>
</body>
</html>