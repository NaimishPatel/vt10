<?php
opcache_reset ();

require_once '../../api/session.php';
require_once '../../api/connection.php';
require_once '../php/include/PHP.php';
$itemId = $_REQUEST ['itemId'];
$_SESSION ['itemType'] = $ITEMTYPE_VIDEO;
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="<?php echo $LABEL_VIDEOINFO_PAGE_META_DESCRIPTION; ?>" />
<link href="<?php echo $FILE_FAVICON_PNG; ?>" rel="shortcut icon" />
<title><?php echo $LABEL_VIDEOINFO_PAGE_TITLE; ?></title>

<!-- Include styles -->
<?php require_once '../php/include/styles.php';?>
<!-- Page specific styles -->
<link href="<?php echo $FILE_COMMON_CSS; ?>" rel="stylesheet">
<link href="<?php echo $FILE_HEADER_CSS; ?>" rel="stylesheet">
<link href="<?php echo $FILE_VIDEOINFO_CSS; ?>" rel="stylesheet">
</head>

<body>
	<!-- Show Header -->
	<?php require_once 'header.php';?>
	<!-- Begin Video Info page content -->
	<div id="top" class="container-fluid">
		<div id="elfinder-content" class="row">
			<div class="main col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<!-- Begin elfinder content -->
				<div id="elfinder">
					<!-- elfinder will be displayed here -->
				</div>
			</div>
		</div>
		<div id="video-content" class="row" hidden>
			<div class="main col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-16 headerLeft12">
						<h3><?php echo $LABEL_VIDEO_INFORMATION; ?>
						<button type="button" class="btn btn-primary" onClick="setVideoJSONdata(this)" ><span class="glyphicon glyphicon-download-alt"></span> Fetch Details</button>
						</h3>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8 headerRight12">
					</div>
					<!-- Basic video details starts -->
					<form id="edit_item00_form" role="form">
						<!-- include in Detail view -->
						<div id="edit_item00_fixedDiv" class="row row_1">
							<div class="col-lg-16 col-md-16 col-sm-16 col-xs-16">
								<div class="form-group">
									<!-- div class="rxequired" -->
									<input type="text" class="form-control" name="edit_item00_itemTitle"
										id="edit_item00_itemTitle" placeholder="<?php echo $LABEL_ITEM_TITLE_PLACEHOLDER; ?>"
										autofocus>
									<!-- /div -->
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group">
									<select class="form-control dummy" name="edit_item00_dummy" tabindex="-1"
										id="dummy">
										<!--option value="">-- itemType2 --</option-->
									</select>
								</div>
							</div>
							<!-- .col title div closed -->
							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">
								<a id="edit_item00_switchParent1" class="btn btn-sm btn-warning collapsed" 
								data-toggle="tooltip" title=".$LABEL_SWITCH_TO_BRIEF_VIEW." data-placement="top" 
								onClick="switchParent(this);">
					 				<span class="sr-only">.$LABEL_SWITCH_TO_BRIEF_VIEW.</span>
					 				<span class="fa fa-list-alt"></span>
					 			</a>
							</div>
						</div>
						<!-- .row_1 -->

						<div class="row row_2">
							<div class="col-lg-16 col-md-16 col-sm-16 col-xs-16">
								<div class="form-group">
									<div class="input-group">
										<input type="text" class="form-control" name="edit_item00_urlOrFileFolderPathText"
											id="edit_item00_urlOrFileFolderPathText"
											placeholder="<?php echo $LABEL_ITEM_URL_FILE_FOLDER_PATH_PLACEHOLDER; ?>"> <a
											tabindex="0" class="input-group-addon" id="edit_item00_addUrlOrFileFolderPath"
											onclick="addUrlOrFileFolderPath($('#edit_item00_urlOrFileFolderPathText').val());"
											data-toggle="tooltip"
											data-original-title="<?php echo $LABEL_ADD_ITEM_URL_FILE_FOLDER_PATH_PLACEHOLDER; ?>"> <span
											class="glyphicon glyphicon-plus"></span>
										</a>
									</div>
									<!--input type="hidden" name="edit_item00_urlOrFileFolderPath" id="edit_item00_urlOrFileFolderPath" /-->
								</div> 
							</div>
							<!-- .col urlOrFileFolderPaths div closed -->
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<div class="form-group">
									<select class="form-control itemType2" name="edit_item00_itemType2"
										id="edit_item00_itemType2">
										<!--option value="">-- itemType2 --</option-->
									</select>
								</div>
							</div>
							<!-- .col itemType2 div closed -->
						</div>
						<!-- end .row_2 -->
						<div class="row row_3">
							<div class="col-lg-16 col-md-16 col-sm-16 col-xs-16">
								<div class="form-group">
									<div id="edit_item00_urlOrFileFolderPathDisplayBox"
										class="urlOrFileFolderPath-display-box display-box">
										<!-- Dismissible urlOrFileFolderPaths will be displayed here -->
									</div>
								</div>
							</div>
							<!-- .col path div closed -->
						</div>
						<div class="row row_3" style="margin-top: 5px; height: 100px">
							<div class="col-lg-22 col-md-22 col-sm-22 col-xs-22 row_3" style="height: 100px">
								<select class="form-control tags" name="edit_item00_tags"
									id="edit_item00_tags">
								</select>
							</div>
							<!-- .col tags div closed -->
						</div>
						<!-- .row_3 -->
				<div id="edit_item00_collapsableDiv" class="row collapse in">
						<div class="row row_4" >
							<div class="col-lg-22 col-md-22 col-sm-22 col-xs-22">
								<div class="form-group">
									<textarea class="form-control" rows="3" name="edit_item00_additionalDescp"
										id="edit_item00_additionalDescp"
										placeholder="<?php echo $LABEL_ITEM_ADDITIONAL_DESCP_PLACEHOLDER; ?>"></textarea>
								</div>
							</div>
						</div>
						<!-- end .row_5 -->
						<div class="row row_5">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<span style="font-size: xx-small; z-index: 1000; display: none">Dec-15-2005 or 12-15-2005</span>
								<div class="input-group">
									<input class="form-control date form_datetime" id="edit_item00_eventStartDateTime"
										placeholder="<?php echo $LABEL_ITEM_EVENT_START_DATE_PLACEHOLDER; ?>"
										data-date="1970-01-16T00:00:00Z" data-date-format="dd M yyyy"
										data-link-field="dtp_input1" type="text" value="" 
										 /> <span class="input-group-addon"> <span
										class="glyphicon glyphicon-calendar datetime_picker_icon" data-target="edit_item00_eventStartDateTime"></span>
									</span>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<div class="form-group">
									<div class="input-group">
										<input class="form-control date form_datetime" id="edit_item00_eventEndDateTime"
											placeholder="<?php echo $LABEL_ITEM_EVENT_END_DATE_PLACEHOLDER; ?>"
											data-date="1970-01-16T00:00:00Z" data-date-format="dd M yyyy"
											data-link-field="dtp_input2" type="text" value="" /> <a tabindex="-1"
											class="input-group-addon" id="edit_item00_addDates" 
											data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_DATES; ?>"> <span
											class="glyphicon glyphicon-calendar datetime_picker_icon" data-target="edit_item00_eventEndDateTime"></span>
										</a>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<select class="form-control technicalQuality" name="edit_item00_technicalQuality"
										id="edit_item00_technicalQuality">
										<!--option value="">-- Quality --</option-->
									</select>
								</div>
							</div>
							<!-- .col quality div closed -->
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<div class="form-group">
									<select class="form-control audioQuality" name="edit_item00_audioQuality"
										id="edit_item00_audioQuality">
										<!--option value="">-- Quality --</option-->
									</select>
								</div>
							</div>
							<!-- .col audioQuality div closed -->
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<div class="form-group">
									<select class="form-control accessibility" name="edit_item00_accessibility"
										id="edit_item00_accessibility">
									</select>
								</div>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<div class="form-group">
									<select class="form-control language" name="edit_item00_langId_language"
										id="edit_item00_langId_language">
										<!--option value="">-- Language --</option-->
									</select> <input type="hidden" name="edit_item00_langText_language"
										id="edit_item00_langText_language" />
								</div>
							</div>
							<!-- .col language div closed -->
						</div>
						<div class="row row_6">
							<div class="col-lg-16 col-md-16 col-sm-16 col-xs-16">
									<div class="form-group">
										<select class="form-control itemCategory" name="edit_item00_itemCategory"
											id="edit_item00_itemCategory">
											<!--option value="">-- Category1 --</option-->
										</select>
									</div>
									<!-- .form-group div closed -->
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<select class="form-control confidentiality" name="edit_item00_confidentiality"
											id="edit_item00_confidentiality">
											<!--option value="">-- Confidentiality --</option-->
										</select>
									</div>
									<!-- .form-group div closed -->
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-24">
								<div class="form-group" >
									<a tabindex="0" id="edit_item00_save" class="btn btn-primary"
										onClick="sendToServer(this.id, '<?php echo $_SESSION['itemType']; ?>');"
										data-toggle="tooltip" data-original-title="<?php echo $LABEL_SAVE; ?>"> <span
										class="fa fa-save"></span>
									</a>
								</div>
							</div>
						</div>
				</div>
						<div class="row">
							<div class="col-lg-18 col-md-18 col-sm-24 col-xs-24">
								<div class="alert alert-sm alert-danger" id="edit_item00_errors_div" hidden>
									<!-- Show Warning/Error Messages here -->
								</div>
								<div class="alert alert-sm alert-success" id="edit_item00_messages_div" hidden>
									<!-- Show Success Messages here -->
								</div>
							</div>
						</div>
						<!-- Hidden inputs -->
						<input type="hidden" name="edit_item00_parentItemId" id="edit_item00_parentItemId" /> <input
							type="hidden" name="edit_item00_itemId" id="edit_item00_itemId" /> <input type="hidden"
							name="edit_item00_itemType1" id="edit_item00_itemType1"
							value="<?php echo $_SESSION['itemType']; ?>" />
					</form>
					<!-- Basic video details ends -->
				</div>
				
				<style>
					.slider{
					
					}
					.cursor {
					  cursor: pointer;
					}
					.slider_row:after {
					  content: "";
					  display: table;
					  clear: both;
					  
					}
					
					/* Six columns side by side */
					.column {
					  float: left;
					  width: 15.50%;
					  height:100px;
					  border: solid 0.5px;
    				margin: 5PX;
					}
					
					/* Add a transparency effect for thumnbail images */
					.demo {
					  opacity: 0.6;
					}
					
					.active,
					.demo:hover {
					  opacity: 1;
					}
				</style>
				<div class="row slider">
					<div class="col-lg-22 slider_row" style="border: solid 0.5px;">
						<div class="clip-thumnails">
						</div>
						<div>
							<div class="column" style="text-align: center;border:dotted 1px;">
								<!--   <button onclick="addClipInfo(false)" style="background: white;border: none;">+</button>-->
								<a href="#" style="text-decoration: none" onclick="addClipInfo(true)">
									<span class="glyphicon glyphicon-plus" style="cursor:pointer; padding-top: 7%; font-size: 50px;"></span>
									<span style="display: block">Save & Add Clip</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				
				
				<div id="addClip_div" class="row">
				</div>
				<!-- Add clip information button ends -->

				<!-- Clips will be displayed here-->

			</div>
			<!-- .main div closed -->
		</div>
		<!-- .row div closed -->
	</div>
	<!-- .container-fluid #top div closed -->
	<a href="#top">
		<div class="scroll-to-top" data-toggle="tooltip" title="<?php echo $LABEL_SCROLL_TO_TOP; ?>">
			<span class="fa fa-arrow-up"></span>
		</div>
	</a>
	<!-- End video info page content -->

	<!-- Delete Confirmation Modal -->
	<div id="confirmDeleteModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<input type="hidden" name="del_item_number" id="del_item_number" />
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4><?php echo $LABEL_CONFIRM_DELETE_CLIP; ?></h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal"
						onClick="deleteClip($('#del_item_number').val());"><?php echo $LABEL_YES; ?></button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $LABEL_NO; ?></button>
				</div>
			</div>
		</div>
	</div>
	<!-- Delete Confirmation Modal ends -->

	<!-- Video Info Not Saved Modal -->
	<div id="videoInfoNotSavedModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4><?php echo $LABEL_VIDEO_INFO_NOT_SAVED; ?></h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal"
						onClick="sendToServer('edit_item00_save');"><?php echo $LABEL_SAVE_AND_PROCEED; ?></button>
					<button type="button" class="btn btn-default" data-dismiss="modal"
						onClick="document.write('<script> parent.location.href=\'<?php echo $FILE_VIDEOINFO_PHP; ?>\';
						
						</script>');"><?php echo $LABEL_PROCEED_ANYWAY; ?></button>
				</div>
			</div>
		</div>
	</div>
	<!-- Video Info Not Saved Modal ends -->

	<!-- Include scripts -->
	<?php
	require_once '../php/include/scripts.php';
	// <!-- Page specific JS -->
	require_once '../js/videoinfo.js.php';
	// <!-- Common JS -->
	require_once '../js/common.js.php';
	// <!-- Header JS -->
	require_once '../js/header.js.php';
	
	require_once 'vlcjson.php';
	
	require_once 'vlcplaylistjson.php';
	?>
</body>
</html>