<?php
opcache_reset();

require_once '../../api/session.php';
require_once '../../api/connection.php';
require_once '../php/include/PHP.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="" />
<link href="<?php echo $FILE_FAVICON_PNG; ?>" rel="shortcut icon" />
<title>Example1</title>

<!-- Include styles -->
<?php require_once '../php/include/styles.php';?>
<!-- Page specific styles -->
<link href="<?php echo $FILE_COMMON_CSS; ?>" rel="stylesheet">
</head>

<body>
	<!-- Begin page content -->
	<div id="top" class="container-fluid">
		<div id="content" class="row" >
			<div class="main col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<div class="row">
					<!-- Basic image details starts -->
                    <form id="edit_item00_form" role="form">
						<div class="col-lg-24 col-md-9 col-sm-10 col-xs-24">
							<div class="form-group">
								<div class="input-group">
									<select class="form-control event1" name="edit_item00_evtId_event" id="edit_item00_evtId_event">
										<!--option value="">-- Event --</option-->
									</select>
									<input type="hidden" name="edit_item00_evtText_event" id="edit_item00_evtText_event" />
									<a tabindex="0" class="input-group-addon" id="edit_item00_addEventLine" onclick="addEventLine(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_EVENTS; ?>">
										<span class="glyphicon glyphicon-plus"></span>
									</a>
								</div>
							</div>
						</div><!-- .col event div closed -->
						
						<div class="col-lg-24 col-md-9 col-sm-10 col-xs-24">
							<div class="form-group">
								<div class="input-group">
									<select class="form-control subject1" name="edit_item00_evtId_event" id="edit_item00_evtId_event">
										<!--option value="">-- Event --</option-->
									</select>
									<input type="hidden" name="edit_item00_evtText_event" id="edit_item00_evtText_event" />
									<a tabindex="0" class="input-group-addon" id="edit_item00_addEventLine" onclick="addEventLine(this.id);" data-toggle="tooltip" data-original-title="<?php echo $LABEL_ADD_EVENTS; ?>">
										<span class="glyphicon glyphicon-plus"></span>
									</a>
								</div>
							</div>
						</div><!-- .col event div closed -->
						<div class="col-lg-24 col-md-8 col-sm-8 col-xs-24">
							<div class="form-group">
								<select class="form-control typeX" name="edit_item00_itemType2" id="edit_item00_itemType2">
									<!--option value="">-- itemType2 --</option-->
								</select>
							</div>
						</div><!-- .col itemType2 div closed -->
						
						<?php 
							function select2($eleName) {
								return '<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
											<div class="form-group">
												<select class="form-control '.$eleName.'" name="edit_item00_'.$eleName.'" id="edit_item00_'.$eleName.'"></select>
											</div>
										</div><!-- .col '.$eleName.' div closed -->';
							}
							
							function select2withDisplayBox($eleName, $eleTooltipTitle) {
								$shortEleName = substr($eleName, 0, 3);
								return '<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
											<div class="form-group">
												<div class="input-group">
													<select class="form-control '.$eleName.'" name="edit_item00_'.$shortEleName.'Id_'.$eleName.'" id="edit_item00_'.$shortEleName.'Id_'.$eleName.'"></select>
													<input type="hidden" name="edit_item00_'.$shortEleName.'Text_'.$eleName.'" id="edit_item00_'.$shortEleName.'Text_'.$eleName.'" />
													<a tabindex="0" class="input-group-addon" id="edit_item00_addLine" onclick="addLine(this.id, \''.$eleName.'\');" data-toggle="tooltip" data-original-title="'.$eleTooltipTitle.'">
														<span class="glyphicon glyphicon-plus"></span>
													</a>
												</div>
											</div>
										</div><!-- .col '.$eleName.' div closed -->
										<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
											<div class="form-group">
												<div id="edit_item00_'.$eleName.'DisplayBox" class="'.$eleName.'-display-box display-box">
													<!-- Dismissibles will be displayed here -->
												</div>
											</div>
										</div><!-- display '.$eleName.' .col div closed -->';
							}
							echo select2withDisplayBox("reference", "References");
							// echo select2("book");
						?>
						
						
						
						<div class="row">
							<div class="col-lg-24 col-md-24 col-sm-24 col-xs-24">
								<div class="alert alert-sm alert-danger" id="edit_item00_errors_div" hidden>
									<!-- Show Warning/Error Messages here -->
								</div>
								<div class="alert alert-sm alert-success" id="edit_item00_messages_div" hidden>
									<!-- Show Success Messages here -->
								</div>
							</div>
						</div>
						<!-- Hidden inputs -->
						<input type="hidden" name="edit_item00_itemId" id="edit_item00_itemId" />
					</form><!-- Basic image details ends -->
                </div>				
			</div><!-- .main div closed -->
		</div><!-- .row div closed -->
	</div><!-- .container-fluid #top div closed -->
	<a href="#top">
		<div class="scroll-to-top" data-toggle="tooltip" title="<?php echo $LABEL_SCROLL_TO_TOP; ?>">
			<span class="fa fa-arrow-up"></span>
		</div>
	</a>
	<!-- End image info page content -->
		
	<!-- Include scripts -->
	<?php
		require_once '../php/include/scripts.php';
		// 	<!-- Page specific JS -->
		require_once '../js/example1.js.php';
		// 	<!-- Common JS -->
		require_once '../js/common.js.php';
	?>
</body>
</html>