<?php
opcache_reset();
?>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
    	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
    <img src="<?php echo $FILE_BAPS_LOGO_BG_PNG;?>" width="41" height="49" class="img-responsive" align="left" style="padding:5px;">
    <div class="navbar-brand"><a href="<?php echo $FILE_DASHBOARD_PHP;?>" class="title"><?php echo $APP_NAME; ?></a> <sub class="small"><em><?php echo $VERSION; ?></em></sub></div>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo $FILE_VIDEOSEARCH_PHP;?>"><span class="glyphicon glyphicon-search" style="padding-right:5px;"></span><?php echo $LABEL_SEARCH; ?></a></li>
		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-share" style="padding-right:5px;"></span><?php echo $LABEL_ADD_NEW_ENTRY; ?><span class="caret"></span></a>
        <ul class="dropdown-menu">
			<li><a href="<?php echo $FILE_VIDEOINFO_PHP."?showExplorer=true";?>"><span class="glyphicon glyphicon-film" style="padding-right:5px;"></span><?php echo $LABEL_ADD_VIDEO.$LABEL_USING_FILE_EXPLORER; ?></a></li>
			<li><a href="<?php echo $FILE_VIDEOINFO_PHP;?>"><span class="glyphicon glyphicon-film" style="padding-right:5px;"></span><?php echo $LABEL_ADD_VIDEO; ?></a></li>
            <li><a href="<?php echo $FILE_IMAGE_PHP."?showExplorer=true";?>"><span class="glyphicon glyphicon-picture" style="padding-right:5px;"></span><?php echo $LABEL_ADD_IMAGE.$LABEL_USING_FILE_EXPLORER; ?></a></li>
            <li><a href="<?php echo $FILE_IMAGE_PHP;?>"><span class="glyphicon glyphicon-picture" style="padding-right:5px;"></span><?php echo $LABEL_ADD_IMAGE; ?></a></li>
			<li><a href="<?php echo $FILE_PRASANG_PHP;?>"><span class="fa fa-file-text" style="padding-right:5px;"></span><?php echo $LABEL_ADD_PRASANG; ?></a></li>
        	<li class="disabled"><a><span class="glyphicon glyphicon-folder-open" style="padding-right:5px;"></span><?php echo $LABEL_ADD_FOLDER; ?></a></li>
            <li class="disabled"><a><span class="glyphicon glyphicon-volume-up" style="padding-right:5px;"></span><?php echo $LABEL_ADD_AUDIO; ?></a></li>
            <li class="disabled"><a><span class="glyphicon glyphicon-file" style="padding-right:5px;"></span><?php echo $LABEL_ADD_DOCUMENT; ?></a></li>
            <li class="disabled"><a><span class="glyphicon glyphicon-time" style="padding-right:5px;"></span><?php echo $LABEL_ADD_EVENT; ?></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo $IPEOPLE_PORTAL; ?>" target="_blank"><span class="glyphicon glyphicon-user" style="padding-right:5px;"></span><?php echo $LABEL_ADD_PERSON_DETAILS; ?></a></li>
        </ul>
        </li>
        <li class="disabled"><a href="#"><span class="glyphicon glyphicon-edit" style="padding-right:5px;"></span><?php echo $LABEL_EDIT_OLD_ENTRY; ?></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user" style="padding-right:5px;"></span><?php echo $_SESSION['username']; ?> <span class="caret"></span></a>
            	<ul class="dropdown-menu">
                	<li><a href="#" data-toggle="modal" data-target="#account_settings"><span class="glyphicon glyphicon-cog" style="padding-right:5px;"></span><?php echo $LABEL_ACCOUNT_SETTINGS; ?></a></li>
                    <li class="disabled"><a href="#"><span class="glyphicon glyphicon-envelope" style="padding-right:5px;"></span><?php echo $LABEL_CONTACT_ADMIN; ?></a></li>
                    <li role="separator" class="divider"></li>
            		<li><a href="<?php echo $FILE_INDEX_PHP;?>"><span class="glyphicon glyphicon-log-out" style="padding-right:5px;"></span><?php echo $LABEL_LOGOUT; ?></a></li>
                </ul>
            </li>
      </ul>
      </div>
  </div>
</nav>

	<div class="modal fade" id="account_settings" role="dialog">
        <div class="modal-dialog account-settings-modal">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="display:inline;">&times;</button>
                    <h2 style="line-height:0.5;"><?php echo $LABEL_ACCOUNT_SETTINGS; ?></h2>
                </div>
                <div class="modal-body">
					<div class="row">
                        <div class="col-sm-6 col-md-6 account-settings-sidebar">
                            <ul class="nav account-settings nav-sidebar" role="tablist">
                                <li role="presentation" class="active"><a data-toggle="tab" href="#personal-info"><span class="fa fa-user"></span><?php echo $LABEL_PERSONAL_INFO; ?></a></li>
                                <li role="presentation"><a data-toggle="tab" href="#security"><span class="fa fa-lock"></span><?php echo $LABEL_SECURITY; ?></a></li>
                                <li role="presentation"><a data-toggle="tab" href="#preferences"><span class="fa fa-star"></span><?php echo $LABEL_PREFERENCES; ?></a></li>
                            </ul>
                        </div>
						<form name="form_account_settings" id="form_account_settings" >
                    		<div class="tab-content">
                                <div id="personal-info" class="col-sm-18 col-md-18 col-xs-offset-6 tab-pane in active">
                                    <h3 style="margin:0px 5px 5px 5px;"><?php echo $LABEL_PERSONAL_INFO; ?></h3>
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="username"><?php echo $LABEL_USERNAME; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="username" id="username" value="<?php echo $_SESSION['username']; ?>" readonly />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="first_name"><?php echo $LABEL_FIRST_NAME; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="first_name" id="first_name" value="<?php echo $_SESSION['username']; ?>" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="last_name"><?php echo $LABEL_LAST_NAME; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="last_name" id="last_name" value="<?php echo $_SESSION['username']; ?>" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="eMail"><?php echo $LABEL_EMAIL; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="eMail" id="eMail" value="<?php echo $_SESSION['user_email']; ?>" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="phone"><?php echo $LABEL_PHONE_NUMBER; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="phone" id="phone" value="<?php echo $_SESSION['user_mobile']; ?>" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="site"><?php echo $LABEL_SITE_CENTER; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="site" id="site" value="<?php echo $_SESSION['username']; ?>" readonly />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="security" class="col-sm-18 col-md-18 col-xs-offset-6 tab-pane">
                                    <h3 style="margin:0px 5px 5px 5px;"><?php echo $LABEL_SECURITY; ?></h3>
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="role"><?php echo $LABEL_ROLE_LEVEL; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="role" id="role" value="Super admin" readonly />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="privileges"><?php echo $LABEL_PRIVILEGES; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="privileges" id="privileges" value="All access" readonly />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td colspan="2"><h2><small><?php echo $LABEL_CHANGE_PASSWORD; ?></small></h2></td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="old_password"><?php echo $LABEL_OLD_PASSWORD; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <input class="form-control" type="password" name="old_password" id="old_password" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="new_password"><?php echo $LABEL_NEW_PASSWORD; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <input class="form-control" type="password" name="new_password" id="new_password" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="confirm_password"><?php echo $LABEL_CONFIRM_PASSWORD; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <input class="form-control" type="password" name="confirm_password" id="confirm_password" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="preferences" class="col-sm-18 col-md-18 col-xs-offset-6 tab-pane">
                                    <h3 style="margin:0px 5px 5px 5px;"><?php echo $LABEL_PREFERENCES; ?></h3>
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <td width="25%" valign="middle"><label class="control-label" for="font-size"><?php echo $LABEL_FONT_SIZE; ?></label></td>
                                                    <td valign="middle">
                                                        <div class="form-group">
                                                            <label class="radio-inline font-size" id="small" style="font-size:12px;">
                                                                <input type="radio" name="fontSize" id="fontSizeSml" value="small" onClick="document.body.style.fontSize = '12px'" /><span class="fa fa-font"></span>
                                                            </label>
                                                            <label class="radio-inline font-size active" id="regular" style="font-size:14px;">
                                                                <input type="radio" name="fontSize" id="fontSizeReg" value="regular" onClick="document.body.style.fontSize = '14px'" checked /><span class="fa fa-font"></span>
                                                            </label>
                                                            <label class="radio-inline font-size" id="large" style="font-size:16px;">
                                                                <input type="radio" name="fontSize" id="fontSizeLrg" value="large" onClick="document.body.style.fontSize = '16px'" /><span class="fa fa-font"></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td width="25%" valign="middle"><label class="control-label" for="language"><?php echo $LABEL_LANGUAGE; ?></label></td>
                                                    <td valign="middle">
                                                    	<div class="form-group">
                                                            <select class="form-control" name="language" id="language" >
                                                                <option value="English" selected>English</option>
                                                                <option value="Gujarati">Gujarati</option>
                                                            </select>
														</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td width="25%" valign="middle"><label class="control-label" for="naming_format"><?php echo $LABEL_NAMING_FORMAT; ?></label></td>
                                                    <td valign="middle">
                                                    	<div class="form-group">
                                                            <select class="form-control" name="naming_format" id="naming_format" >
                                                                <option value="FN LN" selected><?php echo $LABEL_FIRST_NAME_LAST_NAME; ?></option>
                                                                <option value="LN, FN"><?php echo $LABEL_LAST_NAME_FIRST_NAME; ?></option>
                                                            </select>
														</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td width="25%" valign="middle"><label class="control-label" for="date_format"><?php echo $LABEL_DATE_FORMAT; ?></label></td>
                                                    <td valign="middle">
                                                    	<div class="form-group">
                                                            <select class="form-control" name="date_format" id="date_format" >
                                                                <option value="dd-mm-yyyy"><?php echo $LABEL_dd_mm_yyyy; ?></option>
                                                                <option value="mm-dd-yyyy"><?php echo $LABEL_mm_dd_yyyy; ?></option>
                                                                <option value="month DD, YYYY" selected><?php echo $LABEL_month_DD_YYYY; ?></option>
                                                            </select>
														</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
							</div> <!-- tab-content -->
						</form>
					</div> <!-- row end -->
                </div> <!-- Modal Body End -->
                <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $LABEL_CLOSE; ?></button>
					<button type="button" class="btn btn-primary" onClick="saveAccSettings(); " ><?php echo $LABEL_SAVE_CHANGES; ?></button>
                </div>
            </div>
        </div>
    </div>
