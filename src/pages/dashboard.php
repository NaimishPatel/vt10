<?php
opcache_reset();

require_once '../../api/session.php';
require_once '../../api/connection.php';
require_once '../php/include/PHP.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="<?php echo $LABEL_VIDEOINFO_PAGE_META_DESCRIPTION; ?>" />
<link href="<?php echo $FILE_FAVICON_PNG; ?>" rel="shortcut icon" />
<title><?php echo $LABEL_VIDEOINFO_PAGE_TITLE; ?></title>

<!-- Include styles -->
<?php require_once '../php/include/styles.php';?>
<!-- Page specific styles -->
<link href="<?php echo $FILE_COMMON_CSS; ?>" rel="stylesheet">
<link href="<?php echo $FILE_HEADER_CSS; ?>" rel="stylesheet">
<link href="<?php echo $FILE_DASHBOARD_CSS; ?>" rel="stylesheet">
</head>

<body>
	<!-- Show Header -->
	<?php require_once 'header.php';?>

	<!-- Begin Dashboard page content -->
	<div id="top" class="container-fluid">
		<div class="row">
			<div class="main col-lg-24 col-md-24 col-sm-24 col-xs-24">
				<h1 class="page-header"><?php echo "Dashboard"; ?></h1><!-- Title -->

				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h3><?php echo "News"; ?></h3>
						<ul>
							<li>You can now <b>Tag Images!</b> Have a look... <a href="<?php echo $FILE_IMAGE_PHP; ?>">Click me</a></li>
							<li>Users can now <b>add items without using File Explorer.</b> File path is made editable</li>
							<li><b>File Explorer</b> is now added to tag Videos easily!!! Check this new feature <a href="<?php echo $FILE_VIDEOINFO_PHP; ?>">here</a></li>
							<li>With this new File Explorer, you can now <b>tag multiple video files</b> :)</li>
							<li><b>Search from Favourite Locations</b> instead of all the Locations</li>
						</ul>
                   	</div>
				</div>
				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h3><?php echo "Upcoming Features"; ?></h3>
						<ul>
							<li><b>Tagging Prasangs</b> will soon be a part of this App! Have a look at the current status... <a href="<?php echo $FILE_PRASANG_PHP; ?>">Click me</a></li>
							<li><b>Tagging Audio</b> will soon be a part of this App! Have a look at the current status... <a href="<?php echo $FILE_AUDIO_PHP; ?>">Click me</a></li>
						</ul>
                   	</div>
				</div>
				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h3><?php echo "Currently Working On"; ?></h3>
						<ul>
							<li><b>Tagging Prasangs:</b> Have a look at the current status... <a href="<?php echo $FILE_PRASANG_PHP; ?>">Click me</a></li>
						</ul>
                   	</div>
				</div>
				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h3><?php echo "Forum"; ?></h3>
						<ul>
							<li>Suggest a nice name for this App</li>
						</ul>
                   	</div>
				</div>
				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h3><?php echo "Things To Do"; ?></h3>
						<ul>
							<li>Pu. Satyanishth Swami: Split Folder and File name</li>
							<li>Pu. Satyanishth Swami: Loop through multiple files</li>
							<li>Pu. Satyanishth Swami: Add PeopleSuggestions, SaintSuggestions to Keywords box</li>
							<li>Pu. Satyanishth Swami: Clean-up DB</li>
							<li>Pu. Satyanishth Swami: Clean-up Code</li>
							<li>Sandipani: Adding elements to dismissible boxes does not get noticed as the section does not expand. Find some way</li>
							<li>Sandipani: Select2 dropdowns doesn't expand on down arrow keypress</li>
							<li>Sandipani: Pressing enter twice in select2 suggestion boxes behaves abnormally. Find some way</li>
							<li>Sandipani: Add all BAPS Sites to Locations</li>
						</ul>
                   	</div>
				</div>
				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h3><?php echo "Bugs"; ?></h3>
						<ul>
							<li>On adding new subjects after deleting all previous ones increments the line/word number instead of starting from 1</li>
							<li>While attempting to add another item without saving the current one, no error/prompt to save is displayed</li>
						</ul>
                   	</div>
				</div>
				<!--div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h3><?php echo "Version Info"; ?></h3>
                   	</div>
				</div>
				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h3><?php echo "Recent Activities"; ?></h3>
                   	</div>
				</div>
				<div class="row">
					<div class="col-lg-23 col-md-23 col-sm-23 col-xs-23">
						<h3><?php echo "Stats"; ?></h3>
                   	</div>
				</div-->
			</div><!-- .main div closed -->
		</div><!-- .row div closed -->
	</div><!-- .container-fluid #top div closed -->
	<a href="#top">
		<div class="scroll-to-top" data-toggle="tooltip" title="<?php echo $LABEL_SCROLL_TO_TOP; ?>">
			<span class="fa fa-arrow-up"></span>
		</div>
	</a>
	<!-- End Dashboard page content -->
	
	<!-- Include scripts -->
	<?php
		require_once '../php/include/scripts.php';
		// 	<!-- Page specific JS -->
		require_once '../js/dashboard.js.php';
		// 	<!-- Common JS -->
		require_once '../js/common.js.php';
		// 	<!-- Header JS -->
		require_once '../js/header.js.php';
	?>
</body>
</html>