<?php
	opcache_reset();
?>
<script>
<?php
	if($itemId != null && $itemId != "") {
		echo "viewMode = true;";
		echo "itemId = '".$itemId."';";
	}
	else {
		echo "viewMode = false;";
	 // echo "alert(JsonString);";
	}
?>

$(document).ready(function(){
	$(window).scroll(function() {
		if ($(document).scrollTop() > 200) {
		  $(".scroll-to-top").fadeIn(200);
		} else {
		  $(".scroll-to-top").fadeOut(200);
		}
	});
	
	// $('#elfinder').hide();
	// $('#video-content').show();
	
	elFinder.prototype.i18.en.messages['cmdtag'] = 'Tag';          
    elFinder.prototype._options.commands.push('tag');
    elFinder.prototype.commands.tag = function() {
        this.exec = function(hashes) {
             //do whatever
			 // alert(this.fm.path(hashes));
			 var filePath = this.fm.path(hashes);
			$.post("<?php echo $FILE_AUDIO_FILE_INFO_RETRIEVE_PHP; ?>", {paths : 
			
			/*
 			 var filePaths = [];
			 var loopIndx;
			 for(loopIndx = 0; loopIndx < hashes.length; loopIndx++) {
				
				addUrlOrFileFolderPath(this.fm.path(hashes[loopIndx]));
				filePaths[loopIndx] = this.fm.path(hashes[loopIndx]);
			}
			 $('#edit_item00_urlOrFileFolderPath').val(filePaths.join('<?php echo $FILE_PATH_SEPERATOR; ?>'));
			 $('#elfinder').hide();
			 $('#audio-content').show();
			 if(hashes.length > 1)
				$("#addClip_div").hide();
			*/	
				
        }
        this.getstate = function() {
            //return 0 to enable, -1 to disable icon access
            return 0;
        }
    }
	

	// Append options to select dropdowns - Category
	$('.itemCategory').select2({
		placeholder: "<?php echo $LABEL_CATEGORY_PLACEHOLDER; ?>",
		data: <?php echo $ITEM_CATEGORY_LOOKUPS; ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 0
	});
	$('.itemCategory').val(null).trigger("change");
	bindVideoEventsAndComponents(false);
	
	if(viewMode) {	//populate the page
		// JsonString = '[{"itemId":"123661","parentItemId":"","itemCategory":"1.H","itemTitle":"Guruhari Darshan trial","technicalQuality":"3","technicalNotes":"Good","urlOrFileFolderPath":"My Videos","itemType":"1","itemSource":"bapsChannel","confidentiality":"1","accessibility":"01","language":"001","additionalDescp":"Description for Darshan","eventDateArray":[{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"12","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"12"},{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"11","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"11"}],"keywordLineArray":[{"keywordLine":"3","keywordIndex":"1","keywordId":"10","keywordText":"Pramukh Swami Maharaj"},{"keywordLine":"3","keywordIndex":"2","keywordId":"wearing","keywordText":"wearing"},{"keywordLine":"3","keywordIndex":"3","keywordId":"1097","keywordText":"Chakdis"},{"keywordLine":"4","keywordIndex":"1","keywordId":"sadhus","keywordText":"sadhus"},{"keywordLine":"4","keywordIndex":"2","keywordId":"1759","keywordText":"dancing"}],"subjectArray":[{"subjectLine":"1","subjectIndex":"1","subjectId":"2177","subjectText":"કાન / Ears"},{"subjectLine":"2","subjectIndex":"1","subjectId":"3506","subjectText":"Topic"}],"eventArray":[{"eventLine":"1","eventIndex":"1","eventId":"1910","eventText":"1995 - Amrut Mahotsav"},{"eventLine":"2","eventIndex":"1","eventId":"1907","eventText":"1990 - Yuva Adhiveshan"}],"locationArray":[{"locationId":"10105275","locationText":"Salangpur, BAPS Mandir","locationTree":"Salangpur, BAPS Mandir"},{"locationId":"10105274","locationText":"Mumbai (M. Corp.)","locationTree":"Mumbai (M. Corp.)"}]},{"itemId":"123665","parentItemId":"123661","itemCategory":"1.H","itemTitle":"Guruhari Darshan trial","technicalQuality":"3","technicalNotes":"Good","urlOrFileFolderPath":"My Videos","itemType":"1","itemSource":"bapsChannel","confidentiality":"1","accessibility":"01","language":"001","additionalDescp":"Description for Darshan","eventDateArray":[{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"12","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"12"},{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"11","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"11"}],"keywordLineArray":[{"keywordLine":"3","keywordIndex":"1","keywordId":"10","keywordText":"Pramukh Swami Maharaj"},{"keywordLine":"3","keywordIndex":"2","keywordId":"wearing","keywordText":"wearing"},{"keywordLine":"3","keywordIndex":"3","keywordId":"1097","keywordText":"Chakdis"},{"keywordLine":"4","keywordIndex":"1","keywordId":"sadhus","keywordText":"sadhus"},{"keywordLine":"4","keywordIndex":"2","keywordId":"1759","keywordText":"dancing"}],"subjectArray":[{"subjectLine":"1","subjectIndex":"1","subjectId":"2177","subjectText":"કાન / Ears"},{"subjectLine":"2","subjectIndex":"1","subjectId":"3506","subjectText":"Topic"}],"eventArray":[{"eventLine":"1","eventIndex":"1","eventId":"1910","eventText":"1995 - Amrut Mahotsav"},{"eventLine":"2","eventIndex":"1","eventId":"1907","eventText":"1990 - Yuva Adhiveshan"}],"locationArray":[{"locationId":"10105275","locationText":"Salangpur, BAPS Mandir","locationTree":"Salangpur, BAPS Mandir"},{"locationId":"10105274","locationText":"Mumbai (M. Corp.)","locationTree":"Mumbai (M. Corp.)"}]}]';
		$('#elfinder').hide();
		$('#audio-content').show();
		var strUrl = "parentItemId=" + itemId;
		xmlhttp = getXmlhttp();
		xmlhttp.onreadystatechange = function()	{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				if(xmlhttp.responseText != "") {
					var JsonString = xmlhttp.responseText;
					var jsonData = JSON.parse(JsonString);
					// alert(JsonString);
					populateItemToView(jsonData);
				} else {
					//do nothing
				}
			}
		}
		console.log(strUrl);
		xmlhttp.open("POST", "<?php echo $FILE_ITEM_VIEW_PHP; ?>?" + strUrl, false); //encodeURIComponent(strUrl)
		xmlhttp.send();
	} else {
		$('#elfinder').elfinder({
			url : '../php/remote/connector.audio.php',  // connector URL (REQUIRED)
			onlyMimes : ['audio'],
			height : (($(window).height()-50)*1-20),
			rememberLastDir: true,
			uiOptions : {
				// toolbar configuration
				toolbar : [
					['back', 'forward'],
					['netmount'],
					// ['reload'],
					// ['home', 'up'],
					['mkdir', 'mkfile', 'upload'],
					['open', 'download', 'getfile'],
					['info'],
					['quicklook'],
					['copy', 'cut', 'paste'],
					['rm'],
					['duplicate', 'rename', 'edit', 'resize'],
					['extract', 'archive'],
					['search'],
					['view', 'sort'],
					['help']
				],
				// directories tree options
				tree : {
					// expand current root on init
					openRootOnLoad : true,
					// auto load current dir parents
					syncTree : true
				},
				// navbar options
				navbar : {
					minWidth : 150,
					maxWidth : 500
				},
				cwd : {
					// display parent folder with ".." name :)
					oldSchool : false
				}
			},
			contextmenu : {
				// navbarfolder menu
				navbar : ['tag', '|', 'open', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'info'],
				// current directory menu
				cwd    : ['reload', 'back', '|', 'upload', 'mkdir', 'mkfile', 'paste', '|', 'sort', '|', 'info'],
				// current directory file menu
				files  : ['tag', '|', 'getfile', '|','open', 'quicklook', '|', 'download', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'edit', 'rename', 'resize', '|', 'archive', 'extract', '|', 'info']
			}
		});
	}
});

</script>