<?php
	opcache_reset();
?>
<script>
<?php
	if($itemId != null && $itemId != "") {
		echo "viewMode = true;";
		echo "itemId = '".$itemId."';";
	}
	else {
		echo "viewMode = false;";
	 // echo "alert(JsonString);";
	}
?>

$(document).ready(function(){
	$(window).scroll(function() {
		if ($(document).scrollTop() > 200) {
		  $(".scroll-to-top").fadeIn(200);
		} else {
		  $(".scroll-to-top").fadeOut(200);
		}
	});

	bindVideoEventsAndComponents();
	
	select2ajax("event1", 
				"subject_id", 
				"title", 
				"subjects", 
				0, 
				"Event", 
				false, 
				false, 
				";");

	select2ajax("subject1", 
				"subject_id", 
				"title", 
				"subjects", 
				3, 
				"Subject", 
				true, 
				true, 
				["/", ";"]);

	select2ajax("reference", 
				"book_id", 
				"book_title", 
				"books", 
				3, 
				"Reference", 
				true, 
				true, 
				["/", ";"]);
				
	select2("typeX", 
				<?php echo getLookUps($LOOKUP_ITEMTYPE["3"]["type2"]); ?>, 
				3, 
				"Type X", 
				false, 
				true, 
				";");
	
});

function select2ajax(classNameIdentifierForElement, 
				idColumnName, 
				textColumnName, 
				tableName, 
				minimumInputLength, 
				placeholder, 
				multiple, 
				tags, 
				tokenSeparators) {
	var multi =	multiple == true ? "multiple" : "";
	$("." + classNameIdentifierForElement).select2({
		ajax: {
				url: "../php/remote/Example1Suggestions.php",
				dataType: "json",
				dalay: 250,
				data: 			function(params) {
									return {
										idColumnName: idColumnName, 
										textColumnName: textColumnName, 
										tableName: tableName, 
										searchText: params.term
									};
								}
		}, 
		minimumInputLength: minimumInputLength, 
		placeholder: placeholder, 
		multiple: multi, 
		tags: tags, 
		tokenSeparators: tokenSeparators, 
		templateSelection: formatSelection, 
		templateResult: formatResult
	});
	select2bindEvents(classNameIdentifierForElement, multiple);
	// $("." + classNameIdentifierForElement).val(null).trigger("change"); //not needed
}

function select2(classNameIdentifierForElement, 
				data, 
				minimumInputLength, 
				placeholder, 
				multiple, 
				tags, 
				tokenSeparators) {
	var multi =	multiple == true ? "multiple" : "";
	$("." + classNameIdentifierForElement).select2({
		data: data, 
		minimumInputLength: minimumInputLength, 
		placeholder: placeholder, 
		multiple: multi, 
		tags: tags, 
		tokenSeparators: tokenSeparators, 
		templateSelection: formatSelection, 
		templateResult: formatResult
	});
	select2bindEvents(classNameIdentifierForElement, multiple);
	$("." + classNameIdentifierForElement).val(null).trigger("change");
}

function select2bindEvents(classNameIdentifierForElement, multiple) {
	var shortEleName = classNameIdentifierForElement.substr(0, 3);
	$("." + classNameIdentifierForElement).on("select2:select", 	function(e) {
											var subTextEle = $("#" + this.id.replace(shortEleName + "Id", shortEleName + "Text"));
											subTextEle.val(subTextEle.val().trim() == "" ? e.params.data.text.trim() : subTextEle.val().trim() + '<?php echo $KEYWORD_TEXT_SEPERATOR; ?>' + e.params.data.text.trim());
										});
	if(multiple) {
		$("." + classNameIdentifierForElement).on("select2:unselect", 	function(e) {
												var subTextEle = $("#" + this.id.replace(shortEleName + "Id", shortEleName + "Text"));
												if(subTextEle.val().trim().search(getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim()) != -1) {
													subTextArray = subTextEle.val().split("<?php echo $KEYWORD_TEXT_SEPERATOR; ?>");
													removeIndex = subTextArray.indexOf(getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim());
													subTextArray.splice(removeIndex, 1);
													subTextArray.forEach(trimSpaces);
													subTextEle.val(subTextArray.join("<?php echo $KEYWORD_TEXT_SEPERATOR; ?>"));
												}
											});
	}
}
</script>