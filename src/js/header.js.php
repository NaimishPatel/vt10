<?php
	opcache_reset();
?>
<script>
<?php
	if($itemId != null && $itemId != "") {
		// echo "viewMode = true;";
		// echo "itemId = '".$itemId."';";
	}
	else {
		// echo "viewMode = false;";
	 // echo "alert(JsonString);";
	}
?>

function saveAccSettings() {
	var firstName = $("form[id='form_account_settings'] input[id='first_name']").val();
	var lastName = $("form[id='form_account_settings'] input[id='last_name']").val();
	var eMail = $("form[id='form_account_settings'] input[id='eMail']").val();
	var phone = $("form[id='form_account_settings'] input[id='phone']").val();
	var oldPassword = $("form[id='form_account_settings'] input[id='old_password']").val();
	var newPassword = $("form[id='form_account_settings'] input[id='new_password']").val();
	var confirmPassword = $("form[id='form_account_settings'] input[id='confirm_password']").val();
	var fontSize = $("form[id='form_account_settings'] input[name='fontSize']").val();
	var language = $("form[id='form_account_settings'] select[id='language']").val();
	var namingFormat = $("form[id='form_account_settings'] select[id='naming_format']").val();
	var dateFormat = $("form[id='form_account_settings'] select[id='date_format']").val();
	
	//UI/CSS level change
	//fontSize
	
	//Session level change
	//language, namingFormat, dateFormat
	
	//DB level change
	//firstName, lastName, eMail, phone, password
	
	var strUrl = "";
	strUrl += "language=" + language;
	strUrl += "&namingFormat=" + namingFormat;
	strUrl += "&dateFormat=" + dateFormat;
	strUrl += "&firstName=" + firstName;
	strUrl += "&lastName=" + lastName;
	strUrl += "&eMail=" + eMail;
	strUrl += "&phone=" + phone;
	strUrl += "&oldPassword=" + oldPassword;
	strUrl += "&newPassword=" + newPassword;
	
	xmlhttp = getXmlhttp();
	xmlhttp.onreadystatechange = function()	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			if(xmlhttp.responseText != "") {
				var JsonString = xmlhttp.responseText;
				var jsonData = JSON.parse(JsonString);
				alert(JsonString);
				var errors = jsonData["errors"];
				var messages = jsonData["messages"];
				displayErrorsAndMessagesOnTarget(errors, messages, targetElementId);
			} else {
				//do nothing
			}
		}
	}
	console.log(strUrl);
	xmlhttp.open("POST", "<?php echo $FILE_SAVE_ACC_SETTINGS_PHP; ?>?" + strUrl, false); //encodeURIComponent(strUrl)
	xmlhttp.send();
}
/* Unknown code : ask Kirtanbhai
$("#fontSizeSml").change(function(){
	$("#fontSizeSml").parents("label").toggleClass("active");
	$("#fontSizeReg").parents("label").removeClass("active");
	$("#fontSizeLrg").parents("label").removeClass("active");
});
$("#fontSizeReg").change(function(){
	$("#fontSizeSml").parents("label").removeClass("active");
	$("#fontSizeReg").parents("label").toggleClass("active");
	$("#fontSizeLrg").parents("label").removeClass("active");
});
$("#fontSizeLrg").change(function(){
	$("#fontSizeSml").parents("label").removeClass("active");
	$("#fontSizeReg").parents("label").removeClass("active");
	$("#fontSizeLrg").parents("label").toggleClass("active");
});
*/
 
</script>