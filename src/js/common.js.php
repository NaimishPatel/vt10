<?php
	opcache_reset();
?>
<script>

var dates = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];	
// var months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
var months = [{monthText: '', monthValue: '00'}, 
			  {monthText: 'Jan', monthValue: '01'}, 
              {monthText: 'Feb', monthValue: '02'}, 
              {monthText: 'Mar', monthValue: '03'}, 
              {monthText: 'Apr', monthValue: '04'}, 
              {monthText: 'May', monthValue: '05'}, 
              {monthText: 'Jun', monthValue: '06'}, 
              {monthText: 'Jul', monthValue: '07'}, 
              {monthText: 'Aug', monthValue: '08'}, 
              {monthText: 'Sep', monthValue: '09'}, 
              {monthText: 'Oct', monthValue: '10'}, 
              {monthText: 'Nov', monthValue: '11'}, 
              {monthText: 'Dec', monthValue: '12'}
              ];
// var years = ['1900', '1910', '1920', '1930', '1940', '1950', '1960', '1970', '1980','1981','1982','1983','1984','1985','1986','1987','1988','1989','1990','1991','1992','1993','1994','1995','1996','1997','1998','1999','2000','2001','2002','2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016'];
var years = [];
var DATE_MIN_YEAR = <?php echo $DATE_MIN_YEAR; ?>;
var DATE_MAX_YEAR = <?php echo $DATE_MAX_YEAR; ?>;
var loopIndx;
for(loopIndx = DATE_MIN_YEAR; loopIndx <= DATE_MAX_YEAR; loopIndx++) {
	years.push(loopIndx.toString());
}

//typeahead.js code starts
var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    cb(matches);
  };
};

/*
* This function returns Jan for 01
*/
function getMonthMon(monthMM) {
	var loopIndex;
	for(loopIndex = 0; loopIndex < months.length; loopIndex++) {
		if(Number(months[loopIndex].monthValue) == monthMM) {
			return months[loopIndex].monthText;
		}
	}
}

/*
* This function returns 01 for Jan
*/
function getMonthMM(monthMon) {
	var loopIndex;
	for(loopIndex = 0; loopIndex < months.length; loopIndex++) {
		if(months[loopIndex].monthText.toLowerCase() == monthMon.toLowerCase()) {
			return months[loopIndex].monthValue;
		}
	}
}

/*
* This function will return an array [dd, mm, yyyy]
*/
function splitDate(date) {
	var loopIndx;
	var splittedDate = [];
	for(loopIndx = 0; loopIndx < date.length; loopIndx++) {
		switch(date[loopIndx].length) {
			case 2:
				splittedDate[0] = date[loopIndx];
				break;
			case 3:
				splittedDate[1] = getMonthMM(date[loopIndx]);
				break;
			case 4:
				splittedDate[2] = date[loopIndx];
				break;
		}
	}
	return splittedDate;
}

/*
* This function will globally replace the tokens in the htmlStr with its specified values
*/
function substituteTokens(htmlStr, tokens) {
	var loopIndx;
	for(loopIndx = 0; loopIndx < tokens.length; loopIndx++) {
		htmlStr = htmlStr.replace(new RegExp(tokens[loopIndx].tokenStr,"g"), tokens[loopIndx].tokenValue);
	}
	return htmlStr;
}

/*
* This function will return Salangpur for Asia > India > Gujarat > Ahmadabad > Barwala > Salangpur
*/
function getLeafNodeTextFromTreeText(treeText, treeSeparator) {
	var leafNodeText;
	if(treeText != undefined) {
		if(treeText.search(treeSeparator) != -1) {
			leafNodeText = treeText.substr(treeText.lastIndexOf(treeSeparator) + treeSeparator.length, treeText.length - 1);
		} else {
			leafNodeText = treeText;
		}
	} else {
		leafNodeText = "";
	}
	return leafNodeText;
}

function get(str) {
	if(str == undefined || str == null) return "";
	else return str;
}

//satya? : purpose?
function getXmlhttp() {
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
	  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return 	xmlhttp;
}

function isDate(txtDate) {
  var currVal = txtDate;
  if(currVal == '')
    return false;
  
  //Declare Regex  
  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
  var dtArray = currVal.match(rxDatePattern); // is format OK?

  if (dtArray == null)
     return false;
 
  //Checks for mm/dd/yyyy format.
  dtMonth = dtArray[1];
  dtDay= dtArray[3];
  dtYear = dtArray[5];

  if (dtMonth < 1 || dtMonth > 12)
      return false;
  else if (dtDay < 1 || dtDay> 31)
      return false;
  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
      return false;
  else if (dtMonth == 2)
  {
     var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
     if (dtDay> 29 || (dtDay ==29 && !isleap))
          return false;
  }
  return true;
}

/*
* This function will remove whitespace from both ends of a string
*/
function trimSpaces(item, index) {
	item.trim();
}

function displayErrorsAndMessages(errors, messages, nn, pageLegend) {
	var sentToServer = true;
	
	//errors
	var errorsHtml = "<ul>";  /* class='errors' */
	var loopIndx;
	for(loopIndx = 0; loopIndx < errors.length; loopIndx++) {
		errorsHtml += "<li><span class='fa fa-exclamation-circle'></span> <?php echo $LABEL_ERROR_CODE; ?>[" + errors[loopIndx]["code"] + "]: " + errors[loopIndx]["text"] + "</li>";
	}
	errorsHtml += "</ul>";
	$("#" + pageLegend + "_item" + nn + "_errors_div").hide();
	$("#" + pageLegend + "_item" + nn + "_errors_div").html(errorsHtml);
	if(errorsHtml != "<ul></ul>") {
		$("#" + pageLegend + "_item" + nn + "_errors_div").show();
		sentToServer = false;
	}
	//messages
	var messagesHtml = "<ul>";  /* class='messages' */
	var loopIndx;
	for(loopIndx = 0; loopIndx < messages.length; loopIndx++) {
		messagesHtml += "<li><span class='glyphicon glyphicon-thumbs-up'></span> <?php echo $LABEL_MESSAGE_CODE; ?>[" + messages[loopIndx]["code"] + "]: " + messages[loopIndx]["text"] + "</li>";
	}
	messagesHtml += "</ul>";
	$("#" + pageLegend + "_item" + nn + "_messages_div").hide();
	$("#" + pageLegend + "_item" + nn + "_messages_div").html(messagesHtml);
	if(messagesHtml != "<ul></ul>") {
		$("#" + pageLegend + "_item" + nn + "_messages_div").show();
	}
	
	return sentToServer;
}

function displayErrorsAndMessagesOnTarget(errors, messages, targetElementId) {

}

var isDayValid = function(day){
	if(!$.isNumeric(day) || ($.isNumeric(day) && day > 31)){
		return false;
	}
	return true;
};
var isMonthValid = function(month,flag){
	if(flag == 'MM' && $.isNumeric(month) && month > 12){
		return false;
	}
	if(flag == 'MMM' && getMonthMM(month) == undefined){
		return false;
	}
	return true;
};
var isYearValid = function(year){
	if(!$.isNumeric(year) || ($.isNumeric(year) && (year < 1900 || year > 2050))){
		return false;
	}
	return true;
};
var dateNumericValidation = function(date,monthType){
	debugger;
	if(date.length > 2){
		if(!isDayValid(date[0]) || !isMonthValid(date[1],monthType) || !isYearValid(date[2])){
			return false;
		}
	} else if(date.length > 1){
		if(!isMonthValid(date[0],monthType) || !isYearValid(date[1])){
			return false;
		}
	}else if(date.length > 0){
		if(!isYearValid(date[0])){
			return false;
		}
	}
	return true;
}

var isMonthAlfa = function(date){
	if(date.length > 2 && date[1].length >= 3){
			return false;
	} else if(date.length > 1 && date[0].length >= 3){
			return false;
	}
	return true;
}

var replaceDate = function(date){
	var splittedDate = date;
	if(date.length == 3 && $.isNumeric(date[1])){
		splittedDate[0] = date[0];
		splittedDate[1] = getMonthMon(date[1]) == undefined  ? 'ddd' : getMonthMon(date[1]);
		splittedDate[2] = date[2];
	} else if(date.length == 2 && $.isNumeric(date[0])){
		splittedDate[0] = getMonthMon(date[0]) == undefined ? 'ddd' : getMonthMon(date[0]);
		splittedDate[1] = date[1];
	}else if(date.length == 1){
		splittedDate[0] = date[0];
	}
	return splittedDate;
}

var showValiationMessageOnField = function(currElement,msg){
	$(currElement).popover({
        html : true, 
        content: '<span style="color:red">'+msg+'</span>',
        placement : 'auto'
    }).popover('show');
    
	setTimeout(function(){
		$(currElement).popover('hide');
 	},2000);	
};

var camelize = function (str) {
	return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

$("#video-content").on("blur",".form_datetime", function(){

	debugger;
	var currElement = this;
	var eventDate = $(currElement).val();
	var eventDateArr = eventDate.split(" ");
	var eventDateArr = replaceDate(eventDateArr);
	$(currElement).val(eventDateArr.join(" "));
	var eventDateDdMmYyyy = splitDate(eventDateArr);

	if(!dateNumericValidation(eventDateArr,'MM')){
		if($(currElement).val() != ""){
			$(currElement).focus();
			showValiationMessageOnField(currElement,"Please enter date on dd-MMM-yyyy or MMM-yyyy or yyyy format");
		} 
		$(currElement).val("");
		return false;
	}
	if(!isMonthAlfa(eventDateArr) && !dateNumericValidation(eventDateArr,'MMM')){
		if($(currElement).val() != ""){
			$(currElement).focus();
			showValiationMessageOnField(currElement,"Please enter valid Date, date should be dd-MMM-yyyy or MMM-yyyy or yyyy");
		}
		$(currElement).val("");
		return false;
	}

	var eventDateYYYYValue = eventDateDdMmYyyy[2] == undefined ? "0000" : eventDateDdMmYyyy[2];
	var eventDateMMValue = eventDateDdMmYyyy[1] == undefined ? "00" : eventDateDdMmYyyy[1];
	var eventDateDDValue = eventDateDdMmYyyy[0] == undefined ? "00" : eventDateDdMmYyyy[0];

	var endDate = eventDateMMValue + '/' + eventDateDDValue + '/' + eventDateYYYYValue;
	if(eventDateMMValue != "00"
		&& eventDateDDValue != "00"
		&& eventDateYYYYValue != "0000") {
		if(!isDate(endDate)) {
			if($(currElement).val() != ""){
				$(currElement).focus();
				showValiationMessageOnField(currElement,"Please enter valid Date, date should be dd-MMM-yyyy or MMM-yyyy or yyyy");
			}
			$(currElement).val("");
			return false;	
		}
	}

	if($(currElement).val() != "" && !isMonthAlfa(eventDateArr)){
		$(currElement).val(camelize($(currElement).val()));
	}
	

	
	console.log(eventDate);
});

$("#video-content").on("click", ".datetime_picker_icon" ,function(){
	console.log($(this).attr('data-target'));
	var dateElement = $(this).attr('data-target');
	$("#"+dateElement).datetimepicker('show');
		
});


function populateItemToView(jsonData) {
	var pageLegend = "edit";
	var jsItemUItoDBMappingArrayKeys = ['<?php echo implode("', '", array_keys($itemUItoDBMappingArray)); ?>'];

	debugger;
	var pLoopIndx;
	for(pLoopIndx = 0; pLoopIndx < jsonData.length; pLoopIndx++) {
		var nn = pLoopIndx.toString().length == 1 ? "0" + pLoopIndx.toString() : pLoopIndx.toString();
		
		if(pLoopIndx != 0) addClipInfo(false); //Add Clip Info Html && do not send to server to save
		
		var itemDetails = jsonData[pLoopIndx];
		var iLoopIndx;
		for(iLoopIndx = 0; iLoopIndx < jsItemUItoDBMappingArrayKeys.length; iLoopIndx++) {
			var element = $("#edit_item" + nn + "_" + jsItemUItoDBMappingArrayKeys[iLoopIndx]);
			if($(element).prop("tagName") == "INPUT" || $(element).prop("tagName") == "TEXTAREA") {
				console.log(jsItemUItoDBMappingArrayKeys[iLoopIndx]);

				element.val(get(itemDetails[jsItemUItoDBMappingArrayKeys[iLoopIndx]]));
			} else if($(element).prop("tagName") == "SELECT") {
				$(element).val(get(itemDetails[jsItemUItoDBMappingArrayKeys[iLoopIndx]])).trigger("change");
			}
		}

		var filePathArray = jsonData[pLoopIndx]["urlOrFileFolderPath"] == undefined ? [] : jsonData[pLoopIndx]["urlOrFileFolderPath"].split('<?php echo $FILE_PATH_SEPERATOR; ?>');
		var iLoopIndx;
		for(iLoopIndx = 0; iLoopIndx < filePathArray.length; iLoopIndx++) {
			var mm = (iLoopIndx + 1).toString().length == 1 ? "0" + (iLoopIndx + 1).toString() : (iLoopIndx + 1).toString();
			
			var urlOrFileFolderPath = get(filePathArray[iLoopIndx]);

			var tokens = [
				{"tokenStr": "~~mm~~", "tokenValue": mm},
				{"tokenStr": "~~item_urlOrFileFolderPath~~", "tokenValue": urlOrFileFolderPath}
			];
			$("#edit_item00_urlOrFileFolderPathDisplayBox").append(substituteTokens('<?php echo $DISMISSIBLE_DISPLAY_URL_OR_FILE_fOLDER_PATH_TOKEN_HTML; ?>', tokens));
		}
		
		var eventDateArray = jsonData[pLoopIndx]["eventDateArray"] == undefined ? [] : jsonData[pLoopIndx]["eventDateArray"];
		bindDateOnElement(nn,eventDateArray,['eventStartDateDD','eventStartDateMM','eventStartDateYYYY'],"_eventStartDateTime");
		bindDateOnElement(nn,eventDateArray,['eventEndDateDD','eventEndDateMM','eventEndDateYYYY'],"_eventEndDateTime");
		
		var keywordLineArray = jsonData[pLoopIndx]["keywordLineArray"] == undefined ? [] : jsonData[pLoopIndx]["keywordLineArray"];

		values = "";
		$.each(keywordLineArray, function(i, obj) {
			if(obj.noteText){
				values += obj.noteText;
			}
		});
		$("#edit_item" + nn + "_keywordId_keyword").val(values);

		var tagsArray = jsonData[pLoopIndx]["tagsArray"] == undefined ? [] : jsonData[pLoopIndx]["tagsArray"];
		bindSelect2Values(nn,tagsArray,"tagId","tagText","_tags");
		
		var subjectLineArray = jsonData[pLoopIndx]["subjectArray"] == undefined ? [] : jsonData[pLoopIndx]["subjectArray"];
		bindSelect2Values(nn,subjectLineArray,"subjectId","subjectText","_subId_subject");

		var eventLineArray = jsonData[pLoopIndx]["eventArray"] == undefined ? [] : jsonData[pLoopIndx]["eventArray"];
		bindSelect2Values(nn,eventLineArray,"eventId","eventText","_evtId_event");

		var languageLineArray = jsonData[pLoopIndx]["languageArray"] == undefined ? [] : jsonData[pLoopIndx]["languageArray"];
		bindSelect2Values(nn,languageLineArray,"languageId","languageText","_langId_language",true);

		var locationArray = jsonData[pLoopIndx]["locationArray"] == undefined ? [] : jsonData[pLoopIndx]["locationArray"];
		bindSelect2Values(nn,locationArray,"locationId","locationTree","_locId_location");

		var vaktaArray = jsonData[pLoopIndx]["vaktaArray"] == undefined ? [] : jsonData[pLoopIndx]["vaktaArray"];
		bindSelect2Values(nn,vaktaArray,"vaktaId","vaktaName","_vakta");

		var additionalDetails = jsonData[pLoopIndx]["additionalDetails"] == undefined ? [] : jsonData[pLoopIndx]["additionalDetails"];
		$("#edit_item" + nn + "_additionalDescp").val(additionalDetails.additionalDescp);

		var categotyLineArray = jsonData[pLoopIndx]["categotyArray"] == undefined ? [] : jsonData[pLoopIndx]["categotyArray"];
		bindSelect2Values(nn,categotyLineArray,"categoryId","categoryTitle","_itemCategory");
	}
}

function bindSelect2Values(nn,jsonArray,id,text,element,setOnlyValue){
	var opetions = '';
	var values = [];
	$.each(jsonArray, function(i, obj) {
		opetions += '<option value="';
		opetions += obj[id] + '">';
		opetions += obj[text];
		opetions += '</option>';

		values.push(obj[id]);
	});	
	if(setOnlyValue){
		$("#edit_item" + nn + element).val(values).trigger('change');
	}else{
		$("#edit_item" + nn + element).append(opetions).val(values).trigger('change');
	}
}
function bindDateOnElement(nn,eventDateArray,dbFieldArr,element){
	var iLoopIndx;
	for(iLoopIndx = 0; iLoopIndx < eventDateArray.length; iLoopIndx++) {
		var mm = (iLoopIndx + 1).toString().length == 1 ? "0" + (iLoopIndx + 1).toString() : (iLoopIndx + 1).toString();
		
		var eventStartDateYYYYValue = get(eventDateArray[iLoopIndx][dbFieldArr[2]]);
		var eventStartDateMMValue = get(eventDateArray[iLoopIndx][dbFieldArr[1]]);
		var eventStartDateMMValueTxt = getMonthMon(get(eventDateArray[iLoopIndx][dbFieldArr[1]]));
		var eventStartDateDDValue = get(eventDateArray[iLoopIndx][dbFieldArr[0]]);
		
		var val = "";
		if(eventStartDateDDValue){
			val += eventStartDateDDValue + " ";
		}
		if(eventStartDateMMValueTxt){
			val += eventStartDateMMValueTxt + " ";
		}
		if(eventStartDateYYYYValue){
			val += eventStartDateYYYYValue;
		}
		$("#edit_item" + nn + element).val(val);
	}
};

function getSelector(nextClipElement,id,className){
	return nextClipElement != undefined ? nextClipElement+id : className;
}
function bindVideoEventsAndComponents(forClip,nextClipElement) {
	bindEventsAndComponents(nextClipElement);
	$(".tags").select2({
		placeholder: "Events  Vakta Location Subject",
		multiple: "multiple",
		tags: true,
		tokenSeparators: [';'],
		ajax: {
				url: "<?php echo $FILE_KEYWORD_SUGGESTIONS_PHP; ?>",
				dataType: "json",
				dalay: 250,
				data: 			function(params) {
									return {
										searchText: params.term,
										triggererElementId: this[0].id
									};
								}
		},
		templateSelection: formatSelection,
		templateResult: formatResult,
		minimumInputLength: 3
	});
	
	$(".event").select2({
		placeholder: "<?php echo $LABEL_EVENT_PLACEHOLDER; ?>",
		tags: true,
		ajax: {
				url: "<?php echo $FILE_KEYWORD_SUGGESTIONS_PHP; ?>",
				dataType: "json",
				dalay: 250,
				data: 			function(params) {
									return {
										searchText: params.term,
										triggererElementId: this[0].id
									};
								}
		},
		templateSelection: formatSelection,
		templateResult: formatResult,
		minimumInputLength: 3,
		multiple: true
	});
	
	$(".keyword").textcomplete([{ 
				id: 'keyword',
				match: /(^|\S*)([^\u0000-\u007f]{2,}|\w{2,})$/,    ///\b(\w{2,})$/
				search: function (term, callback) {
					if($("#wordSuggestions").prop("checked")){
						debugger;
						$.get("<?php echo $FILE_KEYWORD_SUGGESTIONS_PHP; ?>"+"?triggererElementId=test_keyword&searchText="+encodeURIComponent(term), function( resp ) {
							  	var words = [];
							  	try {
								  var json = JSON.parse(resp);
								  $.each(json.results, function(i, item) {
										if(item.text){
									  		words.push(item.text);
										}
								   });
								} catch (e) {
									words = [];
								}
								callback($.map(words, function (word) {
									return word.indexOf(term) === 0 ? word : null;
								}));
						});
					}else{
						var words = [];
						callback($.map(words, function (word) {
							return "";
						}));
					}
				},
				replace: function (word) {
					return word + ' ';
				}
			}
		], {
			onKeydown: function (e, commands) {
				if (e.ctrlKey && e.keyCode === 74) { // CTRL-J
					return commands.KEY_ENTER;
				}
			}
		});                 
	
	$(".location").select2({
		placeholder: "<?php echo $LABEL_LOCATION_PLACEHOLDER; ?>",
		multiple: "multiple",
		tags: true,
		tokenSeparators: [';'],
		ajax: {
				url: "<?php echo $FILE_LOCATION_SUGGESTIONS_PHP; ?>",
				dataType: "json",
				dalay: 250,
				data: 			function(params) {
									return {
										searchText: params.term,
										parentId: $(this[0]).val() != null ? $(this[0]).val()[$(this[0]).val().length - 1] : "",
										triggererElementId: this[0].id,
										favourite: $("#" + this[0].id.substr(0, 4) + "_search_favourite").prop("checked")
									};
								}
		},
		templateSelection: formatSelection,
		templateResult: formatResult,
		minimumInputLength: 3,
		multiple: true
	});

	var element = getSelector(nextClipElement,"_vakta" ,'.vakta');
	$(element).select2({
		width:'100%' ,
		placeholder: "Vakta",
		tags: true,
		ajax: {
			url: "<?php echo $FILE_KEYWORD_SUGGESTIONS_PHP; ?>",
			dataType: "json",
			dalay: 250,
			data: 	 function(params) {
							return {
								searchText: params.term,
								triggererElementId: this[0].id
							};
						}
			},
			templateSelection: formatSelection,
			templateResult: formatResult,
			minimumInputLength: 3,
			multiple: true
	});
	$(element).val(null).trigger("change");

	var element = getSelector(nextClipElement,"_itemCategory" ,'.itemCategory');
	$(element).select2({
		width:'100%' ,
		placeholder: "<?php  echo $LABEL_CATEGORY_PLACEHOLDER; ?>",
		tags: true,
		ajax: {
			url: "<?php echo $FILE_KEYWORD_SUGGESTIONS_PHP; ?>",
			dataType: "json",
			dalay: 250,
			data: function(params) {
						return {
							searchText: params.term,
							triggererElementId: this[0].id
						};
					}
			},
			templateSelection: formatSelection,
			templateResult: formatResult,
			minimumInputLength: 3,
			multiple: true
	});
	$(element).val(null).trigger("change");
	
	// Append options to select dropdowns
	if(forClip) {

	} else {
	}

	var element = getSelector(nextClipElement,"_technicalQuality" ,'.technicalQuality');
	$(element).select2({
		placeholder: "<?php echo $LABEL_TECHNICAL_QUALITY_PLACEHOLDER; ?>",
		data: <?php echo $ITEM_QUALITY_LOOKUPS; ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 0,
		allowClear: true
	});
	$(element).val(null).trigger("change");
	$(element).on("select2:unselect", 	function(e) {
		$(this).val(null).trigger("change");
	});

	var element = getSelector(nextClipElement,"_audioQuality" ,'.audioQuality');
	$(element).select2({
		placeholder: "<?php echo $LABEL_AUDIO_QUALITY_PLACEHOLDER; ?>",
		data: <?php echo $ITEM_QUALITY_LOOKUPS; ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 0,
		allowClear: true
	});
	$(element).val(null).trigger("change");
	$(element).on("select2:unselect", 	function(e) {
		$(this).val(null).trigger("change");
	});
	
	var element = getSelector(nextClipElement,"_confidentiality" ,'.confidentiality');
	$(element).select2({
		placeholder: "<?php echo $LABEL_CONFIDENTIALITY_PLACEHOLDER; ?>",
		data: <?php echo $ITEM_CONFIDENTIALITY_LOOKUPS; ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 0,
		allowClear: true
	});
	$(element).val(null).trigger("change");
	$(element).on("select2:unselect", 	function(e) {
		debugger;
		$(this).val(null).trigger("change");
	});

	var element = getSelector(nextClipElement,"_accessibility" ,'.accessibility');
	$(element).select2({
		placeholder: "<?php echo $LABEL_ACCESSIBILITY_PLACEHOLDER; ?>",
		data: <?php echo $ITEM_ACCESSIBILITY_LOOKUPS; ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 0,
		allowClear: true
	});
	$(element).val(null).trigger("change");
	$(element).on("select2:unselect", 	function(e) {
		$(this).val(null).trigger("change");
	});
	
	var element = getSelector(nextClipElement,"_langId_language" ,'.language');
	$(element).select2({
		placeholder: "<?php echo $LABEL_LANGUAGE_PLACEHOLDER; ?>",
		data: <?php echo $ITEM_LANGUAGE_LOOKUPS; ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 0,
		multiple: true
		
	});
	$(element).val(null).trigger("change");
	//Autofocus _clipStartTime element of the recently added clip
	$("[id$=_clipStartTime]").last().focus();
}

function bindEventsAndComponents(nextClipElement) {
	$('.form_datetime').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 4,
		forceParse: 0,
        showMeridian: 1,
		viewSelect: 4,
		minView: 2
    });

	$(getSelector(nextClipElement,"_tags" , ".tags")).on("select2:select", 	function(e) {
											var subTextEle = $("#" + this.id.replace("tagId", "tagText"));
											subTextEle.val(subTextEle.val().trim() == "" ? getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim() : subTextEle.val().trim() + '<?php echo $KEYWORD_TEXT_SEPERATOR; ?>' + getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim());
										});
	$(".tags").on("select2:unselect", 	function(e) {
												var subTextEle = $("#" + this.id.replace("tagId", "tagText"));
												if(subTextEle.val().trim().search(getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim()) != -1) {
													subTextArray = subTextEle.val().split("<?php echo $KEYWORD_TEXT_SEPERATOR; ?>");
													removeIndex = subTextArray.indexOf(getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim());
													subTextArray.splice(removeIndex, 1);
													subTextArray.forEach(trimSpaces);
													subTextEle.val(subTextArray.join("<?php echo $KEYWORD_TEXT_SEPERATOR; ?>"));
												}
											});
	
	$(".event").on("select2:select", 	function(e) {
											var subTextEle = $("#" + this.id.replace("evtId", "evtText"));
											subTextEle.val(getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim());
										});
	// $(".event").on("select2:unselect", 	function(e) { // not required });

	$(".language").on("select2:select", 	function(e) {
											var subTextEle = $("#" + this.id.replace("langId", "langText"));
											subTextEle.val(getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim());
										});
	// $(".language").on("select2:unselect", 	function(e) { // not required });
	
	
	$(".location").on("select2:select", 	function(e) {
											var subTextEle = $("#" + this.id.replace("locId", "locText"));
											subTextEle.val(subTextEle.val().trim() == "" ? e.params.data.text.trim() : subTextEle.val().trim() + '<?php echo $KEYWORD_TEXT_SEPERATOR; ?>' + e.params.data.text.trim());
										});
	$(".location").on("select2:unselect", 	function(e) {
												var subTextEle = $("#" + this.id.replace("locId", "locText"));
												if(subTextEle.val().trim().search(getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim()) != -1) {
													subTextArray = subTextEle.val().split("<?php echo $KEYWORD_TEXT_SEPERATOR; ?>");
													removeIndex = subTextArray.indexOf(getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim());
													subTextArray.splice(removeIndex, 1);
													subTextArray.forEach(trimSpaces);
													subTextEle.val(subTextArray.join("<?php echo $KEYWORD_TEXT_SEPERATOR; ?>"));
												}
											});
	
	$(".book").on("select2:select", 	function(e) {
											var subTextEle = $("#" + this.id.replace("bukId", "bukText"));
											subTextEle.val(subTextEle.val().trim() == "" ? e.params.data.text.trim() : subTextEle.val().trim() + '<?php echo $KEYWORD_TEXT_SEPERATOR; ?>' + e.params.data.text.trim());
										});
	$(".book").on("select2:unselect", 	function(e) {
												var subTextEle = $("#" + this.id.replace("bukId", "bukText"));
												if(subTextEle.val().trim().search(getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim()) != -1) {
													subTextArray = subTextEle.val().split("<?php echo $KEYWORD_TEXT_SEPERATOR; ?>");
													removeIndex = subTextArray.indexOf(getLeafNodeTextFromTreeText(e.params.data.text, '<?php echo $TREE_SEPERATOR; ?>').trim());
													subTextArray.splice(removeIndex, 1);
													subTextArray.forEach(trimSpaces);
													subTextEle.val(subTextArray.join("<?php echo $KEYWORD_TEXT_SEPERATOR; ?>"));
												}
											});

	
	/*var element = getSelector(nextClipElement,"_itemCategory1" ,'.itemCategory1'); 
	$(element).select2({
		placeholder: "<?php //echo $LABEL_CATEGORY1_PLACEHOLDER; ?>",
		data: <?php //echo getLookUps($LOOKUP_ITEMTYPE[$_SESSION["itemType"].""]["category1"]); ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 0,
		allowClear: true
	});
	$(element).val(null).trigger("change");*/

	/*var element = getSelector(nextClipElement,"_itemCategory2" ,'.itemCategory2');
	$(element).select2({
		placeholder: "<?php //echo $LABEL_CATEGORY2_PLACEHOLDER; ?>",
		data: <?php //echo getLookUps($LOOKUP_ITEMTYPE[$_SESSION["itemType"].""]["category2"]); ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 0,
		allowClear: true
	});
	$(element).val(null).trigger("change");*/

	var element = getSelector(nextClipElement,"_itemType2" ,'.itemType2');
	$(element).select2({
		placeholder: "<?php echo $LABEL_TYPE_PLACEHOLDER; ?>",
		data: <?php echo getLookUps($LOOKUP_ITEMTYPE[$_SESSION["itemType"].""]["type2"]); ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 0
	});
	$(element).val(null).trigger("change");
	
	$('.no-border-input').focus(function(){
		$(this).parent().addClass('form-control-focus');
	});
	$('.no-border-input').blur(function(){
		$(this).parent().removeClass('form-control-focus');
	});

	$(".date-display-box").hover(
		function(){
			$(this).addClass("active");
		},
		function(){
			$(this).removeClass("active");
	});
	$(".keyword-display-box").hover(
		function(){
			$(this).addClass("active");
		},
		function(){
			$(this).removeClass("active");
	});
	$(".subject-display-box").hover(
		function(){
			$(this).addClass("active");
		},
		function(){
			$(this).removeClass("active");
	});
	$(".event-display-box").hover(
		function(){
			$(this).addClass("active");
		},
		function(){
			$(this).removeClass("active");
	});
	$(".location-display-box").hover(
		function(){
			$(this).addClass("active");
		},
		function(){
			$(this).removeClass("active");
	});
	$(".urlOrFileFolderPath-display-box").hover(
		function(){
			$(this).addClass("active");
		},
		function(){
			$(this).removeClass("active");
	});
	
	$('[data-toggle="tooltip"]').tooltip({
		trigger: "hover"
	});
	
	$('.input-group-addon').keydown(function(event) {
		var keyCode = event.keyCode || event.which;
		if(keyCode == 13) { //Enter
			event.preventDefault();
			switch(this.id.substr("XXXX_itemNN_".length)) {
				case "addDates": 			addDates(this.id);
											break;
				case "addUrlOrFileFolderPath": addUrlOrFileFolderPath($('#edit_item00_urlOrFileFolderPathText').val());
											break;
				case "addLanguageLine": 	addLanguageLine(this.id);
											break;
				case "addLocationLine": 	addLocationLine(this.id);
											break;
				case "addEventLine":	 	addEventLine(this.id);
											break;
				case "addKeywordLine": 		addKeywordLine(this.id);
											break;
				case "addSubjectLine": 		addSubjectLine(this.id);
											break;
				case "addBookLine": 		addBookLine(this.id);
											break;
			}
		}
	});
	$('[id*=_save]').keydown(function(event) {
		var keyCode = event.keyCode || event.which;
		if(keyCode == 13) { //Enter
			event.preventDefault();
			sendToServer(this.id, 0);
		}
	});
		
	$(".delete").click(function() {
		$("#del_item_number").val($(this).data('item-number'));
		$("#confirmDeleteModal").modal();
	});

	function hmsToSecondsOnly(str) {
	    var p = str.split(':'),
	        s = 0, m = 1;

	    while (p.length > 0) {
	        s += m * parseInt(p.pop(), 10);
	        m *= 60;
	    }
	    return s;
	}
	$(".play").click(function() {
		debugger;
		var path = $("#edit_item00_filePath01").val();
		var nn = $(this).data('item-number');
		var elm = "#edit_item"+nn+"_clipStartTime";
		var startTime = hmsToSecondsOnly($(elm).val());

		//var temp = encodeURIComponent("file:///E:\\baps\\vidio\\GuruhariDarshan 20 Feb 2016, Sarangpur, India.mp4");
		var temp = encodeURIComponent("file:///"+path);
		$.get("vlcClipPlay.php?command=empty",function(responseText){
			$.get("vlcClipPlay.php?command=play&fileName="+temp,function(responseText){
				$.get("vlcClipPlay.php?command=seek&startTime="+startTime,function(responseText){

				});
			});
		});
		/* debugger;
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			    if (this.readyState == 4 && this.status == 200) {
					
			    }
		};
		xmlhttp.open("GET", "vlcClipPlay.php?command=play&fileName="+temp, true);
		xmlhttp.send();  */
	});
				
	//Tooltip
	var inputFormControlElems = $("input.form-control");					//input fields
	var textareaFormControlElems = $("textarea.form-control");				//textarea fields
	var spanFormControlElems = $("span.form-control");						//select2
	var anchorElems = $("a[data-toggle='tooltip']");												//anchors
	var loopIndx;
	for(loopIndx = 0; loopIndx < inputFormControlElems.length; loopIndx++) {
		var placeholder;
		if(placeholder = $(inputFormControlElems[loopIndx]).attr("placeholder")) {
			$(inputFormControlElems[loopIndx]).tooltip({
				title: placeholder,
				placement: "top"
			});
		}
	}
	var loopIndx;
	for(loopIndx = 0; loopIndx < textareaFormControlElems.length; loopIndx++) {
		var placeholder;
		if(placeholder = $(textareaFormControlElems[loopIndx]).attr("placeholder")) {
			$(textareaFormControlElems[loopIndx]).tooltip({
				title: placeholder,
				placement: "top"
			});
		}
	}
	var loopIndx;
	for(loopIndx = 0; loopIndx < spanFormControlElems.length; loopIndx++) {
		var placeholder;
		//select2-simple: Event
		if(placeholder = $(spanFormControlElems[loopIndx]).find("span.select2-selection__placeholder").text()) {
			$(spanFormControlElems[loopIndx]).tooltip({
				title: placeholder,
				placement: "top"
			});
		//select2-multiple: Location, Keyword, Subject
		} else if(placeholder = $(spanFormControlElems[loopIndx]).find("input.select2-search__field").attr("placeholder")) {
			$(spanFormControlElems[loopIndx]).tooltip({
				title: placeholder,
				placement: "top"
			});
		}
	}
	//TODO: this is currently not working when we tab through these elements
	var loopIndx;
	for(loopIndx = 0; loopIndx < anchorElems.length; loopIndx++) {
		var placeholder;
		if(placeholder = $(anchorElems[loopIndx]).attr("data-original-title")) {
			$(anchorElems[loopIndx]).tooltip({
				title: placeholder,
				placement: "top"
			});
		}
	}
}

function formatSelection(x) {
	return getLeafNodeTextFromTreeText(x.text, '<?php echo $TREE_SEPERATOR; ?>');
}

function formatResult(x) {
	if(x.text != undefined) {
		var searchText = document.activeElement.value;
		if(searchText != undefined && searchText != "") {
			var returnTxt = x.text.replace(new RegExp(searchText, 'g'), "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_START;?>" + searchText + "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_END;?>");
			returnTxt = returnTxt.replace(new RegExp(searchText.toLowerCase(), 'g'), "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_START;?>" + searchText.toLowerCase() + "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_END;?>");
			returnTxt = returnTxt.replace(new RegExp(searchText.toUpperCase(), 'g'), "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_START;?>" + searchText.toUpperCase() + "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_END;?>");
			returnTxt = returnTxt.replace(new RegExp(abcToAbc(searchText), 'g'), "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_START;?>" + abcToAbc(searchText) + "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_END;?>");
			returnTxt = returnTxt.replace(new RegExp(abcToAbc(searchText, 'ABCToAbc'), 'g'), "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_START;?>" + abcToAbc(searchText, 'ABCToAbc') + "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_END;?>");
			return $("<span>" + returnTxt + "</span>");
		}
		return x.text;
	}
}

function formatCategorySelection(x) {
	return x.text.trim();
}

function formatCategoryResult(x) {
	if(x.text != undefined) {
		var searchText = document.activeElement.value;
		var pad = 0;
		var periodMatches = x.text.match(/\./g);
		if(periodMatches != null) {
			pad = periodMatches.length * 20;
		}
		if(searchText != undefined && searchText != "") {
			var returnTxt = x.text.replace(new RegExp(searchText, 'g'), "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_START;?>" + searchText + "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_END;?>");
			returnTxt = returnTxt.replace(new RegExp(searchText.toLowerCase(), 'g'), "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_START;?>" + searchText.toLowerCase() + "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_END;?>");
			returnTxt = returnTxt.replace(new RegExp(searchText.toUpperCase(), 'g'), "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_START;?>" + searchText.toUpperCase() + "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_END;?>");
			returnTxt = returnTxt.replace(new RegExp(abcToAbc(searchText), 'g'), "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_START;?>" + abcToAbc(searchText) + "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_END;?>");
			returnTxt = returnTxt.replace(new RegExp(abcToAbc(searchText, 'ABCToAbc'), 'g'), "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_START;?>" + abcToAbc(searchText, 'ABCToAbc') + "<?php echo $AUTOSUGGEST_LIST_FORMATTING_TAG_END;?>");
			return $("<span style='padding-left: " + pad + "px;'>" + returnTxt + "</span>");
		} else if(searchText == "") {
			return $("<span style='padding-left: " + pad + "px;'>" + x.text + "</span>");
		}
		return x.text;
	}
}

function getParentLocIds(htmlItemId) {
	var parentLocIds = [];
	var nn = htmlItemId.substr('XXXX_item'.length, 2);
	var pageLegend = htmlItemId.substr(0, 4);
	var parentLocElems = $("[id*=" + pageLegend + "_item" + nn + "_locationId]");
	var loopIndx;
	for(loopIndx = 0; loopIndx < parentLocElems.length; loopIndx++) {
		parentLocIds.push(parentLocElems[loopIndx].value);
	}
	return parentLocIds.join(",");
}

function abcToAbc(str, ABCToAbc) {
	var splitStr = str.split("");
	var returnStr;
	/*if(65 <= str.charCodeAt(0) <= 90) {
		returnStr = splitStr[0].toLowerCase();
	}*/
	if(ABCToAbc) {
		returnStr = splitStr[0];
	} else if(97 <= str.charCodeAt(0) <= 122) {
		returnStr = splitStr[0].toUpperCase();
	} else {
		returnStr = splitStr[0];
	}
	splitStr.reverse().pop();
	splitStr.reverse();
	returnStr += ABCToAbc ? splitStr.join("").toLowerCase() : splitStr.join("");
	return returnStr;
}


function addBookLine(htmlItemId) {
	var nn = htmlItemId.substr('XXXX_item'.length, 2);
	var pageLegend = htmlItemId.substr(0, 4);
	var subjectElements = $("[id*= " + pageLegend + "_item" + nn + "_bookLine]");
	var prev_mm = '00';
	if(subjectElements.length > 0) {
		prev_mm = subjectElements[subjectElements.length - 1].id.substr((pageLegend + '_itemNN_bookLine').length, 2);
	}
	var mm = Number(prev_mm) + 1; //mm will always start with 01
	
	var prev_subjectLine = 0;
	if(subjectElements.length > 0) {
		prev_subjectLine = Number(subjectElements[subjectElements.length - 1].value);
	}
	var subjectLine = prev_subjectLine + 1; //subjectLine will always start with 1
	
	var subjectId = $("#" + pageLegend + "_item" + nn + "_bukId_book").val();
	var subjectText = $("#" + pageLegend + "_item" + nn + "_bukText_book").val().split('<?php echo $KEYWORD_TEXT_SEPERATOR; ?>');
	var events = $("[id*=" + pageLegend + "_item" + nn + "_bookId]");
	var addEvent = true;
	var errors = [];
	var messages = [];
	var loopIndx;
	for(loopIndx = 0; loopIndx < events.length; loopIndx++) {
		if(subjectId == events[loopIndx].value) {
			var error = {code: "0000", text: "<?php echo $ERR_DUPLICATE_BOOK; ?>" + subjectText};
			errors.push(error);
		} else {

		}
		addEvent = displayErrorsAndMessages(errors, messages, nn, pageLegend);
	}
	if(addEvent && subjectText.length > 0 && subjectText[0].trim() != "") {
		// subjectText = getLeafNodeTextFromTreeText(eventText, '<?php echo $TREE_SEPERATOR; ?>');
		var subjects = "";
		var subjectLineText = subjectText.join('<?php echo $KEYWORD_TEXT_SEPERATOR; ?>');
		
		var loopIndx;
		for(loopIndx = 0; loopIndx < subjectText.length; loopIndx++, mm = Number(mm) + 1) {
			mm = mm.toString().length == 1 ? "0" + mm.toString() : mm.toString();
			var tokens = [
						{"tokenStr": "~~nn~~", "tokenValue": nn},
						{"tokenStr": "~~mm~~", "tokenValue": mm},
						{"tokenStr": "~~pageLegend~~", "tokenValue": pageLegend},
						{"tokenStr": "~~item_bookId~~", "tokenValue": subjectId},
						{"tokenStr": "~~item_bookText~~", "tokenValue": subjectText[loopIndx]},
						{"tokenStr": "~~item_bookIndex~~", "tokenValue": loopIndx + 1},
						{"tokenStr": "~~item_bookLine~~", "tokenValue": subjectLine}
						];
			subjects += substituteTokens('<?php echo $DISMISSIBLE_DISPLAY_BOOK_TOKEN_HTML; ?>', tokens);
		}
		
		var tokens = [
						{"tokenStr": "~~item_books~~", "tokenValue": subjects},
						{"tokenStr": "~~item_bookLineText~~", "tokenValue": subjectLineText}
						];
		$("#" + pageLegend + "_item" + nn + "_bookDisplayBox").append(substituteTokens('<?php echo $DISMISSIBLE_DISPLAY_BOOK_LINE_TOKEN_HTML; ?>', tokens));
		
		$("#" + pageLegend + "_item" + nn + "_bukId_book").val(null).trigger("change");
		$("#" + pageLegend + "_item" + nn + "_bukText_book").val("");
	}
	// $("#" + pageLegend + "_item" + nn + "_bukId_book").select2("open");
	// $("#select2-" + pageLegend + "_item" + nn + "_bukId_book-results").parent().prev().find("input").focus();
	$("#" + pageLegend + "_item" + nn + "_bukId_book").prev().find("input").focus();
}


function addUrlOrFileFolderPath(filePath) {
	var urlOrFileFolderPathElements = $("[id*= edit_item00_filePath]");
	var prev_mm = '00';
	if(urlOrFileFolderPathElements.length > 0) {
		prev_mm = urlOrFileFolderPathElements[urlOrFileFolderPathElements.length - 1].id.substr('edit_item00_filePath'.length, 2);
	}
	var mm = Number(prev_mm) + 1; //mm will always start with 01
	mm = mm.toString().length == 1 ? "0" + mm.toString() : mm.toString();
	
	if(filePath != "") {
		var urlOrFileFolderPath = filePath;
		var tokens = [
					{"tokenStr": "~~mm~~", "tokenValue": mm},
					{"tokenStr": "~~item_urlOrFileFolderPath~~", "tokenValue": urlOrFileFolderPath}
					];
		$("#edit_item00_urlOrFileFolderPathDisplayBox").append(substituteTokens('<?php echo $DISMISSIBLE_DISPLAY_URL_OR_FILE_fOLDER_PATH_TOKEN_HTML; ?>', tokens));
		$('#edit_item00_urlOrFileFolderPathText').val("");
	}
}

function switchParent(element) {
	if($(element).text().trim() == "<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>") {	//collapse the expanded clip details
		collapseParent(element);
	} else {													//expand the collapsed clip details
		expandParent(element);
	}
}

function collapseParent(element) {
	var pageLegend = element.id.substr(0, 4);
	$("#" + pageLegend + "_item00_collapsableDiv").removeClass("in");
	if($(element).find("span.sr-only").length == 1) {
		$(element).prev().text("<?php echo $LABEL_SWITCH_TO_DETAILED_VIEW; ?>");
		$(element).attr("data-original-title", "<?php echo $LABEL_SWITCH_TO_DETAILED_VIEW; ?>");
		$(element).find("span.sr-only").text("<?php echo $LABEL_SWITCH_TO_DETAILED_VIEW; ?>");
	} else {
		$(element).text("<?php echo $LABEL_SWITCH_TO_DETAILED_VIEW; ?>");
		$(element).next().attr("data-original-title", "<?php echo $LABEL_SWITCH_TO_DETAILED_VIEW; ?>");
		$(element).next().find("span.sr-only").text("<?php echo $LABEL_SWITCH_TO_DETAILED_VIEW; ?>");
	}
}

function expandParent(element) {
	var pageLegend = element.id.substr(0, 4);
	$("#" + pageLegend + "_item00_collapsableDiv").addClass("in");
	if($(element).find("span.sr-only").length == 1) {
		$(element).prev().text("<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>");
		$(element).attr("data-original-title", "<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>");
		$(element).find("span.sr-only").text("<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>");
	} else {
		$(element).text("<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>");
		$(element).next().attr("data-original-title", "<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>");
		$(element).next().find("span.sr-only").text("<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>");
	}
}

function switchClip(element) {
	if($(element).text().trim() == "<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>") {	//collapse the expanded clip details
		collapseClip(element);
	} else {													//expand the collapsed clip details
		expandClip(element);
	}
}

function collapseClip(element) {
	var nn = element.id.substr('edit_item'.length, 2);
	$("#edit_item" + nn + "_collapsable_div").removeClass("in");
	$(element).attr("data-original-title", "<?php echo $LABEL_SWITCH_TO_DETAILED_VIEW; ?>");
	$(element).find("span.sr-only").text("<?php echo $LABEL_SWITCH_TO_DETAILED_VIEW; ?>");
}

function expandClip(element) {
	var nn = element.id.substr('edit_item'.length, 2);
	$("#edit_item" + nn + "_collapsable_div").addClass("in");
	$(element).attr("data-original-title", "<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>");
	$(element).find("span.sr-only").text("<?php echo $LABEL_SWITCH_TO_BRIEF_VIEW; ?>");
}

function switchAllClips(element) {
	if($(element).text().trim() == "<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_BRIEF_VIEW; ?>") {	//collapse the expanded clip details
		collapseAllClips(element);
	} else {															//expand the collapsed clip details
		expandAllClips(element);
	}
}

function collapseAllClips(element) {
	var switchClipElements = $("[id*=_switchClip]");
	var loopIndx;
	for(loopIndx = 0; loopIndx < switchClipElements.length; loopIndx++) {
		collapseClip(switchClipElements[loopIndx]);
	}
	if($(element).find("span.sr-only").length == 1) {
		$(element).prev().text("<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_DETAILED_VIEW; ?>");
		$(element).attr("data-original-title", "<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_DETAILED_VIEW; ?>");
		$(element).find("span.sr-only").text("<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_DETAILED_VIEW; ?>");
	} else {
		$(element).text("<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_DETAILED_VIEW; ?>");
		$(element).next().attr("data-original-title", "<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_DETAILED_VIEW; ?>");
		$(element).next().find("span.sr-only").text("<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_DETAILED_VIEW; ?>");
	}
}

function expandAllClips(element) {
	var switchClipElements = $("[id*=_switchClip]");
	var loopIndx;
	for(loopIndx = 0; loopIndx < switchClipElements.length; loopIndx++) {
		expandClip(switchClipElements[loopIndx]);
	}
	if($(element).find("span.sr-only").length == 1) {
		$(element).prev().text("<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_BRIEF_VIEW; ?>");
		$(element).attr("data-original-title", "<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_BRIEF_VIEW; ?>");
		$(element).find("span.sr-only").text("<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_BRIEF_VIEW; ?>");
	} else {
		$(element).text("<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_BRIEF_VIEW; ?>");
		$(element).next().attr("data-original-title", "<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_BRIEF_VIEW; ?>");
		$(element).next().find("span.sr-only").text("<?php echo $LABEL_SWITCH_ALL_CLIPS_TO_BRIEF_VIEW; ?>");
	}
}

function populateEndDate(element) {
	var endDateElement = $("#" + element.id.replace('Start' , 'End'));
	$(endDateElement).val($(element).val());
}

function generateSlidatDate(arrChild,itemDate,dbFieldArr){
	if(itemDate != ""){
		var itemDateArr = itemDate.split(" ");
		var itemDateArrDdMmYyyy = splitDate(itemDateArr);
		if(itemDateArr.length == 3){
			arrChild[dbFieldArr[0]] =itemDateArrDdMmYyyy[0];
			arrChild[dbFieldArr[1]] =itemDateArrDdMmYyyy[1];
			arrChild[dbFieldArr[2]] =itemDateArrDdMmYyyy[2];

		}else if(itemDateArr.length == 2){
			arrChild[dbFieldArr[1]] =itemDateArrDdMmYyyy[1];
			arrChild[dbFieldArr[2]] =itemDateArrDdMmYyyy[2];
		}else if(itemDateArr.length == 1){
			arrChild[dbFieldArr[2]] =itemDateArrDdMmYyyy[2];
		}
	}
};

function sendToServer(htmlItemId, itemType) {
	debugger;
	var sentToServer = true;
	var action = htmlItemId.substr('XXXX_itemNN_'.length);
	var nn = htmlItemId.substr('XXXX_item'.length, 2);
	var pageLegend = htmlItemId.substr(0, 4);
	if(validate(action, nn, itemType)) {
		var XXXX_itemNN_ = htmlItemId.substr(0, 'XXXX_itemNN_'.length);
		var jsItemUItoDBMappingArrayKeys = ['<?php echo implode("', '", array_keys($itemUItoDBMappingArray)); ?>'];
		var jsItemTextUItoDBMappingArrayKeys = ['<?php echo implode("', '", array_keys($itemTextsUItoDBMappingArray)); ?>'];
		var jsItemDateUItoDBMappingArrayKeys = ['<?php echo implode("', '", array_keys($itemDateUItoDBMappingArray)); ?>'];
		var jsItemKeywordLineUItoDBMappingArrayKeys = ['<?php echo implode("', '", array_keys($itemKeywordLineUItoDBMappingArray)); ?>'];
		var jsItemSubjectsUItoDBMappingArrayKeys = ['<?php echo implode("', '", array_keys($itemSubjectsUItoDBMappingArray)); ?>'];
		var jsItemEventsUItoDBMappingArrayKeys = ['<?php echo implode("', '", array_keys($itemEventsUItoDBMappingArray)); ?>'];
		var jsItemLanguagesUItoDBMappingArrayKeys = ['<?php echo implode("', '", array_keys($itemLanguagesUItoDBMappingArray)); ?>'];
		var jsItemLocationsUItoDBMappingArrayKeys = ['<?php echo implode("', '", array_keys($itemLocationsUItoDBMappingArray)); ?>'];
		var jsItemReferencesUItoDBMappingArrayKeys = ['<?php echo implode("', '", array_keys($itemReferencesUItoDBMappingArray)); ?>'];

		var arrParent = {};
		var arrText = [];
		var arrDate = [];
		var arrKeywordLine = [];
		var arrSubject = [];
		var arrEvent = [];
		var arrLanguage = [];
		var arrLocation = [];
		var arrReference = [];
		var arrItemNotes = [];
		var arrItemVakta = [];
		var arrItemCategories = [];
		var arrTags = [];
		
		var iLoopIndx;
		for(iLoopIndx = 0; iLoopIndx < jsItemUItoDBMappingArrayKeys.length; iLoopIndx++) {
			arrParent[jsItemUItoDBMappingArrayKeys[iLoopIndx]] = $("#" + XXXX_itemNN_ + jsItemUItoDBMappingArrayKeys[iLoopIndx]).val();
		}

		debugger;
		if(nn == '00'){//This only for video not for clip 
			var filePathElements = $("[id*= edit_item00_filePath]");
			var filePaths = [];
			var iLoopIndx;
			for(iLoopIndx = 0; iLoopIndx < filePathElements.length; iLoopIndx++) {
				filePaths[iLoopIndx] = filePathElements[iLoopIndx].value;
			}
			arrParent["urlOrFileFolderPath"] = filePaths.join('<?php echo $FILE_PATH_SEPERATOR; ?>');
		}
		var iLoopIndx;
		var arrTextChild = {};
		for(iLoopIndx = 0; iLoopIndx < jsItemTextUItoDBMappingArrayKeys.length; iLoopIndx++) {
			arrTextChild[jsItemTextUItoDBMappingArrayKeys[iLoopIndx]] = $("#" + XXXX_itemNN_ + jsItemTextUItoDBMappingArrayKeys[iLoopIndx]).val();
		}
		arrText.push(arrTextChild);

		var arrChild = {};
		var eventStartDate = $("#" + pageLegend + "_item" + nn + "_eventStartDateTime").val();
		generateSlidatDate(arrChild,eventStartDate,['eventStartDateDD','eventStartDateMM','eventStartDateYYYY']);
		var eventEndDate = $("#" + pageLegend + "_item" + nn + "_eventEndDateTime").val();
		generateSlidatDate(arrChild,eventEndDate,['eventEndDateDD','eventEndDateMM','eventEndDateYYYY']);
		arrDate.push(arrChild);	


		var tagElement = $("#"+XXXX_itemNN_ + "tags").select2('data');//jsItemSubjectsUItoDBMappingArrayKeys
		debugger;
		if(tagElement){
			$.each(tagElement, function(index, obj) {
				var arrChild = {'tagLine' : 1,
						'tagIndex':index + 1,
						'tagId':isNaN(obj.id) ? 0 : obj.id,
						'tagText':obj.text
						};
				arrTags.push(arrChild);
			});
		}

		
		var subjectElements = $("#"+XXXX_itemNN_ + "subId_subject").select2('data');//jsItemSubjectsUItoDBMappingArrayKeys
		if(subjectElements){
			$.each(subjectElements, function(index, obj) {
				var arrChild = {'subjectLine' : 1,
						'subjectIndex':index + 1,
						'subjectId':isNaN(obj.id) ? 0 : obj.id,
						'subjectText':obj.text
						};
				arrSubject.push(arrChild);
			});
		}

		debugger;
		var categoriesElements = $("#"+XXXX_itemNN_ + "itemCategory").select2('data');//jsItemSubjectsUItoDBMappingArrayKeys
		if(categoriesElements){
			$.each(categoriesElements, function(index, obj) {
				var arrChild = {
						'categoryId':isNaN(obj.id) ? 0 : obj.id,
						'categoryTitle':obj.text
						};
				arrItemCategories.push(arrChild);
			});
		}
		
		var eventElements = $("#"+XXXX_itemNN_ + "evtId_event").select2('data');
		if(eventElements){
			$.each(eventElements, function(index, obj) {
				var arrChild = {'eventId' : isNaN(obj.id) ? 0 : obj.id,
						'eventText':obj.text
						};
				arrEvent.push(arrChild);
			});
		}

		var languageElements = $("#"+XXXX_itemNN_ + "langId_language").select2('data');
		$.each(languageElements, function(i, obj) {
			var arrChild = {'languageId' : obj.id,
					'languageText':obj.text};
			arrLanguage.push(arrChild);
		});
		
		var locationElements = $("#"+XXXX_itemNN_ + "locId_location").select2('data');
		if(locationElements){
			$.each(locationElements, function(i, location) {
				var locationText = location.text;
				try {
					locationText = locationText.substr(locationText.lastIndexOf('>') + 1);				
				} catch (e) {
				}
				var arrChild = {'locationId' : location.id,
						'locationText':locationText,
						'locationTree':location.text};
				arrLocation.push(arrChild);
			});
		}
		
		var referenceElements = $("[id*=" + XXXX_itemNN_ + jsItemReferencesUItoDBMappingArrayKeys[0] + "]");
		var iLoopIndx;
		for(iLoopIndx = 0; iLoopIndx < referenceElements.length; iLoopIndx++) {
			var MM = referenceElements[iLoopIndx].id.substr(referenceElements[iLoopIndx].id.length - 2);
			var arrReferenceChild = {};
			var jLoopIndx;
			for(jLoopIndx = 0; jLoopIndx < jsItemReferencesUItoDBMappingArrayKeys.length; jLoopIndx++) {
				arrReferenceChild[jsItemReferencesUItoDBMappingArrayKeys[jLoopIndx]] = $("#" + XXXX_itemNN_ + jsItemReferencesUItoDBMappingArrayKeys[jLoopIndx] + MM).val();
			}
			arrReference.push(arrReferenceChild);
		}

		var arrItemNotesChild = {'noteText':$("[id*=" + XXXX_itemNN_ + "keywordId_keyword]").val()};
		arrItemNotes.push(arrItemNotesChild);

		var vaktaElements = $("#"+XXXX_itemNN_ + "vakta").select2('data');
		if(vaktaElements){
			$.each(vaktaElements, function(index, obj) {
				var arrChild = {'vaktaId' : isNaN(obj.id) ? 0 : obj.id,
						'vaktaName':obj.text
						};
				arrItemVakta.push(arrChild);
			});
		}
		
		
		arrParent["textArray"] = arrText;
		arrParent["eventDateArray"] = arrDate;
		arrParent["keywordLineArray"] = arrKeywordLine;
		arrParent["subjectArray"] = arrSubject;
		arrParent["eventArray"] = arrEvent;
		arrParent["languageArray"] = arrLanguage;
		arrParent["locationArray"] = arrLocation;
		arrParent["referenceArray"] = arrReference;
		arrParent["itemNotes"] = arrItemNotes;
		arrParent["itemVakta"] = arrItemVakta;
		arrParent["itemCategories"] = arrItemCategories;
		arrParent["itemTags"] = arrTags;

		var jsonStr = "[" + JSON.stringify(arrParent) + "]";
		alert(jsonStr);
		
		var strUrl = "itemJsonString=" + jsonStr;
		xmlhttp = getXmlhttp();
		xmlhttp.onreadystatechange = function()	{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				if(xmlhttp.responseText != "") {
					if(action == "save") {
						// $("#item" + nn + "_messages_div").html(xmlhttp.responseText);
						var JsonString = xmlhttp.responseText;
						var jsonData = JSON.parse(JsonString);
						
						// alert(JsonString);
						
						//After saving, set the itemId on the item saved
						var itemId = jsonData["itemId"];
						if(itemId == undefined) {
							//something went wrong in ItemAddUpdate.php: I did not recieve itemId from there.
							sentToServer = false;
						} else {
							$("#edit_item" + nn + "_itemId").val(itemId);
						}
						
						var errors = jsonData["errors"];
						var messages = jsonData["messages"];
						sentToServer = displayErrorsAndMessages(errors, messages, nn, pageLegend);
						
					} else if(action == "search") { //replicate this as 'save'
						populateSearchTable(xmlhttp.responseText);
						// $("#item" + nn + "_messages_div").html(xmlhttp.responseText);
						// $("#item" + nn + "_messages_div").show();
					}
				} else {
					sentToServer = false;
				}
			} /*else { this wont work may be b'coz it is executed b4 the ajax call
				sentToServer = false;
			} */
		}
		console.log(strUrl);
		if(action == "save") {
			xmlhttp.open("POST", "<?php echo $FILE_ITEM_ADD_UPDATE_PHP; ?>?" + strUrl, false); //encodeURIComponent(strUrl)
		} else if(action == "search") {
			xmlhttp.open("POST", "<?php echo $FILE_ITEM_SEARCH_PHP; ?>?" + strUrl); //encodeURIComponent(strUrl)
		}
		xmlhttp.send();
		
		return sentToServer;
	} else {
		return sentToServer = false;
	}
}

function validate(action, nn, itemType) {
	if(itemType == <?php echo $ITEMTYPE_IMAGE; ?> || itemType == <?php echo $ITEMTYPE_PRASANG; ?>) return true;
	var isValid = true;
	var errors = [];
	var messages = [];
	var pageLegend = action == "save" ? "edit" : "srch";
	
	//Validate formats
	var timeFormatRegExp = /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i;
		
	if(nn == '00') {
		// TODO: Validate File Path : $("#" + pageLegend + "_item" + nn + "_urlOrFileFolderPath").val()
	} else {
		if($("#" + pageLegend + "_item" + nn + "_clipStartTime").val() != "" && !timeFormatRegExp.test($("#" + pageLegend + "_item" + nn + "_clipStartTime").val())) {
			var error = {code: "0000", text: "<?php echo $ERR_INVALID_CLIP_START_TIME; ?>"};
			errors.push(error);
			$("#" + pageLegend + "_item" + nn + "_clipStartTime").parent().removeClass("form-group");
			$("#" + pageLegend + "_item" + nn + "_clipStartTime").parent().addClass("has-error");
		} else {
			$("#" + pageLegend + "_item" + nn + "_clipStartTime").parent().removeClass("has-error");
			$("#" + pageLegend + "_item" + nn + "_clipStartTime").parent().addClass("form-group");
		}
		if($("#" + pageLegend + "_item" + nn + "_clipEndTime").val() != "" && !timeFormatRegExp.test($("#" + pageLegend + "_item" + nn + "_clipEndTime").val())) {
			var error = {code: "0000", text: "<?php echo $ERR_INVALID_CLIP_END_TIME; ?>"};
			errors.push(error);
			$("#" + pageLegend + "_item" + nn + "_clipEndTime").parent().removeClass("form-group");
			$("#" + pageLegend + "_item" + nn + "_clipEndTime").parent().addClass("has-error");
		} else {
			$("#" + pageLegend + "_item" + nn + "_clipEndTime").parent().removeClass("has-error");
			$("#" + pageLegend + "_item" + nn + "_clipEndTime").parent().addClass("form-group");
		}
	}
	
	if(action == 'save') {
		//Validate mandatory fields
		if($("#" + pageLegend + "_item" + nn + "_itemTitle").val().trim() == "") {
			var error = {code: "0000", text: "<?php echo $ERR_MANDATORY_TITLE; ?>"};
			errors.push(error);
			$("#" + pageLegend + "_item" + nn + "_itemTitle").parent().removeClass("form-group");
			$("#" + pageLegend + "_item" + nn + "_itemTitle").parent().addClass("has-error");
		} else {
			$("#" + pageLegend + "_item" + nn + "_itemTitle").parent().removeClass("has-error");
			$("#" + pageLegend + "_item" + nn + "_itemTitle").parent().addClass("form-group");
		}
		/* if($("#" + pageLegend + "_item" + nn + "_keywordDisplayBox").text().trim() == "") { */
		debugger;
		if($("#" + pageLegend + "_item" + nn + "_tags").val() == ""){
			var error = {code: "0000", text: "<?php echo $ERR_MANDATORY_KEYWORDS; ?>"};
			errors.push(error);
			$(".keywords-box").parent().removeClass("form-group");
			$(".keywords-box").parent().addClass("has-error");
		} else {
			$(".keywords-box").parent().removeClass("has-error");
			$(".keywords-box").parent().addClass("form-group");
		}
		
		if(nn == '00') {
			if($("[id*=" + pageLegend + "_item" + nn + "_filePath]").length == 0) {
				var error = {code: "0000", text: "<?php echo $ERR_MANDATORY_FILE_PATH; ?>"};
				errors.push(error);
				$("#" + pageLegend + "_item" + nn + "_urlOrFileFolderPathDisplayBox").parent().removeClass("form-group");
				$("#" + pageLegend + "_item" + nn + "_urlOrFileFolderPathDisplayBox").parent().addClass("has-error");
			} else {
				$("#" + pageLegend + "_item" + nn + "_urlOrFileFolderPathDisplayBox").parent().removeClass("has-error");
				$("#" + pageLegend + "_item" + nn + "_urlOrFileFolderPathDisplayBox").parent().addClass("form-group");
			}
		} else {
			if($("#" + pageLegend + "_item" + nn + "_clipStartTime").val().trim() == "") {
				var error = {code: "0000", text: "<?php echo $ERR_MANDATORY_CLIP_START_TIME; ?>"};
				errors.push(error);
				$("#" + pageLegend + "_item" + nn + "_clipStartTime").parent().removeClass("form-group");
				$("#" + pageLegend + "_item" + nn + "_clipStartTime").parent().addClass("has-error");
			} else {
				$("#" + pageLegend + "_item" + nn + "_clipStartTime").parent().removeClass("has-error");
				$("#" + pageLegend + "_item" + nn + "_clipStartTime").parent().addClass("form-group");
			}
			if($("#" + pageLegend + "_item" + nn + "_clipEndTime").val().trim() == "") {
				var error = {code: "0000", text: "<?php echo $ERR_MANDATORY_CLIP_END_TIME; ?>"};
				errors.push(error);
				$("#" + pageLegend + "_item" + nn + "_clipEndTime").parent().removeClass("form-group");
				$("#" + pageLegend + "_item" + nn + "_clipEndTime").parent().addClass("has-error");
			} else {
				$("#" + pageLegend + "_item" + nn + "_clipEndTime").parent().removeClass("has-error");
				$("#" + pageLegend + "_item" + nn + "_clipEndTime").parent().addClass("form-group");
			}
		}
	}
	
	return isValid = displayErrorsAndMessages(errors, messages, nn, pageLegend);
}

function addAnotherVideo() {
	if($("#edit_item00_itemId").val() == "" || $("#edit_item00_itemId").val() == undefined) {
		$("#videoInfoNotSavedModal").modal();
	} else {
		parent.location.href = '<?php echo $FILE_VIDEOINFO_PHP; ?>';
	}
}

function addAnotherImage() {
	if($("#edit_item00_itemId").val() == "" || $("#edit_item00_itemId").val() == undefined) {
		$("#imageInfoNotSavedModal").modal();
	} else {
		parent.location.href = '<?php echo $FILE_IMAGE_PHP; ?>';
	}
}

function addAnotherPrasang() {
	if($("#edit_item00_itemId").val() == "" || $("#edit_item00_itemId").val() == undefined) {
		$("#prasangInfoNotSavedModal").modal();
	} else {
		parent.location.href = '<?php echo $FILE_PRASANG_PHP; ?>';
	}
}

function currentSlide(currentClipId,nn){
	$(".clipInfo").hide();
	$(".clipHeader").html('<?php echo $LABEL_CLIP_DETAILS ?>' + nn);

	debugger;
	$("#"+$(currentClipId).attr('data-clipHeader')).show();
}

function addClipInfo(sendToServerFlag) {
	var formElements = $("[id$=_form]");
	var prev_nn = '00';
	debugger;
	if(formElements.length > 0) {
		prev_nn = formElements[formElements.length - 1].id.substr('edit_item'.length, 2);
	}
	
	//save previous clip before adding a new clip
	var sentToServer = true;
 	if(sendToServerFlag) {
 		if($("#edit_item" + prev_nn + "_itemId").val() == "" || $("#edit_item" + prev_nn + "_itemId").val() == undefined)
			sentToServer = sendToServer("edit_item" + prev_nn + "_save", '<?php echo $ITEMTYPE_VIDEO; ?>');
 	}
	if(sentToServer) { //add clip
		var nn = Number(prev_nn) + 1;
		nn = nn.toString().length == 1 ? "0" + nn.toString() : nn.toString();

		
		var tokens = [
					{"tokenStr": "~~nn~~", "tokenValue": nn}
					];
		debugger;

		$(".clipInfo").hide();
		$(substituteTokens('<?php echo $THUMNAILHTML;?>', tokens)).appendTo( $( ".clip-thumnails" ) );

		
		if(nn == "01" && $("#clipDetails_pageSubHeader").length == 0) { //in this case, add Page Sub-Header 'Clip Details' and then add Clip
			$("#addClip_div").before(substituteTokens('<?php echo $CLIP_DETAILS_PAGE_SUB_HEADER_TOKEN_HTML; ?>', tokens));
		}else{
				$(".clipHeader").html('<?php echo $LABEL_CLIP_DETAILS ?>' + nn);
		}
		//add clip
		$("#addClip_div").before(substituteTokens('<?php echo $ADD_CLIP_INFO_TOKEN_HTML; ?>', tokens));
		debugger;
		var nextClipElement = "#edit_item" + nn;
		bindVideoEventsAndComponents(true,nextClipElement);
		
		if(sendToServerFlag) {
			//Populate child's parentId
			$("#edit_item" + nn + "_parentItemId").val($("#edit_item00_itemId").val());
			//Populate other details from parent to child
			copyDates(nn);
			$("#edit_item" + nn + "_confidentiality").children("[value='" + $("#edit_item00_confidentiality").val() + "']").attr("selected", true);
			$("#edit_item" + nn + "_accessibility").children("[value='" + $("#edit_item00_accessibility").val() + "']").attr("selected", true);
			$("#edit_item" + nn + "_language").children("[value='" + $("#edit_item00_language").val() + "']").attr("selected", true);
			$("#edit_item" + nn + "_clipStartTime").val($("#edit_item" + prev_nn + "_clipEndTime").val());
		}
		
		if(nn == '01') {										//collapse parent
			collapseParent(document.getElementById("edit_item00_switchParent1"));
		} else {												//collapse all other clips except this
			collapseAllClips(document.getElementById("edit_item00_switchAllClips1"));
			expandClip(document.getElementById("edit_item" + nn + "_switchClip1"));
		}
	}
}

function copyDates(nn) {
	// $("#edit_item"+ nn +"_dateDisplayBox").html($("#edit_item00_dateDisplayBox").html().replace(/edit_item00_/g, "edit_item" + nn + "_"));
}

function deleteClip(nn) {
	var itemId = $("#edit_item" + nn + "_itemId").val();
// 	alert(itemId);
	var sentToServer = true;
	var formElements = $("[id$=_form]");
	if(itemId == "") {
		if(formElements.length == 2) { //in this case, delete Page Sub-Header 'Clip Details' and then delete Clip
			$("#clipDetails_pageSubHeader").fadeOut().remove();
			$("#edit_item" + nn + "_form").fadeOut().remove();
	  	} else { //only delete clip
	  		$("#edit_item" + nn + "_form").fadeOut().remove();
	  	}
	} else {
		var strUrl = "itemId=" + itemId;
		xmlhttp = getXmlhttp();
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				if(xmlhttp.responseText != "") {
					var JsonString = xmlhttp.responseText;
					var jsonData = JSON.parse(JsonString);
						
					var errors = jsonData["errors"];
					var messages = jsonData["messages"];
					sentToServer = displayErrorsAndMessages(errors, messages, nn, "edit");
					
					if(sentToServer) {
						$("#edit_item" + nn + "_fixed_div").find("input").attr("disabled", "true");
						$("#edit_item" + nn + "_fixed_div").find("select").attr("disabled", "true");
						$("#edit_item" + nn + "_delete").addClass("disabled");
						$("#edit_item" + nn + "_switchClipToBriefView1").addClass("disabled");
						$("#edit_item" + nn + "_switchClipToBriefView2").addClass("disabled");
						$("#edit_item" + nn + "_collapsable_div").fadeOut().remove();
					}
				} else {
					//do nothing
				}
			}
		}
		console.log(strUrl);
		xmlhttp.open("GET", "<?php echo $FILE_ITEM_DELETE_PHP; ?>?" + strUrl);
		xmlhttp.send();
	}
}


$(document).on('focus', '.select2', function (e) {
	  if (e.originalEvent) {
		  var arry = $(this).siblings('select').select2('data');
		  if(arry && arry.length == 0){
	    	$(this).siblings('select').select2('open');
		  }    
	  } 
});
$(".select2-container").tooltip({
    title: function() {
        debugger;
        return $(this).next().attr("title");
    },
    trigger: "hover",
    placement: "auto",
});
/*function autoTab(source, destination) {
	if(source.value.length == source.getAttribute("maxlength")) {
		document.getElementById(destination).focus();
	}
}*/

/* ========================================================================
 * Bootstrap: alert.js v3.3.5
 * http://getbootstrap.com/javascript/#alerts
 * ========================================================================
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */
+function ($) {
  'use strict';

  // ALERT CLASS DEFINITION
  // ======================

  var dismiss = '[data-dismiss="tagX"]'
  var Alert   = function (el) {
    $(el).on('click', dismiss, this.close)
  }

  Alert.VERSION = '3.3.5'

  Alert.TRANSITION_DURATION = 150

  Alert.prototype.close = function (e) {
    var $this    = $(this)
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = $(selector)

    if (e) e.preventDefault()

    if (!$parent.length) {
        if($this.closest('.tag').length == 1) {
        	$parent = $this.closest('.tag')
        } else if($this.closest('.tag-sm').length == 1) {
        	$parent = $this.closest('.tag-sm')
        }
    }

    $parent.trigger(e = $.Event('close.bs.alert'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      // detach from parent, fire event then clean up data
      $parent.detach().trigger('closed.bs.alert').remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent
        .one('bsTransitionEnd', removeElement)
        .emulateTransitionEnd(Alert.TRANSITION_DURATION) :
      removeElement()
  }


  // ALERT PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.alert')

      if (!data) $this.data('bs.alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.alert

  $.fn.alert             = Plugin
  $.fn.alert.Constructor = Alert


  // ALERT NO CONFLICT
  // =================

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  };


  // ALERT DATA-API
  // ==============

  $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)

}(jQuery);


</script>