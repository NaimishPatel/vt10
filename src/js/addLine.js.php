function addLine(htmlItemId, eleName) {
	var nn = htmlItemId.substr('XXXX_item'.length, 2);
	var pageLegend = htmlItemId.substr(0, 4);
	var subjectElements = $("[id*= " + pageLegend + "_item" + nn + "_" + eleName + "Line]");
	var prev_mm = '00';
	if(subjectElements.length > 0) {
		prev_mm = subjectElements[subjectElements.length - 1].id.substr((pageLegend + '_itemNN_" + eleName + "Line').length, 2);
	}
	var mm = Number(prev_mm) + 1; //mm will always start with 01
	
	var prev_subjectLine = 0;
	if(subjectElements.length > 0) {
		prev_subjectLine = Number(subjectElements[subjectElements.length - 1].value);
	}
	var subjectLine = prev_subjectLine + 1; //subjectLine will always start with 1
	
	var subjectId = $("#" + pageLegend + "_item" + nn + "_evtId_event").val();
	var subjectText = $("#" + pageLegend + "_item" + nn + "_evtText_event").val().split('<?php echo $KEYWORD_TEXT_SEPERATOR; ?>');
	var events = $("[id*=" + pageLegend + "_item" + nn + "_eventId]");
	var addEvent = true;
	var errors = [];
	var messages = [];
	var loopIndx;
	for(loopIndx = 0; loopIndx < events.length; loopIndx++) {
		if(subjectId == events[loopIndx].value) {
			var error = {code: "0000", text: "<?php echo $ERR_DUPLICATE_EVENT; ?>" + subjectText};
			errors.push(error);
		} else {

		}
		addEvent = displayErrorsAndMessages(errors, messages, nn, pageLegend);
	}
	if(addEvent && subjectText.length > 0 && subjectText[0].trim() != "") {
		// subjectText = getLeafNodeTextFromTreeText(eventText, '<?php echo $TREE_SEPERATOR; ?>');
		var subjects = "";
		var subjectLineText = subjectText.join('<?php echo $KEYWORD_TEXT_SEPERATOR; ?>');
		
		var loopIndx;
		for(loopIndx = 0; loopIndx < subjectText.length; loopIndx++, mm = Number(mm) + 1) {
			mm = mm.toString().length == 1 ? "0" + mm.toString() : mm.toString();
			var tokens = [
						{"tokenStr": "~~nn~~", "tokenValue": nn},
						{"tokenStr": "~~mm~~", "tokenValue": mm},
						{"tokenStr": "~~pageLegend~~", "tokenValue": pageLegend},
						{"tokenStr": "~~item_eventId~~", "tokenValue": subjectId},
						{"tokenStr": "~~item_eventText~~", "tokenValue": subjectText[loopIndx]},
						{"tokenStr": "~~item_eventIndex~~", "tokenValue": loopIndx + 1},
						{"tokenStr": "~~item_eventLine~~", "tokenValue": subjectLine}
						];
			subjects += substituteTokens('<?php echo $DISMISSIBLE_DISPLAY_EVENT_TOKEN_HTML; ?>', tokens);
		}
		
		var tokens = [
						{"tokenStr": "~~item_events~~", "tokenValue": subjects},
						{"tokenStr": "~~item_eventLineText~~", "tokenValue": subjectLineText}
						];
		$("#" + pageLegend + "_item" + nn + "_eventDisplayBox").append(substituteTokens('<?php echo $DISMISSIBLE_DISPLAY_EVENT_LINE_TOKEN_HTML; ?>', tokens));
		
		$("#" + pageLegend + "_item" + nn + "_evtId_event").val(null).trigger("change");
		$("#" + pageLegend + "_item" + nn + "_evtText_event").val("");
	}
	$("#" + pageLegend + "_item" + nn + "_evtId_event").select2("open");
	$("#select2-" + pageLegend + "_item" + nn + "_evtId_event-results").parent().prev().find("input").focus();
}