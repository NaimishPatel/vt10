<?php
	opcache_reset();
?>
<script>
<?php
	if($itemId != null && $itemId != "") {
		echo "viewMode = true;";
		echo "itemId = '".$itemId."';";
	}
	else {
		echo "viewMode = false;";
	 // echo "alert(JsonString);";
	}
?>

$(document).ready(function(){
	$(window).scroll(function() {
		if ($(document).scrollTop() > 200) {
		  $(".scroll-to-top").fadeIn(200);
		} else {
		  $(".scroll-to-top").fadeOut(200);
		}
	});

	// Append options to select dropdowns - Book Title
	$('.book').select2({
		placeholder: "<?php echo $LABEL_BOOK_PLACEHOLDER; ?>",
		multiple: "multiple",
		tags: true,
		tokenSeparators: [';'],
		ajax: {
				url: "<?php echo $FILE_BOOK_SUGGESTIONS_PHP; ?>",
				dataType: "json",
				dalay: 250,
				data: 			function(params) {
									return {
										searchText: params.term,
										triggererElementId: this[0].id
									};
								}
		},
		// data: <?php echo $BOOK_TITLE_LOOKUPS; ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 1
	});
	$('.book').val(null).trigger("change");
	bindVideoEventsAndComponents();
	
	if(viewMode) {	//populate the page
		// JsonString = '[{"itemId":"123661","parentItemId":"","itemCategory":"1.H","itemTitle":"Guruhari Darshan trial","technicalQuality":"3","technicalNotes":"Good","urlOrFileFolderPath":"My Videos","itemType":"1","itemSource":"bapsChannel","confidentiality":"1","accessibility":"01","language":"001","additionalDescp":"Description for Darshan","eventDateArray":[{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"12","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"12"},{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"11","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"11"}],"keywordLineArray":[{"keywordLine":"3","keywordIndex":"1","keywordId":"10","keywordText":"Pramukh Swami Maharaj"},{"keywordLine":"3","keywordIndex":"2","keywordId":"wearing","keywordText":"wearing"},{"keywordLine":"3","keywordIndex":"3","keywordId":"1097","keywordText":"Chakdis"},{"keywordLine":"4","keywordIndex":"1","keywordId":"sadhus","keywordText":"sadhus"},{"keywordLine":"4","keywordIndex":"2","keywordId":"1759","keywordText":"dancing"}],"subjectArray":[{"subjectLine":"1","subjectIndex":"1","subjectId":"2177","subjectText":"àª•àª¾àª¨ / Ears"},{"subjectLine":"2","subjectIndex":"1","subjectId":"3506","subjectText":"Topic"}],"eventArray":[{"eventLine":"1","eventIndex":"1","eventId":"1910","eventText":"1995 - Amrut Mahotsav"},{"eventLine":"2","eventIndex":"1","eventId":"1907","eventText":"1990 - Yuva Adhiveshan"}],"locationArray":[{"locationId":"10105275","locationText":"Salangpur, BAPS Mandir","locationTree":"Salangpur, BAPS Mandir"},{"locationId":"10105274","locationText":"Mumbai (M. Corp.)","locationTree":"Mumbai (M. Corp.)"}]},{"itemId":"123665","parentItemId":"123661","itemCategory":"1.H","itemTitle":"Guruhari Darshan trial","technicalQuality":"3","technicalNotes":"Good","urlOrFileFolderPath":"My Videos","itemType":"1","itemSource":"bapsChannel","confidentiality":"1","accessibility":"01","language":"001","additionalDescp":"Description for Darshan","eventDateArray":[{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"12","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"12"},{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"11","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"11"}],"keywordLineArray":[{"keywordLine":"3","keywordIndex":"1","keywordId":"10","keywordText":"Pramukh Swami Maharaj"},{"keywordLine":"3","keywordIndex":"2","keywordId":"wearing","keywordText":"wearing"},{"keywordLine":"3","keywordIndex":"3","keywordId":"1097","keywordText":"Chakdis"},{"keywordLine":"4","keywordIndex":"1","keywordId":"sadhus","keywordText":"sadhus"},{"keywordLine":"4","keywordIndex":"2","keywordId":"1759","keywordText":"dancing"}],"subjectArray":[{"subjectLine":"1","subjectIndex":"1","subjectId":"2177","subjectText":"àª•àª¾àª¨ / Ears"},{"subjectLine":"2","subjectIndex":"1","subjectId":"3506","subjectText":"Topic"}],"eventArray":[{"eventLine":"1","eventIndex":"1","eventId":"1910","eventText":"1995 - Amrut Mahotsav"},{"eventLine":"2","eventIndex":"1","eventId":"1907","eventText":"1990 - Yuva Adhiveshan"}],"locationArray":[{"locationId":"10105275","locationText":"Salangpur, BAPS Mandir","locationTree":"Salangpur, BAPS Mandir"},{"locationId":"10105274","locationText":"Mumbai (M. Corp.)","locationTree":"Mumbai (M. Corp.)"}]}]';
		var strUrl = "parentItemId=" + itemId;
		xmlhttp = getXmlhttp();
		xmlhttp.onreadystatechange = function()	{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				if(xmlhttp.responseText != "") {
					var JsonString = xmlhttp.responseText;
					var jsonData = JSON.parse(JsonString);
					// alert(JsonString);
					populateItemToView(jsonData);
				} else {
					//do nothing
				}
			}
		}
		console.log(strUrl);
		xmlhttp.open("POST", "<?php echo $FILE_ITEM_VIEW_PHP; ?>?" + strUrl, false);
		xmlhttp.send();
	}
});

// function bindPrasangEventsAndComponents() {
	// bindEventsAndComponents();
	// $(".subject").select2({
		// placeholder: "<?php echo $LABEL_SUBJECT_PLACEHOLDER; ?>",
		// multiple: "multiple",
		// tags: true,
		// tokenSeparators: [';', ','],
		// ajax: {
				// url: "<?php echo $FILE_KEYWORD_SUGGESTIONS_FOR_PRASANG_PHP; ?>",
				// dataType: "json",
				// dalay: 250,
				// data: 			function(params) {
									// return {
										// searchText: params.term,
										// triggererElementId: this[0].id
									// };
								// }
		// },
		// templateSelection: formatSelection,
		// templateResult: formatResult,
		// minimumInputLength: 3
	// });

	// $(".event").select2({
		// placeholder: "<?php echo $LABEL_EVENT_PLACEHOLDER; ?>",
		// ajax: {
				// url: "<?php echo $FILE_KEYWORD_SUGGESTIONS_FOR_PRASANG_PHP; ?>",
				// dataType: "json",
				// dalay: 250,
				// data: 			function(params) {
									// return {
										// searchText: params.term,
										// triggererElementId: this[0].id
									// };
								// }
		// },
		// templateSelection: formatSelection,
		// templateResult: formatResult,
		// minimumInputLength: 3
	// });

	// $(".keyword").select2({
		// placeholder: "<?php echo $LABEL_KEYWORD_PLACEHOLDER; ?>",
		// multiple: "multiple",
		// tags: true,
		// tokenSeparators: [';', ','],
		// ajax: {
				// url: "<?php echo $FILE_KEYWORD_SUGGESTIONS_FOR_PRASANG_PHP; ?>",
				// dataType: "json",
				// dalay: 250,
				// data: 			function(params) {
									// return {
										// searchText: params.term,
										// parentId: getParentLocIds(this[0].id),
										// triggererElementId: this[0].id
									// };
								// }
		// },
		// templateSelection: formatSelection,
		// templateResult: formatResult,
		// minimumInputLength: 3
	// });
// }

</script>