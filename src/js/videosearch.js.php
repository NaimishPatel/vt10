<?php
	opcache_reset();
?>
<script>

$(document).ready(function(){
	$(window).scroll(function() {
		if ($(document).scrollTop() > 200) {
		  $(".scroll-to-top").fadeIn(200);
		} else {
		  $(".scroll-to-top").fadeOut(200);
		}
	});

	// Append options to select dropdowns - Category
	$('.itemCategory').select2({
		placeholder: "<?php echo $LABEL_CATEGORY_PLACEHOLDER; ?>",
		data: <?php echo $ITEM_CATEGORY_LOOKUPS; ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 0
	});
	$('.itemCategory').val(null).trigger("change");
	bindVideoEventsAndComponents(false);
	$('[data-toggle="offcanvas"]').click(function () {
		$('.row-offcanvas').toggleClass('active');
	});
	
	// $("#editPageContent").hide();
});

function populateSearchTable(responseJson) {
//	var responseJson = '[{"itemId":"123508","parentItemId":"123","itemCategory":"1","itemTitle":"..Satsang Darshan-95th JanmaJayanti","technicalQuality":"4","technicalNotes":"voice and audio okay ","urlOrFileFolderPath":"c:/mediaStorage/2015/12/19","fileFolderName":"AGMZ1467.avi","itemType":"2","itemUniqueCode":"bapsSite1Item346","itemSource":"3","confidentiality":"4","accesssibility":"5","language":"543385","additionalDescp":"1. blah blah blah  2. blah blah blah  3. blah blah blah...","eventDateArray":[{"eventStartDateYYYY":"2016","eventStartDateMM":"12","eventStartDateDD":"19","eventEndDateYYYY":"2016","eventEndDateMM":"12","eventEndDateDD":"19"},{"eventStartDateYYYY":"2017","eventStartDateMM":"12","eventStartDateDD":"19","eventEndDateYYYY":"2017","eventEndDateMM":"12","eventEndDateDD":"19"}],"subjectArray":[{"subjectId":"4001","subjectText":"Nishkam Vartman1","subjectRank":"4"},{"subjectId":"4002","subjectText":"Nirlobh Vartman2","subjectRank":"4"},{"subjectId":"4003","subjectText":"Nirman Vartman3","subjectRank":"4"},{"subjectId":"4004","subjectText":"Nihsheh Vartman4","subjectRank":"4"},{"subjectId":"4005","subjectText":"Niswad Vartman5","subjectRank":"4"}],"keywordLineArray":[{"whoId":"10","actionId":"1711","whereId":"10105275","whenId":"1898","whoText":"Pramukh Swami Maharaj","actionText":"smiling","whomText":"Subhashbhai","whereText":"Salangpur, BAPS Mandir","whenText":"Shangar"},{"whoId":"1009","actionId":"1008","whatId":"1007","whomId":"1006","whereId":"1005","whoText":"Obama","actionText":"doing","whatText":"dandvat","whomText":"PramukhSwamiMaharaj","whereText":"Sarangpur YagnapurushVadi","whenText":"PSM 95th Janmajayanti","keywordLineRank":"5"}]},{"itemId":"123508","parentItemId":"123","itemCategory":"1","itemTitle":"..Satsang Darshan-95th JanmaJayanti","technicalQuality":"4","technicalNotes":"voice and audio okay ","urlOrFileFolderPath":"c:/mediaStorage/2015/12/19","fileFolderName":"AGMZ1467.avi","itemType":"2","itemUniqueCode":"bapsSite1Item346","itemSource":"3","confidentiality":"4","accesssibility":"5","language":"543385","additionalDescp":"1. blah blah blah  2. blah blah blah  3. blah blah blah...","eventDateArray":[{"eventStartDateYYYY":"2016","eventStartDateMM":"12","eventStartDateDD":"19","eventEndDateYYYY":"2016","eventEndDateMM":"12","eventEndDateDD":"19"},{"eventStartDateYYYY":"2017","eventStartDateMM":"12","eventStartDateDD":"19","eventEndDateYYYY":"2017","eventEndDateMM":"12","eventEndDateDD":"19"}],"subjectArray":[{"subjectId":"4001","subjectText":"Nishkam Vartman1","subjectRank":"4"},{"subjectId":"4002","subjectText":"Nirlobh Vartman2","subjectRank":"4"},{"subjectId":"4003","subjectText":"Nirman Vartman3","subjectRank":"4"},{"subjectId":"4004","subjectText":"Nihsheh Vartman4","subjectRank":"4"},{"subjectId":"4005","subjectText":"Niswad Vartman5","subjectRank":"4"}],"keywordLineArray":[{"whoId":"10","actionId":"1711","whereId":"10105275","whenId":"1898","whoText":"Pramukh Swami Maharaj","actionText":"smiling","whomText":"Subhashbhai","whereText":"Salangpur, BAPS Mandir","whenText":"Shangar"},{"whoId":"1009","actionId":"1008","whatId":"1007","whomId":"1006","whereId":"1005","whoText":"Obama","actionText":"doing","whatText":"dandvat","whomText":"PramukhSwamiMaharaj","whereText":"Sarangpur YagnapurushVadi","whenText":"PSM 95th Janmajayanti","keywordLineRank":"5"}]}]';
	// alert(responseJson);
	if(responseJson != "") {	//populate the table
		var searchJsonData = JSON.parse(responseJson);
		if(typeof searchResultsDataTable != undefined && $.fn.dataTable.isDataTable('#search_results')) {
			searchResultsDataTable.clear();
			searchResultsDataTable.rows.add(searchJsonData);
			searchResultsDataTable.draw();
		} else {
			searchResultsDataTable = $('#search_results').DataTable({
				data: searchJsonData,
				columnDefs: [
							{data: 'itemTitle', defaultContent: '', targets: 0},							//Title
							{data: 'itemSource', defaultContent: '', targets: 1},							//Soure
							{data: 'urlOrFileFolderPath', defaultContent: '', targets: 2},					//File Path
							{data: 'lastUpdatedDate', defaultContent: '', targets: 3},						//Last Updated
							{data: 'itemId', targets: 4, render: function(data, type, full, meta) {
																	return '<div class="controls"><a href="#" class="text-primary" data-toggle="offcanvas" data-placement="top" title="<?php echo $LABEL_VIEW; ?>" onClick="editSearchItemDetails(' + data + ')"><span class="fa fa-eye"></span></a></div>';
																}											//Controls
							}
							],
				order: [[0, "asc"]]
			});
		}
		
		//<div class="controls"><a href="videoinfo.php?itemId=' + get(itemDetails['itemId']) + '" class="text-primary" data-toggle="tooltip" data-placement="top" title="View"><span class="fa fa-eye"></span></a></div>
	}
}

function editSearchItemDetails(itemId) {
	$("#searchPageContent").hide();
	$("#editPageContent").append('<?php echo $PARENT_ITEM_TOKEN_HTML; ?>');
	$("#editPageContent").show();
	$('.itemCategory').select2({
		placeholder: "<?php echo $LABEL_CATEGORY_PLACEHOLDER; ?>",
		data: <?php echo $ITEM_CATEGORY_LOOKUPS; ?>,
		templateSelection: formatCategorySelection,
		templateResult: formatCategoryResult,
		minimumInputLength: 0
	});
	$('.itemCategory').val(null).trigger("change");
	bindVideoEventsAndComponents(false);
	var strUrl = "parentItemId=" + itemId;
	xmlhttp = getXmlhttp();
	xmlhttp.onreadystatechange = function()	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			if(xmlhttp.responseText != "") {
				var JsonString = xmlhttp.responseText;
				var jsonData = JSON.parse(JsonString);
				// alert(JsonString);
				populateItemToView(jsonData);
			} else {
				//do nothing
			}
		}
	}
	console.log(strUrl);
	xmlhttp.open("POST", "<?php echo $FILE_ITEM_VIEW_PHP; ?>?" + strUrl, false);
	xmlhttp.send();
}

function backTOSearchResults() {
	$("#searchPageContent").show();
	$("#editPageContent").html("");
}

</script>