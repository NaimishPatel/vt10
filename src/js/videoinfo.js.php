<?php
	opcache_reset();
?>
<script>
<?php
	if($itemId != null && $itemId != "") {
		echo "viewMode = true;";
		echo "itemId = '".$itemId."';";
	}
	else {
		echo "viewMode = false;";
	 // echo "alert(JsonString);";
	}
	if($_REQUEST["showExplorer"] != null && $_REQUEST["showExplorer"] != "false") {
		echo "showExplorer = true;";
	}
	else {
		echo "showExplorer = false;";
	}
?>

$(document).ready(function(){
	$(window).scroll(function() {
		if ($(document).scrollTop() > 200) {
		  $(".scroll-to-top").fadeIn(200);
		} else {
		  $(".scroll-to-top").fadeOut(200);
		}
	});

	$('#wordSuggestions').change(function() {
        if ($(this).is(':checked')) {
    		localStorage.setItem("wordSuggestions", true);
        }else{
        	localStorage.setItem("wordSuggestions", false);
        }
    });
    
	function enableWordSuggestions(){
		var flag = localStorage.getItem("wordSuggestions") == 'false' ? false : true;
		$("#wordSuggestions").prop("checked",flag);
	}
	enableWordSuggestions();
	
	// $('#elfinder').hide();
	// $('#video-content').show();
	
	elFinder.prototype.i18.en.messages['cmdtag'] = 'Tag';          
    elFinder.prototype._options.commands.push('tag');
    elFinder.prototype.commands.tag = function() {
        this.exec = function(hashes) {
			 // alert(this.fm.path(hashes));
			 var filePaths = [];
			 var loopIndx;
			 for(loopIndx = 0; loopIndx < hashes.length; loopIndx++) {
				addUrlOrFileFolderPath(this.fm.path(hashes[loopIndx]));
				filePaths[loopIndx] = this.fm.path(hashes[loopIndx]);
			}
			 $('#elfinder').hide();
			 $('#video-content').show();
			 if(hashes.length > 1)
				$("#addClip_div").hide();
        }
        this.getstate = function() {
            //return 0 to enable, -1 to disable icon access
            return 0;
        }
    }
	

	// Append options to select dropdowns - Category
	// $('.itemCategory').select2({
		// placeholder: "<?php echo $LABEL_CATEGORY_PLACEHOLDER; ?>",
		// data: <?php echo $ITEM_CATEGORY_LOOKUPS; ?>,
		// templateSelection: formatCategorySelection,
		// templateResult: formatCategoryResult,
		// minimumInputLength: 0
	// });
	// $('.itemCategory').val(null).trigger("change");
	bindVideoEventsAndComponents(false);
	
	if(viewMode) {	//populate the page
		// JsonString = '[{"itemId":"123661","parentItemId":"","itemCategory":"1.H","itemTitle":"Guruhari Darshan trial","technicalQuality":"3","technicalNotes":"Good","urlOrFileFolderPath":"My Videos","itemType":"1","itemSource":"bapsChannel","confidentiality":"1","accessibility":"01","language":"001","additionalDescp":"Description for Darshan","eventDateArray":[{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"12","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"12"},{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"11","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"11"}],"keywordLineArray":[{"keywordLine":"3","keywordIndex":"1","keywordId":"10","keywordText":"Pramukh Swami Maharaj"},{"keywordLine":"3","keywordIndex":"2","keywordId":"wearing","keywordText":"wearing"},{"keywordLine":"3","keywordIndex":"3","keywordId":"1097","keywordText":"Chakdis"},{"keywordLine":"4","keywordIndex":"1","keywordId":"sadhus","keywordText":"sadhus"},{"keywordLine":"4","keywordIndex":"2","keywordId":"1759","keywordText":"dancing"}],"subjectArray":[{"subjectLine":"1","subjectIndex":"1","subjectId":"2177","subjectText":"કાન / Ears"},{"subjectLine":"2","subjectIndex":"1","subjectId":"3506","subjectText":"Topic"}],"eventArray":[{"eventLine":"1","eventIndex":"1","eventId":"1910","eventText":"1995 - Amrut Mahotsav"},{"eventLine":"2","eventIndex":"1","eventId":"1907","eventText":"1990 - Yuva Adhiveshan"}],"locationArray":[{"locationId":"10105275","locationText":"Salangpur, BAPS Mandir","locationTree":"Salangpur, BAPS Mandir"},{"locationId":"10105274","locationText":"Mumbai (M. Corp.)","locationTree":"Mumbai (M. Corp.)"}]},{"itemId":"123665","parentItemId":"123661","itemCategory":"1.H","itemTitle":"Guruhari Darshan trial","technicalQuality":"3","technicalNotes":"Good","urlOrFileFolderPath":"My Videos","itemType":"1","itemSource":"bapsChannel","confidentiality":"1","accessibility":"01","language":"001","additionalDescp":"Description for Darshan","eventDateArray":[{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"12","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"12"},{"eventStartDateYYYY":"2016","eventStartDateMM":"01","eventStartDateDD":"11","eventEndDateYYYY":"2016","eventEndDateMM":"01","eventEndDateDD":"11"}],"keywordLineArray":[{"keywordLine":"3","keywordIndex":"1","keywordId":"10","keywordText":"Pramukh Swami Maharaj"},{"keywordLine":"3","keywordIndex":"2","keywordId":"wearing","keywordText":"wearing"},{"keywordLine":"3","keywordIndex":"3","keywordId":"1097","keywordText":"Chakdis"},{"keywordLine":"4","keywordIndex":"1","keywordId":"sadhus","keywordText":"sadhus"},{"keywordLine":"4","keywordIndex":"2","keywordId":"1759","keywordText":"dancing"}],"subjectArray":[{"subjectLine":"1","subjectIndex":"1","subjectId":"2177","subjectText":"કાન / Ears"},{"subjectLine":"2","subjectIndex":"1","subjectId":"3506","subjectText":"Topic"}],"eventArray":[{"eventLine":"1","eventIndex":"1","eventId":"1910","eventText":"1995 - Amrut Mahotsav"},{"eventLine":"2","eventIndex":"1","eventId":"1907","eventText":"1990 - Yuva Adhiveshan"}],"locationArray":[{"locationId":"10105275","locationText":"Salangpur, BAPS Mandir","locationTree":"Salangpur, BAPS Mandir"},{"locationId":"10105274","locationText":"Mumbai (M. Corp.)","locationTree":"Mumbai (M. Corp.)"}]}]';
		$('#elfinder').hide();
		$('#video-content').show();
		var strUrl = "parentItemId=" + itemId;
		xmlhttp = getXmlhttp();
		xmlhttp.onreadystatechange = function()	{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				if(xmlhttp.responseText != "") {
					var JsonString = xmlhttp.responseText;
					var jsonData = JSON.parse(JsonString);
					// alert(JsonString);
					populateItemToView(jsonData);
				} else {
					//do nothing
				}
			}
		}
		console.log(strUrl);
		xmlhttp.open("POST", "<?php echo $FILE_ITEM_VIEW_PHP; ?>?" + strUrl, false); //encodeURIComponent(strUrl)
		xmlhttp.send();
	} else if(showExplorer) {
		$('#elfinder').elfinder({
			url : '../php/remote/connector.video.php',  // connector URL (REQUIRED)
			onlyMimes : ['video'],
			height : (($(window).height()-50)*1-20),
			rememberLastDir: true,
			uiOptions : {
				// toolbar configuration
				toolbar : [
					['back', 'forward'],
					['netmount'],
					// ['reload'],
					// ['home', 'up'],
					['mkdir', 'mkfile', 'upload'],
					['open', 'download', 'getfile'],
					['info'],
					['quicklook'],
					['copy', 'cut', 'paste'],
					['rm'],
					['duplicate', 'rename', 'edit', 'resize'],
					['extract', 'archive'],
					['search'],
					['view', 'sort'],
					['help']
				],
				// directories tree options
				tree : {
					// expand current root on init
					openRootOnLoad : true,
					// auto load current dir parents
					syncTree : true
				},
				// navbar options
				navbar : {
					minWidth : 150,
					maxWidth : 500
				},
				cwd : {
					// display parent folder with ".." name :)
					oldSchool : false
				}
			},
			contextmenu : {
				// navbarfolder menu
				navbar : ['tag', '|', 'open', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'info'],
				// current directory menu
				cwd    : ['reload', 'back', '|', 'upload', 'mkdir', 'mkfile', 'paste', '|', 'sort', '|', 'info'],
				// current directory file menu
				files  : ['tag', '|', 'getfile', '|','open', 'quicklook', '|', 'download', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'edit', 'rename', 'resize', '|', 'archive', 'extract', '|', 'info']
			}
		});
	} else {
		$('#video-content').show();
	}
});

/** ########################## VLS  Functions########################*/
 function formatMetaData(data){
	if(data != undefined){
		return data;
	}
	return "";
 }
 
 function setVideoJSONdata(elm){
	 debugger
	 $(elm).find('span').removeClass('glyphicon.glyphicon-download-alt');
	 $(elm).find('span').addClass("fa fa-spinner fa-spin");
	 /* $.get("vlcjson.php",function(responseText){
		console.log(responseText);
		$(elm).find('span').removeClass("fa fa-spinner fa-spin");
		$(elm).find('span').addClass('glyphicon.glyphicon-download-alt');
		var videoJSONdata = JSON.parse(this.responseText);
		var temp = '{"fullscreen":0,"stats":{"inputbitrate":0.0068438407,"sentbytes":0,"lostabuffers":2,"averagedemuxbitrate":0,"readpackets":114,"demuxreadpackets":0,"lostpictures":0,"displayedpictures":0,"sentpackets":0,"demuxreadbytes":110376,"demuxbitrate":0.0070067896,"playedabuffers":641,"demuxdiscontinuity":0,"decodedaudio":642,"sendbitrate":0,"readbytes":112192,"averageinputbitrate":0,"demuxcorrupted":0,"decodedvideo":0},"audiodelay":0,"apiversion":3,"currentplid":4,"time":286,"volume":261,"length":1625,"random":false,"audiofilters":{"filter_0":""},"rate":1,"videoeffects":{"hue":0,"saturation":1,"contrast":1,"brightness":1,"gamma":1},"state":"paused","loop":false,"version":"2.2.6 Umbrella","position":0.17652391,"information":{"chapter":0,"chapters":[],"title":0,"category":{"meta":{"album":"Shreemukh","title":"Chesta","filename":"Chesta","artwork_url":"file:///C:/Users/admin/AppData/Roaming/vlc/art/artistalbum/Shreemukh/Shreemukh/art","genre":"Kirtans","artist":"Shreemukh"},"Stream 0":{"Bitrate":"56 kb/s","Type":"Audio","Channels":"Stereo","Sample_rate":"24000 Hz","Codec":"MPEG Audio layer 1/2 (mpga)"}},"titles":[]},"repeat":false,"subtitledelay":0,"equalizer":[]}';
    	//var videoJSONdata = JSON.parse(temp);
		document.getElementById("edit_item00_itemTitle").value = videoJSONdata.information.category['meta'].filename;
		debugger
		document.getElementById("edit_item00_additionalDescp").value += '\n Resolution: ' + formatMetaData(videoJSONdata.information.category['Stream 0'].Video_resolution);
        document.getElementById("edit_item00_additionalDescp").value += '\n Frame Rate: ' +  formatMetaData(videoJSONdata.information.category['Stream 0'].Frame_rate);
		document.getElementById("edit_item00_additionalDescp").value += '\n Rate: ' +  formatMetaData(videoJSONdata.rate);
        document.getElementById("edit_item00_additionalDescp").value += '\n Codec: ' +  formatMetaData(videoJSONdata.information.category['Stream 0'].Codec);
	}); */
	 debugger;
	 	var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		        var videoJSONdata = JSON.parse(this.responseText);
		        $(elm).find('span').removeClass("fa fa-spinner fa-spin");
				$(elm).find('span').addClass('glyphicon.glyphicon-download-alt');
		        var temp = '{"fullscreen":0,"stats":{"inputbitrate":0.0068438407,"sentbytes":0,"lostabuffers":2,"averagedemuxbitrate":0,"readpackets":114,"demuxreadpackets":0,"lostpictures":0,"displayedpictures":0,"sentpackets":0,"demuxreadbytes":110376,"demuxbitrate":0.0070067896,"playedabuffers":641,"demuxdiscontinuity":0,"decodedaudio":642,"sendbitrate":0,"readbytes":112192,"averageinputbitrate":0,"demuxcorrupted":0,"decodedvideo":0},"audiodelay":0,"apiversion":3,"currentplid":4,"time":286,"volume":261,"length":1625,"random":false,"audiofilters":{"filter_0":""},"rate":1,"videoeffects":{"hue":0,"saturation":1,"contrast":1,"brightness":1,"gamma":1},"state":"paused","loop":false,"version":"2.2.6 Umbrella","position":0.17652391,"information":{"chapter":0,"chapters":[],"title":0,"category":{"meta":{"album":"Shreemukh","title":"Chesta","filename":"Chesta","artwork_url":"file:///C:/Users/admin/AppData/Roaming/vlc/art/artistalbum/Shreemukh/Shreemukh/art","genre":"Kirtans","artist":"Shreemukh"},"Stream 0":{"Bitrate":"56 kb/s","Type":"Audio","Channels":"Stereo","Sample_rate":"24000 Hz","Codec":"MPEG Audio layer 1/2 (mpga)"}},"titles":[]},"repeat":false,"subtitledelay":0,"equalizer":[]}';
	        	//var videoJSONdata = JSON.parse(temp);
				document.getElementById("edit_item00_itemTitle").value = videoJSONdata.information.category['meta'].filename;
				debugger
				document.getElementById("edit_item00_additionalDescp").value += '\n Resolution: ' + formatMetaData(videoJSONdata.information.category['Stream 0'].Video_resolution);
		        document.getElementById("edit_item00_additionalDescp").value += '\n Frame Rate: ' +  formatMetaData(videoJSONdata.information.category['Stream 0'].Frame_rate);
				document.getElementById("edit_item00_additionalDescp").value += '\n Rate: ' +  formatMetaData(videoJSONdata.rate);
		        document.getElementById("edit_item00_additionalDescp").value += '\n Codec: ' +  formatMetaData(videoJSONdata.information.category['Stream 0'].Codec);
				
		    }
	};
	xmlhttp.open("GET", "vlcjson.php", true);
	xmlhttp.send(); 
	 
	 /* $.get("vlcplaylistjson.php",function(responseText){
		 try {
		        var videoJSONdata = JSON.parse(this.responseText);
				var found = getfullpathfromplaylistid(videoJSONdata.children[0].children);
				console.log(found[0].uri);
				document.getElementById("edit_item00_urlOrFileFolderPathText").value = (decodeURIComponent(found[0].uri).replace("file:///", "").replace("file://", "\\\\")).replace(/\//g, "\\");
				addUrlOrFileFolderPath($('#edit_item00_urlOrFileFolderPathText').val());
		    } catch (e) {
				// TODO: handle exception
			}
	 }); */
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
		    try {
			    debugger;
		    	$(elm).find('span').removeClass("fa fa-spinner fa-spin");
				$(elm).find('span').addClass('glyphicon.glyphicon-download-alt');
		        var videoJSONdata = JSON.parse(this.responseText);
				var found = getfullpathfromplaylistid(videoJSONdata.children[0].children);
				console.log(found[0].uri);
				document.getElementById("edit_item00_urlOrFileFolderPathText").value = (decodeURIComponent(found[0].uri).replace("file:///", "").replace("file://", "\\\\")).replace(/\//g, "\\");
				addUrlOrFileFolderPath($('#edit_item00_urlOrFileFolderPathText').val());
		    } catch (e) {
				// TODO: handle exception
			}
	    }
	};
	xmlhttp.open("GET", "vlcplaylistjson.php", true);
	xmlhttp.send();  
}


function toHHMMSS (secs) {
    var sec_num = parseInt(secs, 10); 
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
}

function getfullpathfromplaylistid(myObj) {
	var data = myObj;
	  return data.filter(
	      function(data){ return data.current == 'current' }
	  );
}


function setstarttime(elm){
	debugger;
	$(elm).find('span').removeClass('glyphicon glyphicon-time');
	$(elm).find('span').addClass("fa fa-spinner fa-spin");
	/* $.get("vlcjson.php",function(responseText){
		$(elm).find('span').removeClass("fa fa-spinner fa-spin");
		$(elm).find('span').addClass('glyphicon glyphicon-time');
		
		//var myObj = JSON.parse(this.responseText);
		var myObj = JSON.parse('{"time":1000}');
        // document.getElementById("endtime").innerHTML = toHHMMSS(myObj.time);
        var nn = $(elm).attr('data-target');
        document.getElementById("edit_item"+nn+"_clipStartTime").value = toHHMMSS(myObj.time);
	}); */
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	        var myObj = JSON.parse(this.responseText);
	        $(elm).find('span').removeClass("fa fa-spinner fa-spin");
			$(elm).find('span').addClass('glyphicon glyphicon-time');
	        // document.getElementById("starttime").innerHTML = toHHMMSS(myObj.time);
	        var nn = $(elm).attr('data-target');
	        document.getElementById("edit_item"+nn+"_clipStartTime").value = toHHMMSS(myObj.time);
	    }
	};
	xmlhttp.open("GET", "vlcjson.php", true);
	xmlhttp.send(); 
}

function setendtime(elm){
	debugger;
	$(elm).find('span').removeClass('glyphicon glyphicon-time');
	$(elm).find('span').addClass("fa fa-spinner fa-spin");
	$.get("vlcjson.php",function(responseText){
		$(elm).find('span').removeClass("fa fa-spinner fa-spin");
		$(elm).find('span').addClass('glyphicon glyphicon-time');
		
		var myObj = JSON.parse(responseText);
		//var myObj = JSON.parse('{"time":1100}');
        // document.getElementById("endtime").innerHTML = toHHMMSS(myObj.time);
        var nn = $(elm).attr('data-target');
        document.getElementById("edit_item"+nn+"_clipEndTime").value = toHHMMSS(myObj.time);
	});
	/* var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		        var myObj = JSON.parse(this.responseText);
		        // document.getElementById("endtime").innerHTML = toHHMMSS(myObj.time);
		        document.getElementById("edit_item"+nn+"_clipEndTime").value = toHHMMSS(myObj.time);
	    }
	};
	xmlhttp.open("GET", "vlcjson.php", true);
	xmlhttp.send(); */ 
} 

 


</script>