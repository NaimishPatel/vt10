<?php
	session_start();
	opcache_reset();
	if(isset($_SESSION['username']))
		session_unset();
	require_once 'src/php/include/constants4index.php';
	require_once 'api/connection.php';
	require_once 'src/php/functions.php';
?>
<?php
	function encrypt($str) {
		return crypt($str,'$1$salt&pepe');
	}
?>
<?php
	$p_username = getFormValue('username');
	$p_password = getFormValue('password');
	$showError = "";

	if ($p_username != "" && $p_password != "")	{
		$vars = array("~~username~~" => $p_username, "~~password~~" => encrypt($p_password));
		$qryAuthQuery = substituteTokensIn($QUERY_AUTHENTICATE_USER, $vars);
		$rsUser  = mysql_query($qryAuthQuery, $con);
		if($rowUser = mysql_fetch_array($rsUser)) {
			$_SESSION["username"] = $p_username;
			$_SESSION["userId"] = $rowUser['user_id'];
			$_SESSION["parentUserId"] = $rowUser['parent_user_id'];
			$_SESSION["userEmail"] = $rowUser['user_email'];
			$_SESSION["userMobile"] = $rowUser['user_mobile'];
			$_SESSION["masterAdmin"] = $rowUser['master_admin'];
			// echo "User ID: ".$_SESSION["userId"];
			// echo "<script> window.location.href = '".$FILE_VIDEOINFO_PHP_4INDEX."';</script>";
			echo "<script> window.location.href = '".$FILE_DASHBOARD_PHP_4INDEX."';</script>";
		} else {
			$showError = $ERR_INVALID_USERNAME_PASSWORD;
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="<?php echo $LABEL_INDEX_PAGE_META_DESCRIPTION; ?>" />
	<title><?php echo $LABEL_INDEX_PAGE_TITLE; ?></title>
    <link href="<?php echo $FILE_FAVICON_PNG_4INDEX; ?>" rel="shortcut icon" />
    <link href="<?php echo $FILE_BOOTSTRAP_MIN_CSS_4INDEX; ?>" rel="stylesheet" />
    <link href="<?php echo $FILE_FONT_AWESOME_MIN_CSS_4INDEX; ?>" rel="stylesheet" />
	<!--link href="<?php echo $FILE_VIDEOINFO_CSS_4INDEX; ?>" rel="stylesheet" /-->
	<style>
		/* CSS for Login page starts */
		div#center {
			width:360px;
			height:208px;
			border-radius: 15px 15px 5px 5px;
			-moz-border-radius: 15px 15px 5px 5px;
		/* 	box-shadow: 5px 5px 80px 0px #8b8b8b; */
			position:absolute;
			top:0px;
			right:0px;
			bottom:0px;
			left:0px;
			margin:auto;
		}
		#welcomeText {
			font-family:Tahoma, Geneva, sans-serif;
			width:360px;
			font-size:125%;
			padding:8px;
			color:#404040;
			text-shadow: 1px 1px 0px #FFF;
			background-color:#ddd;
			border-bottom:#adadad solid 1px !important;
			border:#e3e3e3 solid 1px;
			border-radius: 15px 15px 0px 0px;
			-moz-border-radius: 15px 15px 0px 0px;
		}
		#formDiv {
			width:360px;
			padding:15px;
			background-color:#cdcdcd;
			border-top:#e3e3e3 solid 1px !important;
			border:#e3e3e3 solid 1px;
			border-radius: 0px 0px 5px 5px;
			-moz-border-radius: 0px 0px 5px 5px;
		}
		/* CSS for Login page ends */
	</style>
</head>

<body style="background-image:url(<?php echo $FILE_BAPS_LOGO_BG_PNG_4INDEX; ?>); background-attachment:fixed; background-repeat:no-repeat; background-position:center 15%;">
	<div class="container-fluid">
		<div id="center">
			<div align="center" id="welcomeText">
				<?php echo $LABEL_INDEX_PAGE_WELCOME_TEXT; ?>
			</div>
	        <div id="formDiv">
		        <form name="loginform" id="loginform" method="post">
		        	<div class="form-group input-group">
						<span class="input-group-addon">
							<label for="usrname" style="width:15px;">
								<span class="fa fa-user"></span>
							</label>
						</span>
						<input type="text" class="form-control" name="username" id="usrname" placeholder="<?php echo $LABEL_USERNAME_PLACEHOLDER; ?>" tabindex="1" autofocus style="width:100%;">
		            </div>
		            <div class="form-group input-group">
						<span class="input-group-addon">
							<label for="password" style="width:15px;">
								<span class="fa fa-eye"></span>
							</label>
						</span>
						<input type="password" class="form-control" name="password" id="password" placeholder="<?php echo $LABEL_PASSWORD_PLACEHOLDER; ?>" tabindex="2" style="width:100%;">
		            </div>
		            <a class="btn btn-default btn-block btn-sm" name="login" id="login" style="font-size:87.5%;" tabindex="3" onClick="validate();">
		            	<span class="glyphicon glyphicon-log-in" style="font-size:75%;"></span>
		            	&nbsp;&nbsp;<?php echo $LABEL_LOGIN; ?>
		            </a>
		        </form>
		        <div class="small text-danger text-center show-error" style="font-family:Tahoma, Geneva, sans-serif; margin-top:10px;"><?php echo $showError; ?></div>
	        	<div style="font-size:68.75%; font-family:Tahoma, Geneva, sans-serif; color:#666; text-shadow: 1px 1px 0px #dadada; text-align:center; margin-top:10px;">
	        		<?php echo $LABEL_CONTACT_ADMIN_USERNAME_PASSWORD; ?>
	        	</div>
	        </div>
    	</div>
	</div>
	<script src="<?php echo $FILE_JQUERY_JS_4INDEX; ?>"></script>
	<script src="<?php echo $FILE_BOOTSTRAP_JS_4INDEX; ?>"></script>
	<script>
		function validate() {
			if($('#usrname').val() == "" || $('#password').val() == "") {
				$('.show-error').text("<?php echo $ERR_BLANK_USERNAME_PASSWORD; ?>");
				$('.form-control').parent().removeClass('form-group');
				$('.form-control').parent().addClass('has-error');
			} else {
				document.loginform.action = "#";
				document.loginform.submit();
			}
		}
		$('#login').keydown(function(event) {
			var keyCode = event.keyCode || event.which;
			if(keyCode == 13) { // Enter key
				event.preventDefault();
				validate();
			}
		});
		<?php
			if($showError) {
				echo "$('.form-control').parent().removeClass('form-group');";
				echo "$('.form-control').parent().addClass('has-error');";
			}
		?>
	</script>
</body>
</html>
